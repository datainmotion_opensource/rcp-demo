/**
 * Copyright (c) 2016 Data In Motion Consulting and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion Consulting - initial API and implementation
 */
package de.dim.rcp.demo.api.helloworld;

/**
 * This is just a simple Service template
 *  
 */
public interface IExampleService {
	
	/**
	 * Here should be a good description
	 * @param withThis my fancy parameter
	 */
	public void doSomething(String withThis);

}
