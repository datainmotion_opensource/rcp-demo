/**
 * Copyright (c) 2016 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package de.dim.rcp.demo.helloworld.command;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.osgi.framework.console.CommandInterpreter;
import org.eclipse.osgi.framework.console.CommandProvider;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import de.dim.rcp.demo.api.helloworld.IExampleService;

/**
 * @author Juergen Albert
 *
 */
@Component(immediate = true)
public class HelloWorldCommand implements CommandProvider {

	List<IExampleService> services = new LinkedList<>();
	
	public void _doSomething(CommandInterpreter ci){
		String withWhat = ci.nextArgument();
		if(withWhat == null){
			ci.println("There must be some kind of argument here!");
			return;
		}
		services.forEach((s) -> s.doSomething(withWhat));
	}
	
	public void _doSomethingSpecial(CommandInterpreter cmd){
		String filter = cmd.nextArgument();
		if(filter == null){
			cmd.println("There must be some kind of argument here!");
			return;
		}
		
		String withWhat = cmd.nextArgument();
		if(withWhat == null){
			cmd.println("There must be some kind of argument here!");
			return;
		}
		
		BundleContext bundleContext = FrameworkUtil.getBundle(getClass()).getBundleContext();
		try {
			Collection<ServiceReference<IExampleService>> serviceReferences = bundleContext.getServiceReferences(IExampleService.class, String.format("(name=%s)", filter));
			
			if(serviceReferences.size() == 0){
				cmd.println("No service could be found for name " + filter);
				return;
			}
			
			ServiceReference<IExampleService> serviceReference = serviceReferences.iterator().next();
			IExampleService service = bundleContext.getService(serviceReference);
			service.doSomething(withWhat);
			bundleContext.ungetService(serviceReference);
			
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Activate
	public void activate() {
		System.err.println("HelloWorldCommand activated");
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osgi.framework.console.CommandProvider#getHelp()
	 */
	@Override
	public String getHelp() {
		return "A Sample Command with the following usage: doSomething <someWord>";
	}
	
	/**
	 * @param exampleService the Service to set
	 */
	@Reference(unbind = "unsetExampleService", cardinality =  ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public void setExampleService(IExampleService exampleService){
		System.err.println("A Service was set");
		services.add(exampleService);
	}

	/**
	 * @param exampleService the service to unset
	 */
	public void unsetExampleService(IExampleService exampleService){
		System.err.println("A Service was unset");
		services.remove(exampleService);
	}
	
	/**
	 * @param exampleService the Service to set
	 */
	@Reference(unbind = "unsetManualExampleService", target="(name=manual)", cardinality=ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.DYNAMIC)
	public void setManuelExampleService(IExampleService exampleService){
		System.err.println("Oh look another service that had the name manuel!");
	}
	
	/**
	 * @param exampleService the service to unset
	 */
	public void unsetManualExampleService(IExampleService exampleService){
		System.err.println("And here goes the manuel service...");
	}

}
