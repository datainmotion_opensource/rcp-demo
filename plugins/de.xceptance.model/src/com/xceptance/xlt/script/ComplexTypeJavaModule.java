/**
 */
package com.xceptance.xlt.script;

import java.math.BigInteger;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Java Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getTags <em>Tags</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getDescription <em>Description</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getClass_ <em>Class</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getId <em>Id</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule()
 * @model extendedMetaData="name='complexType.JavaModule' kind='elementOnly'"
 * @generated
 */
public interface ComplexTypeJavaModule extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Tags</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tags</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tags</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule_Tags()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tags' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getTags();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule_Description()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getDescription();

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeModuleParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule_Parameter()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parameter' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<ComplexTypeModuleParameter> getParameter();

	/**
	 * Returns the value of the '<em><b>Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' attribute.
	 * @see #setClass(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule_Class()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString" required="true"
	 *        extendedMetaData="kind='attribute' name='class'"
	 * @generated
	 */
	String getClass_();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getClass_ <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' attribute.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule_Id()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(BigInteger)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeJavaModule_Version()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeVersionIdent" required="true"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	BigInteger getVersion();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(BigInteger value);

} // ComplexTypeJavaModule
