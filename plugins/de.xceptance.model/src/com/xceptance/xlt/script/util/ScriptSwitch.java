/**
 */
package com.xceptance.xlt.script.util;

import com.xceptance.xlt.script.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.xceptance.xlt.script.ScriptPackage
 * @generated
 */
public class ScriptSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScriptPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScriptSwitch() {
		if (modelPackage == null) {
			modelPackage = ScriptPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE: {
				ComplexTypeJavaModule complexTypeJavaModule = (ComplexTypeJavaModule)theEObject;
				T result = caseComplexTypeJavaModule(complexTypeJavaModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_MODULE_PARAMETER: {
				ComplexTypeModuleParameter complexTypeModuleParameter = (ComplexTypeModuleParameter)theEObject;
				T result = caseComplexTypeModuleParameter(complexTypeModuleParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_ACTION: {
				ComplexTypeScriptAction complexTypeScriptAction = (ComplexTypeScriptAction)theEObject;
				T result = caseComplexTypeScriptAction(complexTypeScriptAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND: {
				ComplexTypeScriptCommand complexTypeScriptCommand = (ComplexTypeScriptCommand)theEObject;
				T result = caseComplexTypeScriptCommand(complexTypeScriptCommand);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE: {
				ComplexTypeScriptModule complexTypeScriptModule = (ComplexTypeScriptModule)theEObject;
				T result = caseComplexTypeScriptModule(complexTypeScriptModule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1: {
				ComplexTypeScriptModule1 complexTypeScriptModule1 = (ComplexTypeScriptModule1)theEObject;
				T result = caseComplexTypeScriptModule1(complexTypeScriptModule1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE_CONDITION: {
				ComplexTypeScriptModuleCondition complexTypeScriptModuleCondition = (ComplexTypeScriptModuleCondition)theEObject;
				T result = caseComplexTypeScriptModuleCondition(complexTypeScriptModuleCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER: {
				ComplexTypeScriptModuleParameter complexTypeScriptModuleParameter = (ComplexTypeScriptModuleParameter)theEObject;
				T result = caseComplexTypeScriptModuleParameter(complexTypeScriptModuleParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE: {
				ComplexTypeTestCase complexTypeTestCase = (ComplexTypeTestCase)theEObject;
				T result = caseComplexTypeTestCase(complexTypeTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS: {
				ComplexTypeTestCasePostSteps complexTypeTestCasePostSteps = (ComplexTypeTestCasePostSteps)theEObject;
				T result = caseComplexTypeTestCasePostSteps(complexTypeTestCasePostSteps);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScriptPackage.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Java Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Java Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeJavaModule(ComplexTypeJavaModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Module Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Module Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeModuleParameter(ComplexTypeModuleParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Script Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Script Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeScriptAction(ComplexTypeScriptAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Script Command</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Script Command</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeScriptCommand(ComplexTypeScriptCommand object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Script Module</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Script Module</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeScriptModule(ComplexTypeScriptModule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Script Module1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Script Module1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeScriptModule1(ComplexTypeScriptModule1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Script Module Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Script Module Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeScriptModuleCondition(ComplexTypeScriptModuleCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Script Module Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Script Module Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeScriptModuleParameter(ComplexTypeScriptModuleParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeTestCase(ComplexTypeTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Type Test Case Post Steps</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Type Test Case Post Steps</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexTypeTestCasePostSteps(ComplexTypeTestCasePostSteps object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ScriptSwitch
