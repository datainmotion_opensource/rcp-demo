/**
 */
package com.xceptance.xlt.script.util;

import com.xceptance.xlt.script.*;

import java.math.BigInteger;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.emf.ecore.xml.type.util.XMLTypeValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see com.xceptance.xlt.script.ScriptPackage
 * @generated
 */
public class ScriptValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ScriptValidator INSTANCE = new ScriptValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "com.xceptance.xlt.script";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLTypeValidator xmlTypeValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScriptValidator() {
		super();
		xmlTypeValidator = XMLTypeValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ScriptPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE:
				return validateComplexTypeJavaModule((ComplexTypeJavaModule)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_MODULE_PARAMETER:
				return validateComplexTypeModuleParameter((ComplexTypeModuleParameter)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_ACTION:
				return validateComplexTypeScriptAction((ComplexTypeScriptAction)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND:
				return validateComplexTypeScriptCommand((ComplexTypeScriptCommand)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE:
				return validateComplexTypeScriptModule((ComplexTypeScriptModule)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1:
				return validateComplexTypeScriptModule1((ComplexTypeScriptModule1)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE_CONDITION:
				return validateComplexTypeScriptModuleCondition((ComplexTypeScriptModuleCondition)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER:
				return validateComplexTypeScriptModuleParameter((ComplexTypeScriptModuleParameter)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE:
				return validateComplexTypeTestCase((ComplexTypeTestCase)value, diagnostics, context);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS:
				return validateComplexTypeTestCasePostSteps((ComplexTypeTestCasePostSteps)value, diagnostics, context);
			case ScriptPackage.DOCUMENT_ROOT:
				return validateDocumentRoot((DocumentRoot)value, diagnostics, context);
			case ScriptPackage.SIMPLE_TYPE_SCRIPT_COMMAND_NAME:
				return validateSimpleTypeScriptCommandName((SimpleTypeScriptCommandName)value, diagnostics, context);
			case ScriptPackage.SIMPLE_TYPE_NON_EMPTY_STRING:
				return validateSimpleTypeNonEmptyString((String)value, diagnostics, context);
			case ScriptPackage.SIMPLE_TYPE_SCRIPT_COMMAND_NAME_OBJECT:
				return validateSimpleTypeScriptCommandNameObject((SimpleTypeScriptCommandName)value, diagnostics, context);
			case ScriptPackage.SIMPLE_TYPE_STRING128:
				return validateSimpleTypeString128((String)value, diagnostics, context);
			case ScriptPackage.SIMPLE_TYPE_VERSION_IDENT:
				return validateSimpleTypeVersionIdent((BigInteger)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeJavaModule(ComplexTypeJavaModule complexTypeJavaModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeJavaModule, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeModuleParameter(ComplexTypeModuleParameter complexTypeModuleParameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeModuleParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeScriptAction(ComplexTypeScriptAction complexTypeScriptAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeScriptAction, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeScriptCommand(ComplexTypeScriptCommand complexTypeScriptCommand, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeScriptCommand, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeScriptModule(ComplexTypeScriptModule complexTypeScriptModule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeScriptModule, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeScriptModule1(ComplexTypeScriptModule1 complexTypeScriptModule1, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeScriptModule1, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeScriptModuleCondition(ComplexTypeScriptModuleCondition complexTypeScriptModuleCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeScriptModuleCondition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeScriptModuleParameter(ComplexTypeScriptModuleParameter complexTypeScriptModuleParameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeScriptModuleParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeTestCase(ComplexTypeTestCase complexTypeTestCase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeTestCase, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComplexTypeTestCasePostSteps(ComplexTypeTestCasePostSteps complexTypeTestCasePostSteps, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(complexTypeTestCasePostSteps, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDocumentRoot(DocumentRoot documentRoot, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(documentRoot, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeScriptCommandName(SimpleTypeScriptCommandName simpleTypeScriptCommandName, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeNonEmptyString(String simpleTypeNonEmptyString, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateSimpleTypeNonEmptyString_MinLength(simpleTypeNonEmptyString, diagnostics, context);
		return result;
	}

	/**
	 * Validates the MinLength constraint of '<em>Simple Type Non Empty String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeNonEmptyString_MinLength(String simpleTypeNonEmptyString, DiagnosticChain diagnostics, Map<Object, Object> context) {
		int length = simpleTypeNonEmptyString.length();
		boolean result = length >= 1;
		if (!result && diagnostics != null)
			reportMinLengthViolation(ScriptPackage.Literals.SIMPLE_TYPE_NON_EMPTY_STRING, simpleTypeNonEmptyString, length, 1, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeScriptCommandNameObject(SimpleTypeScriptCommandName simpleTypeScriptCommandNameObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeString128(String simpleTypeString128, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateSimpleTypeString128_MaxLength(simpleTypeString128, diagnostics, context);
		return result;
	}

	/**
	 * Validates the MaxLength constraint of '<em>Simple Type String128</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeString128_MaxLength(String simpleTypeString128, DiagnosticChain diagnostics, Map<Object, Object> context) {
		int length = simpleTypeString128.length();
		boolean result = length <= 128;
		if (!result && diagnostics != null)
			reportMaxLengthViolation(ScriptPackage.Literals.SIMPLE_TYPE_STRING128, simpleTypeString128, length, 128, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeVersionIdent(BigInteger simpleTypeVersionIdent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateSimpleTypeVersionIdent_Min(simpleTypeVersionIdent, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateSimpleTypeVersionIdent_Min
	 */
	public static final BigInteger SIMPLE_TYPE_VERSION_IDENT__MIN__VALUE = new BigInteger("2");

	/**
	 * Validates the Min constraint of '<em>Simple Type Version Ident</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleTypeVersionIdent_Min(BigInteger simpleTypeVersionIdent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = simpleTypeVersionIdent.compareTo(SIMPLE_TYPE_VERSION_IDENT__MIN__VALUE) >= 0;
		if (!result && diagnostics != null)
			reportMinViolation(ScriptPackage.Literals.SIMPLE_TYPE_VERSION_IDENT, simpleTypeVersionIdent, SIMPLE_TYPE_VERSION_IDENT__MIN__VALUE, true, diagnostics, context);
		return result;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ScriptValidator
