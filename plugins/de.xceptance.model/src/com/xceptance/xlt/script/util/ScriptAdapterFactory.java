/**
 */
package com.xceptance.xlt.script.util;

import com.xceptance.xlt.script.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.xceptance.xlt.script.ScriptPackage
 * @generated
 */
public class ScriptAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScriptPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScriptAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ScriptPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScriptSwitch<Adapter> modelSwitch =
		new ScriptSwitch<Adapter>() {
			@Override
			public Adapter caseComplexTypeJavaModule(ComplexTypeJavaModule object) {
				return createComplexTypeJavaModuleAdapter();
			}
			@Override
			public Adapter caseComplexTypeModuleParameter(ComplexTypeModuleParameter object) {
				return createComplexTypeModuleParameterAdapter();
			}
			@Override
			public Adapter caseComplexTypeScriptAction(ComplexTypeScriptAction object) {
				return createComplexTypeScriptActionAdapter();
			}
			@Override
			public Adapter caseComplexTypeScriptCommand(ComplexTypeScriptCommand object) {
				return createComplexTypeScriptCommandAdapter();
			}
			@Override
			public Adapter caseComplexTypeScriptModule(ComplexTypeScriptModule object) {
				return createComplexTypeScriptModuleAdapter();
			}
			@Override
			public Adapter caseComplexTypeScriptModule1(ComplexTypeScriptModule1 object) {
				return createComplexTypeScriptModule1Adapter();
			}
			@Override
			public Adapter caseComplexTypeScriptModuleCondition(ComplexTypeScriptModuleCondition object) {
				return createComplexTypeScriptModuleConditionAdapter();
			}
			@Override
			public Adapter caseComplexTypeScriptModuleParameter(ComplexTypeScriptModuleParameter object) {
				return createComplexTypeScriptModuleParameterAdapter();
			}
			@Override
			public Adapter caseComplexTypeTestCase(ComplexTypeTestCase object) {
				return createComplexTypeTestCaseAdapter();
			}
			@Override
			public Adapter caseComplexTypeTestCasePostSteps(ComplexTypeTestCasePostSteps object) {
				return createComplexTypeTestCasePostStepsAdapter();
			}
			@Override
			public Adapter caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeJavaModule <em>Complex Type Java Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule
	 * @generated
	 */
	public Adapter createComplexTypeJavaModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeModuleParameter <em>Complex Type Module Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeModuleParameter
	 * @generated
	 */
	public Adapter createComplexTypeModuleParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeScriptAction <em>Complex Type Script Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptAction
	 * @generated
	 */
	public Adapter createComplexTypeScriptActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand <em>Complex Type Script Command</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand
	 * @generated
	 */
	public Adapter createComplexTypeScriptCommandAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeScriptModule <em>Complex Type Script Module</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule
	 * @generated
	 */
	public Adapter createComplexTypeScriptModuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1 <em>Complex Type Script Module1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1
	 * @generated
	 */
	public Adapter createComplexTypeScriptModule1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition <em>Complex Type Script Module Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleCondition
	 * @generated
	 */
	public Adapter createComplexTypeScriptModuleConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleParameter <em>Complex Type Script Module Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleParameter
	 * @generated
	 */
	public Adapter createComplexTypeScriptModuleParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeTestCase <em>Complex Type Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase
	 * @generated
	 */
	public Adapter createComplexTypeTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps <em>Complex Type Test Case Post Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCasePostSteps
	 * @generated
	 */
	public Adapter createComplexTypeTestCasePostStepsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.xceptance.xlt.script.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.xceptance.xlt.script.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ScriptAdapterFactory
