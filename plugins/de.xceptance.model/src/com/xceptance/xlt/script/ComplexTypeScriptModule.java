/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Script Module</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getComment <em>Comment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule#isDisabled <em>Disabled</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule()
 * @model extendedMetaData="name='complexType.Script.Module' kind='elementOnly'"
 * @generated
 */
public interface ComplexTypeScriptModule extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule_Comment()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getComment();

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule_Condition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='condition' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<ComplexTypeScriptModuleCondition> getCondition();

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptModuleParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule_Parameter()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parameter' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<ComplexTypeScriptModuleParameter> getParameter();

	/**
	 * Returns the value of the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #setDisabled(boolean)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule_Disabled()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='disabled'"
	 * @generated
	 */
	boolean isDisabled();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @generated
	 */
	void setDisabled(boolean value);

	/**
	 * Unsets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	void unsetDisabled();

	/**
	 * Returns whether the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#isDisabled <em>Disabled</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Disabled</em>' attribute is set.
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	boolean isSetDisabled();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule_Name()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString" required="true"
	 *        extendedMetaData="kind='attribute' name='name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // ComplexTypeScriptModule
