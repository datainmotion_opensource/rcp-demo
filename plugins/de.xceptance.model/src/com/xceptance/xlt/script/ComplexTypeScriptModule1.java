/**
 */
package com.xceptance.xlt.script;

import java.math.BigInteger;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Script Module1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getTags <em>Tags</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getDescription <em>Description</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getGroupScriptActions <em>Group Script Actions</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getCommand <em>Command</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getAction <em>Action</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getModule <em>Module</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getCodecomment <em>Codecomment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getId <em>Id</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1()
 * @model extendedMetaData="name='complexType.ScriptModule' kind='elementOnly'"
 * @generated
 */
public interface ComplexTypeScriptModule1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Tags</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tags</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tags</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Tags()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tags' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getTags();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Description()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getDescription();

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeModuleParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Parameter()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parameter' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ComplexTypeModuleParameter> getParameter();

	/**
	 * Returns the value of the '<em><b>Group Script Actions</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Script Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Script Actions</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_GroupScriptActions()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='GroupScriptActions:4'"
	 * @generated
	 */
	FeatureMap getGroupScriptActions();

	/**
	 * Returns the value of the '<em><b>Command</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptCommand}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Command()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='command' namespace='##targetNamespace' group='#GroupScriptActions:4'"
	 * @generated
	 */
	EList<ComplexTypeScriptCommand> getCommand();

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Action()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='action' namespace='##targetNamespace' group='#GroupScriptActions:4'"
	 * @generated
	 */
	EList<ComplexTypeScriptAction> getAction();

	/**
	 * Returns the value of the '<em><b>Module</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptModule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Module()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='module' namespace='##targetNamespace' group='#GroupScriptActions:4'"
	 * @generated
	 */
	EList<ComplexTypeScriptModule> getModule();

	/**
	 * Returns the value of the '<em><b>Codecomment</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Codecomment</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Codecomment</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Codecomment()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='codecomment' namespace='##targetNamespace' group='#GroupScriptActions:4'"
	 * @generated
	 */
	EList<String> getCodecomment();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Id()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(BigInteger)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModule1_Version()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeVersionIdent" required="true"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	BigInteger getVersion();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(BigInteger value);

} // ComplexTypeScriptModule1
