/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Test Case Post Steps</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getGroupScriptActions <em>Group Script Actions</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getCommand <em>Command</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getAction <em>Action</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getModule <em>Module</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getCodecomment <em>Codecomment</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCasePostSteps()
 * @model extendedMetaData="name='complexType.TestCase.PostSteps' kind='elementOnly'"
 * @generated
 */
public interface ComplexTypeTestCasePostSteps extends EObject {
	/**
	 * Returns the value of the '<em><b>Group Script Actions</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Script Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Script Actions</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCasePostSteps_GroupScriptActions()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='GroupScriptActions:0'"
	 * @generated
	 */
	FeatureMap getGroupScriptActions();

	/**
	 * Returns the value of the '<em><b>Command</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptCommand}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCasePostSteps_Command()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='command' namespace='##targetNamespace' group='#GroupScriptActions:0'"
	 * @generated
	 */
	EList<ComplexTypeScriptCommand> getCommand();

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCasePostSteps_Action()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='action' namespace='##targetNamespace' group='#GroupScriptActions:0'"
	 * @generated
	 */
	EList<ComplexTypeScriptAction> getAction();

	/**
	 * Returns the value of the '<em><b>Module</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptModule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCasePostSteps_Module()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='module' namespace='##targetNamespace' group='#GroupScriptActions:0'"
	 * @generated
	 */
	EList<ComplexTypeScriptModule> getModule();

	/**
	 * Returns the value of the '<em><b>Codecomment</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Codecomment</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Codecomment</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCasePostSteps_Codecomment()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='codecomment' namespace='##targetNamespace' group='#GroupScriptActions:0'"
	 * @generated
	 */
	EList<String> getCodecomment();

} // ComplexTypeTestCasePostSteps
