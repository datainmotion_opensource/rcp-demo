/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.xceptance.xlt.script.ScriptPackage
 * @generated
 */
public interface ScriptFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScriptFactory eINSTANCE = com.xceptance.xlt.script.impl.ScriptFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Complex Type Java Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Java Module</em>'.
	 * @generated
	 */
	ComplexTypeJavaModule createComplexTypeJavaModule();

	/**
	 * Returns a new object of class '<em>Complex Type Module Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Module Parameter</em>'.
	 * @generated
	 */
	ComplexTypeModuleParameter createComplexTypeModuleParameter();

	/**
	 * Returns a new object of class '<em>Complex Type Script Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Script Action</em>'.
	 * @generated
	 */
	ComplexTypeScriptAction createComplexTypeScriptAction();

	/**
	 * Returns a new object of class '<em>Complex Type Script Command</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Script Command</em>'.
	 * @generated
	 */
	ComplexTypeScriptCommand createComplexTypeScriptCommand();

	/**
	 * Returns a new object of class '<em>Complex Type Script Module</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Script Module</em>'.
	 * @generated
	 */
	ComplexTypeScriptModule createComplexTypeScriptModule();

	/**
	 * Returns a new object of class '<em>Complex Type Script Module1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Script Module1</em>'.
	 * @generated
	 */
	ComplexTypeScriptModule1 createComplexTypeScriptModule1();

	/**
	 * Returns a new object of class '<em>Complex Type Script Module Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Script Module Condition</em>'.
	 * @generated
	 */
	ComplexTypeScriptModuleCondition createComplexTypeScriptModuleCondition();

	/**
	 * Returns a new object of class '<em>Complex Type Script Module Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Script Module Parameter</em>'.
	 * @generated
	 */
	ComplexTypeScriptModuleParameter createComplexTypeScriptModuleParameter();

	/**
	 * Returns a new object of class '<em>Complex Type Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Test Case</em>'.
	 * @generated
	 */
	ComplexTypeTestCase createComplexTypeTestCase();

	/**
	 * Returns a new object of class '<em>Complex Type Test Case Post Steps</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Type Test Case Post Steps</em>'.
	 * @generated
	 */
	ComplexTypeTestCasePostSteps createComplexTypeTestCasePostSteps();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ScriptPackage getScriptPackage();

} //ScriptFactory
