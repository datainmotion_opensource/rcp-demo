/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Script Module Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#getValue <em>Value</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#isDisabled <em>Disabled</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModuleCondition()
 * @model extendedMetaData="name='complexType.Script.Module.Condition' kind='simple'"
 * @generated
 */
public interface ComplexTypeScriptModuleCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModuleCondition_Value()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #setDisabled(boolean)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptModuleCondition_Disabled()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='disabled'"
	 * @generated
	 */
	boolean isDisabled();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @generated
	 */
	void setDisabled(boolean value);

	/**
	 * Unsets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	void unsetDisabled();

	/**
	 * Returns whether the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#isDisabled <em>Disabled</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Disabled</em>' attribute is set.
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	boolean isSetDisabled();

} // ComplexTypeScriptModuleCondition
