/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.xceptance.xlt.script.ScriptFactory
 * @model kind="package"
 * @generated
 */
public interface ScriptPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "script";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://xlt.xceptance.com/xlt-script/2";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "script";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScriptPackage eINSTANCE = com.xceptance.xlt.script.impl.ScriptPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl <em>Complex Type Java Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeJavaModule()
	 * @generated
	 */
	int COMPLEX_TYPE_JAVA_MODULE = 0;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE__TAGS = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE__PARAMETER = 3;

	/**
	 * The feature id for the '<em><b>Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE__CLASS = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE__ID = 5;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE__VERSION = 6;

	/**
	 * The number of structural features of the '<em>Complex Type Java Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Complex Type Java Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_JAVA_MODULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeModuleParameterImpl <em>Complex Type Module Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeModuleParameterImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeModuleParameter()
	 * @generated
	 */
	int COMPLEX_TYPE_MODULE_PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>Desc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_MODULE_PARAMETER__DESC = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_MODULE_PARAMETER__NAME = 1;

	/**
	 * The number of structural features of the '<em>Complex Type Module Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_MODULE_PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Complex Type Module Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_MODULE_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptActionImpl <em>Complex Type Script Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptActionImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptAction()
	 * @generated
	 */
	int COMPLEX_TYPE_SCRIPT_ACTION = 2;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_ACTION__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_ACTION__COMMENT = 1;

	/**
	 * The feature id for the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_ACTION__DISABLED = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_ACTION__NAME = 3;

	/**
	 * The number of structural features of the '<em>Complex Type Script Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_ACTION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Complex Type Script Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl <em>Complex Type Script Command</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptCommand()
	 * @generated
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND = 3;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__TARGET = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__VALUE = 2;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT = 3;

	/**
	 * The feature id for the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__NAME = 5;

	/**
	 * The feature id for the '<em><b>Target1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1 = 6;

	/**
	 * The feature id for the '<em><b>Value1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1 = 7;

	/**
	 * The number of structural features of the '<em>Complex Type Script Command</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Complex Type Script Command</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_COMMAND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl <em>Complex Type Script Module</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModule()
	 * @generated
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE = 4;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE__COMMENT = 1;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE__CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER = 3;

	/**
	 * The feature id for the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE__DISABLED = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE__NAME = 5;

	/**
	 * The number of structural features of the '<em>Complex Type Script Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Complex Type Script Module</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl <em>Complex Type Script Module1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModule1()
	 * @generated
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1 = 5;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__TAGS = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER = 3;

	/**
	 * The feature id for the '<em><b>Group Script Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS = 4;

	/**
	 * The feature id for the '<em><b>Command</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND = 5;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__ACTION = 6;

	/**
	 * The feature id for the '<em><b>Module</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__MODULE = 7;

	/**
	 * The feature id for the '<em><b>Codecomment</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__ID = 9;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1__VERSION = 10;

	/**
	 * The number of structural features of the '<em>Complex Type Script Module1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Complex Type Script Module1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE1_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleConditionImpl <em>Complex Type Script Module Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModuleConditionImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModuleCondition()
	 * @generated
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_CONDITION = 6;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_CONDITION__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_CONDITION__DISABLED = 1;

	/**
	 * The number of structural features of the '<em>Complex Type Script Module Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_CONDITION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Complex Type Script Module Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_CONDITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleParameterImpl <em>Complex Type Script Module Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModuleParameterImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModuleParameter()
	 * @generated
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Complex Type Script Module Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Complex Type Script Module Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl <em>Complex Type Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeTestCase()
	 * @generated
	 */
	int COMPLEX_TYPE_TEST_CASE = 8;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__TAGS = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Group Script Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS = 3;

	/**
	 * The feature id for the '<em><b>Command</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__COMMAND = 4;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__ACTION = 5;

	/**
	 * The feature id for the '<em><b>Module</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__MODULE = 6;

	/**
	 * The feature id for the '<em><b>Codecomment</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__CODECOMMENT = 7;

	/**
	 * The feature id for the '<em><b>Post Steps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__POST_STEPS = 8;

	/**
	 * The feature id for the '<em><b>Base URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__BASE_URL = 9;

	/**
	 * The feature id for the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__DISABLED = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__ID = 11;

	/**
	 * The feature id for the '<em><b>Junit Test</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__JUNIT_TEST = 12;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE__VERSION = 13;

	/**
	 * The number of structural features of the '<em>Complex Type Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_FEATURE_COUNT = 14;

	/**
	 * The number of operations of the '<em>Complex Type Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl <em>Complex Type Test Case Post Steps</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeTestCasePostSteps()
	 * @generated
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS = 9;

	/**
	 * The feature id for the '<em><b>Group Script Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS = 0;

	/**
	 * The feature id for the '<em><b>Command</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND = 1;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION = 2;

	/**
	 * The feature id for the '<em><b>Module</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE = 3;

	/**
	 * The feature id for the '<em><b>Codecomment</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT = 4;

	/**
	 * The number of structural features of the '<em>Complex Type Test Case Post Steps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Complex Type Test Case Post Steps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_TYPE_TEST_CASE_POST_STEPS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.impl.DocumentRootImpl
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 10;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Javamodule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__JAVAMODULE = 3;

	/**
	 * The feature id for the '<em><b>Scriptmodule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SCRIPTMODULE = 4;

	/**
	 * The feature id for the '<em><b>Testcase</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__TESTCASE = 5;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.xceptance.xlt.script.SimpleTypeScriptCommandName <em>Simple Type Script Command Name</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeScriptCommandName()
	 * @generated
	 */
	int SIMPLE_TYPE_SCRIPT_COMMAND_NAME = 11;

	/**
	 * The meta object id for the '<em>Simple Type Non Empty String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeNonEmptyString()
	 * @generated
	 */
	int SIMPLE_TYPE_NON_EMPTY_STRING = 12;

	/**
	 * The meta object id for the '<em>Simple Type Script Command Name Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeScriptCommandNameObject()
	 * @generated
	 */
	int SIMPLE_TYPE_SCRIPT_COMMAND_NAME_OBJECT = 13;

	/**
	 * The meta object id for the '<em>Simple Type String128</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeString128()
	 * @generated
	 */
	int SIMPLE_TYPE_STRING128 = 14;

	/**
	 * The meta object id for the '<em>Simple Type Version Ident</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.math.BigInteger
	 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeVersionIdent()
	 * @generated
	 */
	int SIMPLE_TYPE_VERSION_IDENT = 15;


	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeJavaModule <em>Complex Type Java Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Java Module</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule
	 * @generated
	 */
	EClass getComplexTypeJavaModule();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule#getGroup()
	 * @see #getComplexTypeJavaModule()
	 * @generated
	 */
	EAttribute getComplexTypeJavaModule_Group();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getTags <em>Tags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tags</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule#getTags()
	 * @see #getComplexTypeJavaModule()
	 * @generated
	 */
	EAttribute getComplexTypeJavaModule_Tags();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Description</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule#getDescription()
	 * @see #getComplexTypeJavaModule()
	 * @generated
	 */
	EAttribute getComplexTypeJavaModule_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule#getParameter()
	 * @see #getComplexTypeJavaModule()
	 * @generated
	 */
	EReference getComplexTypeJavaModule_Parameter();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule#getClass_()
	 * @see #getComplexTypeJavaModule()
	 * @generated
	 */
	EAttribute getComplexTypeJavaModule_Class();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule#getId()
	 * @see #getComplexTypeJavaModule()
	 * @generated
	 */
	EAttribute getComplexTypeJavaModule_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeJavaModule#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeJavaModule#getVersion()
	 * @see #getComplexTypeJavaModule()
	 * @generated
	 */
	EAttribute getComplexTypeJavaModule_Version();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeModuleParameter <em>Complex Type Module Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Module Parameter</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeModuleParameter
	 * @generated
	 */
	EClass getComplexTypeModuleParameter();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeModuleParameter#getDesc <em>Desc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Desc</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeModuleParameter#getDesc()
	 * @see #getComplexTypeModuleParameter()
	 * @generated
	 */
	EAttribute getComplexTypeModuleParameter_Desc();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeModuleParameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeModuleParameter#getName()
	 * @see #getComplexTypeModuleParameter()
	 * @generated
	 */
	EAttribute getComplexTypeModuleParameter_Name();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeScriptAction <em>Complex Type Script Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Script Action</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptAction
	 * @generated
	 */
	EClass getComplexTypeScriptAction();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptAction#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptAction#getGroup()
	 * @see #getComplexTypeScriptAction()
	 * @generated
	 */
	EAttribute getComplexTypeScriptAction_Group();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptAction#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Comment</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptAction#getComment()
	 * @see #getComplexTypeScriptAction()
	 * @generated
	 */
	EAttribute getComplexTypeScriptAction_Comment();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptAction#isDisabled <em>Disabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disabled</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptAction#isDisabled()
	 * @see #getComplexTypeScriptAction()
	 * @generated
	 */
	EAttribute getComplexTypeScriptAction_Disabled();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptAction#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptAction#getName()
	 * @see #getComplexTypeScriptAction()
	 * @generated
	 */
	EAttribute getComplexTypeScriptAction_Name();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand <em>Complex Type Script Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Script Command</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand
	 * @generated
	 */
	EClass getComplexTypeScriptCommand();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#getGroup()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Group();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Target</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#getTarget()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Target();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#getValue()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Value();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Comment</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#getComment()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Comment();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#isDisabled <em>Disabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disabled</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#isDisabled()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Disabled();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#getName()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getTarget1 <em>Target1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target1</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#getTarget1()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Target1();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getValue1 <em>Value1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value1</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptCommand#getValue1()
	 * @see #getComplexTypeScriptCommand()
	 * @generated
	 */
	EAttribute getComplexTypeScriptCommand_Value1();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeScriptModule <em>Complex Type Script Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Script Module</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule
	 * @generated
	 */
	EClass getComplexTypeScriptModule();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule#getGroup()
	 * @see #getComplexTypeScriptModule()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule_Group();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Comment</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule#getComment()
	 * @see #getComplexTypeScriptModule()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule_Comment();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule#getCondition()
	 * @see #getComplexTypeScriptModule()
	 * @generated
	 */
	EReference getComplexTypeScriptModule_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule#getParameter()
	 * @see #getComplexTypeScriptModule()
	 * @generated
	 */
	EReference getComplexTypeScriptModule_Parameter();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#isDisabled <em>Disabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disabled</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule#isDisabled()
	 * @see #getComplexTypeScriptModule()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule_Disabled();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModule#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule#getName()
	 * @see #getComplexTypeScriptModule()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule_Name();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1 <em>Complex Type Script Module1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Script Module1</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1
	 * @generated
	 */
	EClass getComplexTypeScriptModule1();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getGroup()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule1_Group();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getTags <em>Tags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tags</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getTags()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule1_Tags();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Description</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getDescription()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule1_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getParameter()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EReference getComplexTypeScriptModule1_Parameter();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getGroupScriptActions <em>Group Script Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group Script Actions</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getGroupScriptActions()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule1_GroupScriptActions();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getCommand <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Command</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getCommand()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EReference getComplexTypeScriptModule1_Command();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getAction()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EReference getComplexTypeScriptModule1_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getModule <em>Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Module</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getModule()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EReference getComplexTypeScriptModule1_Module();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getCodecomment <em>Codecomment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Codecomment</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getCodecomment()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule1_Codecomment();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getId()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule1_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModule1#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModule1#getVersion()
	 * @see #getComplexTypeScriptModule1()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModule1_Version();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition <em>Complex Type Script Module Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Script Module Condition</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleCondition
	 * @generated
	 */
	EClass getComplexTypeScriptModuleCondition();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#getValue()
	 * @see #getComplexTypeScriptModuleCondition()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModuleCondition_Value();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#isDisabled <em>Disabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disabled</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleCondition#isDisabled()
	 * @see #getComplexTypeScriptModuleCondition()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModuleCondition_Disabled();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleParameter <em>Complex Type Script Module Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Script Module Parameter</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleParameter
	 * @generated
	 */
	EClass getComplexTypeScriptModuleParameter();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleParameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleParameter#getName()
	 * @see #getComplexTypeScriptModuleParameter()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModuleParameter_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeScriptModuleParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeScriptModuleParameter#getValue()
	 * @see #getComplexTypeScriptModuleParameter()
	 * @generated
	 */
	EAttribute getComplexTypeScriptModuleParameter_Value();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeTestCase <em>Complex Type Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Test Case</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase
	 * @generated
	 */
	EClass getComplexTypeTestCase();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getGroup()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_Group();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getTags <em>Tags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tags</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getTags()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_Tags();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Description</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getDescription()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_Description();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getGroupScriptActions <em>Group Script Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group Script Actions</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getGroupScriptActions()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_GroupScriptActions();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getCommand <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Command</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getCommand()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EReference getComplexTypeTestCase_Command();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getAction()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EReference getComplexTypeTestCase_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getModule <em>Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Module</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getModule()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EReference getComplexTypeTestCase_Module();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getCodecomment <em>Codecomment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Codecomment</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getCodecomment()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_Codecomment();

	/**
	 * Returns the meta object for the containment reference '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getPostSteps <em>Post Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Post Steps</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getPostSteps()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EReference getComplexTypeTestCase_PostSteps();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getBaseURL <em>Base URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base URL</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getBaseURL()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_BaseURL();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isDisabled <em>Disabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disabled</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#isDisabled()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_Disabled();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getId()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isJunitTest <em>Junit Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Junit Test</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#isJunitTest()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_JunitTest();

	/**
	 * Returns the meta object for the attribute '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCase#getVersion()
	 * @see #getComplexTypeTestCase()
	 * @generated
	 */
	EAttribute getComplexTypeTestCase_Version();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps <em>Complex Type Test Case Post Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Type Test Case Post Steps</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCasePostSteps
	 * @generated
	 */
	EClass getComplexTypeTestCasePostSteps();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getGroupScriptActions <em>Group Script Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group Script Actions</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getGroupScriptActions()
	 * @see #getComplexTypeTestCasePostSteps()
	 * @generated
	 */
	EAttribute getComplexTypeTestCasePostSteps_GroupScriptActions();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getCommand <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Command</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getCommand()
	 * @see #getComplexTypeTestCasePostSteps()
	 * @generated
	 */
	EReference getComplexTypeTestCasePostSteps_Command();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getAction()
	 * @see #getComplexTypeTestCasePostSteps()
	 * @generated
	 */
	EReference getComplexTypeTestCasePostSteps_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getModule <em>Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Module</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getModule()
	 * @see #getComplexTypeTestCasePostSteps()
	 * @generated
	 */
	EReference getComplexTypeTestCasePostSteps_Module();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getCodecomment <em>Codecomment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Codecomment</em>'.
	 * @see com.xceptance.xlt.script.ComplexTypeTestCasePostSteps#getCodecomment()
	 * @see #getComplexTypeTestCasePostSteps()
	 * @generated
	 */
	EAttribute getComplexTypeTestCasePostSteps_Codecomment();

	/**
	 * Returns the meta object for class '{@link com.xceptance.xlt.script.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see com.xceptance.xlt.script.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link com.xceptance.xlt.script.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see com.xceptance.xlt.script.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link com.xceptance.xlt.script.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see com.xceptance.xlt.script.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link com.xceptance.xlt.script.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see com.xceptance.xlt.script.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link com.xceptance.xlt.script.DocumentRoot#getJavamodule <em>Javamodule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Javamodule</em>'.
	 * @see com.xceptance.xlt.script.DocumentRoot#getJavamodule()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Javamodule();

	/**
	 * Returns the meta object for the containment reference '{@link com.xceptance.xlt.script.DocumentRoot#getScriptmodule <em>Scriptmodule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scriptmodule</em>'.
	 * @see com.xceptance.xlt.script.DocumentRoot#getScriptmodule()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Scriptmodule();

	/**
	 * Returns the meta object for the containment reference '{@link com.xceptance.xlt.script.DocumentRoot#getTestcase <em>Testcase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Testcase</em>'.
	 * @see com.xceptance.xlt.script.DocumentRoot#getTestcase()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Testcase();

	/**
	 * Returns the meta object for enum '{@link com.xceptance.xlt.script.SimpleTypeScriptCommandName <em>Simple Type Script Command Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Simple Type Script Command Name</em>'.
	 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
	 * @generated
	 */
	EEnum getSimpleTypeScriptCommandName();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Simple Type Non Empty String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Simple Type Non Empty String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='simpleType.NonEmptyString' baseType='http://www.eclipse.org/emf/2003/XMLType#string' minLength='1'"
	 * @generated
	 */
	EDataType getSimpleTypeNonEmptyString();

	/**
	 * Returns the meta object for data type '{@link com.xceptance.xlt.script.SimpleTypeScriptCommandName <em>Simple Type Script Command Name Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Simple Type Script Command Name Object</em>'.
	 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
	 * @model instanceClass="com.xceptance.xlt.script.SimpleTypeScriptCommandName"
	 *        extendedMetaData="name='simpleType.Script.Command.Name:Object' baseType='simpleType.Script.Command.Name'"
	 * @generated
	 */
	EDataType getSimpleTypeScriptCommandNameObject();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Simple Type String128</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Simple Type String128</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='simpleType.String.128' baseType='http://www.eclipse.org/emf/2003/XMLType#string' maxLength='128'"
	 * @generated
	 */
	EDataType getSimpleTypeString128();

	/**
	 * Returns the meta object for data type '{@link java.math.BigInteger <em>Simple Type Version Ident</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Simple Type Version Ident</em>'.
	 * @see java.math.BigInteger
	 * @model instanceClass="java.math.BigInteger"
	 *        extendedMetaData="name='simpleType.VersionIdent' baseType='http://www.eclipse.org/emf/2003/XMLType#positiveInteger' minInclusive='2'"
	 * @generated
	 */
	EDataType getSimpleTypeVersionIdent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScriptFactory getScriptFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl <em>Complex Type Java Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeJavaModule()
		 * @generated
		 */
		EClass COMPLEX_TYPE_JAVA_MODULE = eINSTANCE.getComplexTypeJavaModule();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_JAVA_MODULE__GROUP = eINSTANCE.getComplexTypeJavaModule_Group();

		/**
		 * The meta object literal for the '<em><b>Tags</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_JAVA_MODULE__TAGS = eINSTANCE.getComplexTypeJavaModule_Tags();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION = eINSTANCE.getComplexTypeJavaModule_Description();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_JAVA_MODULE__PARAMETER = eINSTANCE.getComplexTypeJavaModule_Parameter();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_JAVA_MODULE__CLASS = eINSTANCE.getComplexTypeJavaModule_Class();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_JAVA_MODULE__ID = eINSTANCE.getComplexTypeJavaModule_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_JAVA_MODULE__VERSION = eINSTANCE.getComplexTypeJavaModule_Version();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeModuleParameterImpl <em>Complex Type Module Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeModuleParameterImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeModuleParameter()
		 * @generated
		 */
		EClass COMPLEX_TYPE_MODULE_PARAMETER = eINSTANCE.getComplexTypeModuleParameter();

		/**
		 * The meta object literal for the '<em><b>Desc</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_MODULE_PARAMETER__DESC = eINSTANCE.getComplexTypeModuleParameter_Desc();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_MODULE_PARAMETER__NAME = eINSTANCE.getComplexTypeModuleParameter_Name();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptActionImpl <em>Complex Type Script Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptActionImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptAction()
		 * @generated
		 */
		EClass COMPLEX_TYPE_SCRIPT_ACTION = eINSTANCE.getComplexTypeScriptAction();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_ACTION__GROUP = eINSTANCE.getComplexTypeScriptAction_Group();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_ACTION__COMMENT = eINSTANCE.getComplexTypeScriptAction_Comment();

		/**
		 * The meta object literal for the '<em><b>Disabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_ACTION__DISABLED = eINSTANCE.getComplexTypeScriptAction_Disabled();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_ACTION__NAME = eINSTANCE.getComplexTypeScriptAction_Name();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl <em>Complex Type Script Command</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptCommand()
		 * @generated
		 */
		EClass COMPLEX_TYPE_SCRIPT_COMMAND = eINSTANCE.getComplexTypeScriptCommand();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__GROUP = eINSTANCE.getComplexTypeScriptCommand_Group();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__TARGET = eINSTANCE.getComplexTypeScriptCommand_Target();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__VALUE = eINSTANCE.getComplexTypeScriptCommand_Value();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT = eINSTANCE.getComplexTypeScriptCommand_Comment();

		/**
		 * The meta object literal for the '<em><b>Disabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED = eINSTANCE.getComplexTypeScriptCommand_Disabled();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__NAME = eINSTANCE.getComplexTypeScriptCommand_Name();

		/**
		 * The meta object literal for the '<em><b>Target1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1 = eINSTANCE.getComplexTypeScriptCommand_Target1();

		/**
		 * The meta object literal for the '<em><b>Value1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1 = eINSTANCE.getComplexTypeScriptCommand_Value1();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl <em>Complex Type Script Module</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModule()
		 * @generated
		 */
		EClass COMPLEX_TYPE_SCRIPT_MODULE = eINSTANCE.getComplexTypeScriptModule();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE__GROUP = eINSTANCE.getComplexTypeScriptModule_Group();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE__COMMENT = eINSTANCE.getComplexTypeScriptModule_Comment();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_SCRIPT_MODULE__CONDITION = eINSTANCE.getComplexTypeScriptModule_Condition();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER = eINSTANCE.getComplexTypeScriptModule_Parameter();

		/**
		 * The meta object literal for the '<em><b>Disabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE__DISABLED = eINSTANCE.getComplexTypeScriptModule_Disabled();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE__NAME = eINSTANCE.getComplexTypeScriptModule_Name();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl <em>Complex Type Script Module1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModule1()
		 * @generated
		 */
		EClass COMPLEX_TYPE_SCRIPT_MODULE1 = eINSTANCE.getComplexTypeScriptModule1();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE1__GROUP = eINSTANCE.getComplexTypeScriptModule1_Group();

		/**
		 * The meta object literal for the '<em><b>Tags</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE1__TAGS = eINSTANCE.getComplexTypeScriptModule1_Tags();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION = eINSTANCE.getComplexTypeScriptModule1_Description();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER = eINSTANCE.getComplexTypeScriptModule1_Parameter();

		/**
		 * The meta object literal for the '<em><b>Group Script Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS = eINSTANCE.getComplexTypeScriptModule1_GroupScriptActions();

		/**
		 * The meta object literal for the '<em><b>Command</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND = eINSTANCE.getComplexTypeScriptModule1_Command();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_SCRIPT_MODULE1__ACTION = eINSTANCE.getComplexTypeScriptModule1_Action();

		/**
		 * The meta object literal for the '<em><b>Module</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_SCRIPT_MODULE1__MODULE = eINSTANCE.getComplexTypeScriptModule1_Module();

		/**
		 * The meta object literal for the '<em><b>Codecomment</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT = eINSTANCE.getComplexTypeScriptModule1_Codecomment();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE1__ID = eINSTANCE.getComplexTypeScriptModule1_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE1__VERSION = eINSTANCE.getComplexTypeScriptModule1_Version();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleConditionImpl <em>Complex Type Script Module Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModuleConditionImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModuleCondition()
		 * @generated
		 */
		EClass COMPLEX_TYPE_SCRIPT_MODULE_CONDITION = eINSTANCE.getComplexTypeScriptModuleCondition();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE_CONDITION__VALUE = eINSTANCE.getComplexTypeScriptModuleCondition_Value();

		/**
		 * The meta object literal for the '<em><b>Disabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE_CONDITION__DISABLED = eINSTANCE.getComplexTypeScriptModuleCondition_Disabled();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleParameterImpl <em>Complex Type Script Module Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeScriptModuleParameterImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeScriptModuleParameter()
		 * @generated
		 */
		EClass COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER = eINSTANCE.getComplexTypeScriptModuleParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER__NAME = eINSTANCE.getComplexTypeScriptModuleParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER__VALUE = eINSTANCE.getComplexTypeScriptModuleParameter_Value();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl <em>Complex Type Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeTestCase()
		 * @generated
		 */
		EClass COMPLEX_TYPE_TEST_CASE = eINSTANCE.getComplexTypeTestCase();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__GROUP = eINSTANCE.getComplexTypeTestCase_Group();

		/**
		 * The meta object literal for the '<em><b>Tags</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__TAGS = eINSTANCE.getComplexTypeTestCase_Tags();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__DESCRIPTION = eINSTANCE.getComplexTypeTestCase_Description();

		/**
		 * The meta object literal for the '<em><b>Group Script Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS = eINSTANCE.getComplexTypeTestCase_GroupScriptActions();

		/**
		 * The meta object literal for the '<em><b>Command</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_TEST_CASE__COMMAND = eINSTANCE.getComplexTypeTestCase_Command();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_TEST_CASE__ACTION = eINSTANCE.getComplexTypeTestCase_Action();

		/**
		 * The meta object literal for the '<em><b>Module</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_TEST_CASE__MODULE = eINSTANCE.getComplexTypeTestCase_Module();

		/**
		 * The meta object literal for the '<em><b>Codecomment</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__CODECOMMENT = eINSTANCE.getComplexTypeTestCase_Codecomment();

		/**
		 * The meta object literal for the '<em><b>Post Steps</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_TEST_CASE__POST_STEPS = eINSTANCE.getComplexTypeTestCase_PostSteps();

		/**
		 * The meta object literal for the '<em><b>Base URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__BASE_URL = eINSTANCE.getComplexTypeTestCase_BaseURL();

		/**
		 * The meta object literal for the '<em><b>Disabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__DISABLED = eINSTANCE.getComplexTypeTestCase_Disabled();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__ID = eINSTANCE.getComplexTypeTestCase_Id();

		/**
		 * The meta object literal for the '<em><b>Junit Test</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__JUNIT_TEST = eINSTANCE.getComplexTypeTestCase_JunitTest();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE__VERSION = eINSTANCE.getComplexTypeTestCase_Version();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl <em>Complex Type Test Case Post Steps</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getComplexTypeTestCasePostSteps()
		 * @generated
		 */
		EClass COMPLEX_TYPE_TEST_CASE_POST_STEPS = eINSTANCE.getComplexTypeTestCasePostSteps();

		/**
		 * The meta object literal for the '<em><b>Group Script Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS = eINSTANCE.getComplexTypeTestCasePostSteps_GroupScriptActions();

		/**
		 * The meta object literal for the '<em><b>Command</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND = eINSTANCE.getComplexTypeTestCasePostSteps_Command();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION = eINSTANCE.getComplexTypeTestCasePostSteps_Action();

		/**
		 * The meta object literal for the '<em><b>Module</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE = eINSTANCE.getComplexTypeTestCasePostSteps_Module();

		/**
		 * The meta object literal for the '<em><b>Codecomment</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT = eINSTANCE.getComplexTypeTestCasePostSteps_Codecomment();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.impl.DocumentRootImpl
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Javamodule</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__JAVAMODULE = eINSTANCE.getDocumentRoot_Javamodule();

		/**
		 * The meta object literal for the '<em><b>Scriptmodule</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__SCRIPTMODULE = eINSTANCE.getDocumentRoot_Scriptmodule();

		/**
		 * The meta object literal for the '<em><b>Testcase</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__TESTCASE = eINSTANCE.getDocumentRoot_Testcase();

		/**
		 * The meta object literal for the '{@link com.xceptance.xlt.script.SimpleTypeScriptCommandName <em>Simple Type Script Command Name</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeScriptCommandName()
		 * @generated
		 */
		EEnum SIMPLE_TYPE_SCRIPT_COMMAND_NAME = eINSTANCE.getSimpleTypeScriptCommandName();

		/**
		 * The meta object literal for the '<em>Simple Type Non Empty String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeNonEmptyString()
		 * @generated
		 */
		EDataType SIMPLE_TYPE_NON_EMPTY_STRING = eINSTANCE.getSimpleTypeNonEmptyString();

		/**
		 * The meta object literal for the '<em>Simple Type Script Command Name Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeScriptCommandNameObject()
		 * @generated
		 */
		EDataType SIMPLE_TYPE_SCRIPT_COMMAND_NAME_OBJECT = eINSTANCE.getSimpleTypeScriptCommandNameObject();

		/**
		 * The meta object literal for the '<em>Simple Type String128</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeString128()
		 * @generated
		 */
		EDataType SIMPLE_TYPE_STRING128 = eINSTANCE.getSimpleTypeString128();

		/**
		 * The meta object literal for the '<em>Simple Type Version Ident</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.math.BigInteger
		 * @see com.xceptance.xlt.script.impl.ScriptPackageImpl#getSimpleTypeVersionIdent()
		 * @generated
		 */
		EDataType SIMPLE_TYPE_VERSION_IDENT = eINSTANCE.getSimpleTypeVersionIdent();

	}

} //ScriptPackage
