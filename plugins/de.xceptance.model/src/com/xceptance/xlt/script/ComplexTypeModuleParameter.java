/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Module Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeModuleParameter#getDesc <em>Desc</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeModuleParameter#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeModuleParameter()
 * @model extendedMetaData="name='complexType.Module.Parameter' kind='empty'"
 * @generated
 */
public interface ComplexTypeModuleParameter extends EObject {
	/**
	 * Returns the value of the '<em><b>Desc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Desc</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Desc</em>' attribute.
	 * @see #setDesc(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeModuleParameter_Desc()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString"
	 *        extendedMetaData="kind='attribute' name='desc'"
	 * @generated
	 */
	String getDesc();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeModuleParameter#getDesc <em>Desc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Desc</em>' attribute.
	 * @see #getDesc()
	 * @generated
	 */
	void setDesc(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeModuleParameter_Name()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString" required="true"
	 *        extendedMetaData="kind='attribute' name='name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeModuleParameter#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // ComplexTypeModuleParameter
