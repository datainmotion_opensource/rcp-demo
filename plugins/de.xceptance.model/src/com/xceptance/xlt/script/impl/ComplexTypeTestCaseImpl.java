/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.ComplexTypeScriptAction;
import com.xceptance.xlt.script.ComplexTypeScriptCommand;
import com.xceptance.xlt.script.ComplexTypeScriptModule;
import com.xceptance.xlt.script.ComplexTypeTestCase;
import com.xceptance.xlt.script.ComplexTypeTestCasePostSteps;
import com.xceptance.xlt.script.ScriptPackage;

import java.math.BigInteger;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Type Test Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getGroupScriptActions <em>Group Script Actions</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getCommand <em>Command</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getAction <em>Action</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getModule <em>Module</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getCodecomment <em>Codecomment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getPostSteps <em>Post Steps</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getBaseURL <em>Base URL</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#isDisabled <em>Disabled</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#isJunitTest <em>Junit Test</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCaseImpl#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexTypeTestCaseImpl extends MinimalEObjectImpl.Container implements ComplexTypeTestCase {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The cached value of the '{@link #getGroupScriptActions() <em>Group Script Actions</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupScriptActions()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap groupScriptActions;

	/**
	 * The cached value of the '{@link #getPostSteps() <em>Post Steps</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostSteps()
	 * @generated
	 * @ordered
	 */
	protected ComplexTypeTestCasePostSteps postSteps;

	/**
	 * The default value of the '{@link #getBaseURL() <em>Base URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseURL()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBaseURL() <em>Base URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseURL()
	 * @generated
	 * @ordered
	 */
	protected String baseURL = BASE_URL_EDEFAULT;

	/**
	 * The default value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected boolean disabled = DISABLED_EDEFAULT;

	/**
	 * This is true if the Disabled attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean disabledESet;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isJunitTest() <em>Junit Test</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isJunitTest()
	 * @generated
	 * @ordered
	 */
	protected static final boolean JUNIT_TEST_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isJunitTest() <em>Junit Test</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isJunitTest()
	 * @generated
	 * @ordered
	 */
	protected boolean junitTest = JUNIT_TEST_EDEFAULT;

	/**
	 * This is true if the Junit Test attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean junitTestESet;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected BigInteger version = VERSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexTypeTestCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTags() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__TAGS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getDescription() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__DESCRIPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroupScriptActions() {
		if (groupScriptActions == null) {
			groupScriptActions = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS);
		}
		return groupScriptActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptCommand> getCommand() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__COMMAND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptAction> getAction() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptModule> getModule() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__MODULE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getCodecomment() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__CODECOMMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeTestCasePostSteps getPostSteps() {
		return postSteps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPostSteps(ComplexTypeTestCasePostSteps newPostSteps, NotificationChain msgs) {
		ComplexTypeTestCasePostSteps oldPostSteps = postSteps;
		postSteps = newPostSteps;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS, oldPostSteps, newPostSteps);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostSteps(ComplexTypeTestCasePostSteps newPostSteps) {
		if (newPostSteps != postSteps) {
			NotificationChain msgs = null;
			if (postSteps != null)
				msgs = ((InternalEObject)postSteps).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS, null, msgs);
			if (newPostSteps != null)
				msgs = ((InternalEObject)newPostSteps).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS, null, msgs);
			msgs = basicSetPostSteps(newPostSteps, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS, newPostSteps, newPostSteps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBaseURL() {
		return baseURL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseURL(String newBaseURL) {
		String oldBaseURL = baseURL;
		baseURL = newBaseURL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__BASE_URL, oldBaseURL, baseURL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisabled(boolean newDisabled) {
		boolean oldDisabled = disabled;
		disabled = newDisabled;
		boolean oldDisabledESet = disabledESet;
		disabledESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__DISABLED, oldDisabled, disabled, !oldDisabledESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDisabled() {
		boolean oldDisabled = disabled;
		boolean oldDisabledESet = disabledESet;
		disabled = DISABLED_EDEFAULT;
		disabledESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__DISABLED, oldDisabled, DISABLED_EDEFAULT, oldDisabledESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDisabled() {
		return disabledESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isJunitTest() {
		return junitTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJunitTest(boolean newJunitTest) {
		boolean oldJunitTest = junitTest;
		junitTest = newJunitTest;
		boolean oldJunitTestESet = junitTestESet;
		junitTestESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST, oldJunitTest, junitTest, !oldJunitTestESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetJunitTest() {
		boolean oldJunitTest = junitTest;
		boolean oldJunitTestESet = junitTestESet;
		junitTest = JUNIT_TEST_EDEFAULT;
		junitTestESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST, oldJunitTest, JUNIT_TEST_EDEFAULT, oldJunitTestESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetJunitTest() {
		return junitTestESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(BigInteger newVersion) {
		BigInteger oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_TEST_CASE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS:
				return ((InternalEList<?>)getGroupScriptActions()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__COMMAND:
				return ((InternalEList<?>)getCommand()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ACTION:
				return ((InternalEList<?>)getAction()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__MODULE:
				return ((InternalEList<?>)getModule()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS:
				return basicSetPostSteps(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__TAGS:
				return getTags();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DESCRIPTION:
				return getDescription();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS:
				if (coreType) return getGroupScriptActions();
				return ((FeatureMap.Internal)getGroupScriptActions()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__COMMAND:
				return getCommand();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ACTION:
				return getAction();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__MODULE:
				return getModule();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__CODECOMMENT:
				return getCodecomment();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS:
				return getPostSteps();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__BASE_URL:
				return getBaseURL();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DISABLED:
				return isDisabled();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ID:
				return getId();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST:
				return isJunitTest();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__VERSION:
				return getVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS:
				((FeatureMap.Internal)getGroupScriptActions()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__COMMAND:
				getCommand().clear();
				getCommand().addAll((Collection<? extends ComplexTypeScriptCommand>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ACTION:
				getAction().clear();
				getAction().addAll((Collection<? extends ComplexTypeScriptAction>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__MODULE:
				getModule().clear();
				getModule().addAll((Collection<? extends ComplexTypeScriptModule>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__CODECOMMENT:
				getCodecomment().clear();
				getCodecomment().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS:
				setPostSteps((ComplexTypeTestCasePostSteps)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__BASE_URL:
				setBaseURL((String)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DISABLED:
				setDisabled((Boolean)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ID:
				setId((String)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST:
				setJunitTest((Boolean)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__VERSION:
				setVersion((BigInteger)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP:
				getGroup().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__TAGS:
				getTags().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DESCRIPTION:
				getDescription().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS:
				getGroupScriptActions().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__COMMAND:
				getCommand().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ACTION:
				getAction().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__MODULE:
				getModule().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__CODECOMMENT:
				getCodecomment().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS:
				setPostSteps((ComplexTypeTestCasePostSteps)null);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__BASE_URL:
				setBaseURL(BASE_URL_EDEFAULT);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DISABLED:
				unsetDisabled();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ID:
				setId(ID_EDEFAULT);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST:
				unsetJunitTest();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP:
				return group != null && !group.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__TAGS:
				return !getTags().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DESCRIPTION:
				return !getDescription().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS:
				return groupScriptActions != null && !groupScriptActions.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__COMMAND:
				return !getCommand().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ACTION:
				return !getAction().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__MODULE:
				return !getModule().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__CODECOMMENT:
				return !getCodecomment().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS:
				return postSteps != null;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__BASE_URL:
				return BASE_URL_EDEFAULT == null ? baseURL != null : !BASE_URL_EDEFAULT.equals(baseURL);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DISABLED:
				return isSetDisabled();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST:
				return isSetJunitTest();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", groupScriptActions: ");
		result.append(groupScriptActions);
		result.append(", baseURL: ");
		result.append(baseURL);
		result.append(", disabled: ");
		if (disabledESet) result.append(disabled); else result.append("<unset>");
		result.append(", id: ");
		result.append(id);
		result.append(", junitTest: ");
		if (junitTestESet) result.append(junitTest); else result.append("<unset>");
		result.append(", version: ");
		result.append(version);
		result.append(')');
		return result.toString();
	}

} //ComplexTypeTestCaseImpl
