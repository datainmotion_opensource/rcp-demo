/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.ComplexTypeScriptModule;
import com.xceptance.xlt.script.ComplexTypeScriptModuleCondition;
import com.xceptance.xlt.script.ComplexTypeScriptModuleParameter;
import com.xceptance.xlt.script.ScriptPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Type Script Module</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl#isDisabled <em>Disabled</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModuleImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexTypeScriptModuleImpl extends MinimalEObjectImpl.Container implements ComplexTypeScriptModule {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected boolean disabled = DISABLED_EDEFAULT;

	/**
	 * This is true if the Disabled attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean disabledESet;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexTypeScriptModuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getComment() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE__COMMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptModuleCondition> getCondition() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE__CONDITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptModuleParameter> getParameter() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisabled(boolean newDisabled) {
		boolean oldDisabled = disabled;
		disabled = newDisabled;
		boolean oldDisabledESet = disabledESet;
		disabledESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__DISABLED, oldDisabled, disabled, !oldDisabledESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDisabled() {
		boolean oldDisabled = disabled;
		boolean oldDisabledESet = disabledESet;
		disabled = DISABLED_EDEFAULT;
		disabledESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__DISABLED, oldDisabled, DISABLED_EDEFAULT, oldDisabledESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDisabled() {
		return disabledESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__CONDITION:
				return ((InternalEList<?>)getCondition()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER:
				return ((InternalEList<?>)getParameter()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__COMMENT:
				return getComment();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__CONDITION:
				return getCondition();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER:
				return getParameter();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__DISABLED:
				return isDisabled();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__CONDITION:
				getCondition().clear();
				getCondition().addAll((Collection<? extends ComplexTypeScriptModuleCondition>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER:
				getParameter().clear();
				getParameter().addAll((Collection<? extends ComplexTypeScriptModuleParameter>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__DISABLED:
				setDisabled((Boolean)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__GROUP:
				getGroup().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__COMMENT:
				getComment().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__CONDITION:
				getCondition().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER:
				getParameter().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__DISABLED:
				unsetDisabled();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__GROUP:
				return group != null && !group.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__COMMENT:
				return !getComment().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__CONDITION:
				return !getCondition().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER:
				return !getParameter().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__DISABLED:
				return isSetDisabled();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", disabled: ");
		if (disabledESet) result.append(disabled); else result.append("<unset>");
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ComplexTypeScriptModuleImpl
