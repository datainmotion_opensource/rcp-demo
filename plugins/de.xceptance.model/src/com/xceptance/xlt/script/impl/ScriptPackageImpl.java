/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.project.ProjectPackage;

import com.xceptance.project.impl.ProjectPackageImpl;

import com.xceptance.xlt.data.DataPackage;

import com.xceptance.xlt.data.impl.DataPackageImpl;

import com.xceptance.xlt.script.ComplexTypeJavaModule;
import com.xceptance.xlt.script.ComplexTypeModuleParameter;
import com.xceptance.xlt.script.ComplexTypeScriptAction;
import com.xceptance.xlt.script.ComplexTypeScriptCommand;
import com.xceptance.xlt.script.ComplexTypeScriptModule;
import com.xceptance.xlt.script.ComplexTypeScriptModule1;
import com.xceptance.xlt.script.ComplexTypeScriptModuleCondition;
import com.xceptance.xlt.script.ComplexTypeScriptModuleParameter;
import com.xceptance.xlt.script.ComplexTypeTestCase;
import com.xceptance.xlt.script.ComplexTypeTestCasePostSteps;
import com.xceptance.xlt.script.DocumentRoot;
import com.xceptance.xlt.script.ScriptFactory;
import com.xceptance.xlt.script.ScriptPackage;
import com.xceptance.xlt.script.SimpleTypeScriptCommandName;

import com.xceptance.xlt.script.util.ScriptValidator;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScriptPackageImpl extends EPackageImpl implements ScriptPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeJavaModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeModuleParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeScriptActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeScriptCommandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeScriptModuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeScriptModule1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeScriptModuleConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeScriptModuleParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexTypeTestCasePostStepsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum simpleTypeScriptCommandNameEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType simpleTypeNonEmptyStringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType simpleTypeScriptCommandNameObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType simpleTypeString128EDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType simpleTypeVersionIdentEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.xceptance.xlt.script.ScriptPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ScriptPackageImpl() {
		super(eNS_URI, ScriptFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ScriptPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ScriptPackage init() {
		if (isInited) return (ScriptPackage)EPackage.Registry.INSTANCE.getEPackage(ScriptPackage.eNS_URI);

		// Obtain or create and register package
		ScriptPackageImpl theScriptPackage = (ScriptPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ScriptPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ScriptPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ProjectPackageImpl theProjectPackage = (ProjectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) instanceof ProjectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ProjectPackage.eNS_URI) : ProjectPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theScriptPackage.createPackageContents();
		theProjectPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theScriptPackage.initializePackageContents();
		theProjectPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theScriptPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return ScriptValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theScriptPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ScriptPackage.eNS_URI, theScriptPackage);
		return theScriptPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeJavaModule() {
		return complexTypeJavaModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeJavaModule_Group() {
		return (EAttribute)complexTypeJavaModuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeJavaModule_Tags() {
		return (EAttribute)complexTypeJavaModuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeJavaModule_Description() {
		return (EAttribute)complexTypeJavaModuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeJavaModule_Parameter() {
		return (EReference)complexTypeJavaModuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeJavaModule_Class() {
		return (EAttribute)complexTypeJavaModuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeJavaModule_Id() {
		return (EAttribute)complexTypeJavaModuleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeJavaModule_Version() {
		return (EAttribute)complexTypeJavaModuleEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeModuleParameter() {
		return complexTypeModuleParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeModuleParameter_Desc() {
		return (EAttribute)complexTypeModuleParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeModuleParameter_Name() {
		return (EAttribute)complexTypeModuleParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeScriptAction() {
		return complexTypeScriptActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptAction_Group() {
		return (EAttribute)complexTypeScriptActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptAction_Comment() {
		return (EAttribute)complexTypeScriptActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptAction_Disabled() {
		return (EAttribute)complexTypeScriptActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptAction_Name() {
		return (EAttribute)complexTypeScriptActionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeScriptCommand() {
		return complexTypeScriptCommandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Group() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Target() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Value() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Comment() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Disabled() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Name() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Target1() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptCommand_Value1() {
		return (EAttribute)complexTypeScriptCommandEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeScriptModule() {
		return complexTypeScriptModuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule_Group() {
		return (EAttribute)complexTypeScriptModuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule_Comment() {
		return (EAttribute)complexTypeScriptModuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeScriptModule_Condition() {
		return (EReference)complexTypeScriptModuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeScriptModule_Parameter() {
		return (EReference)complexTypeScriptModuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule_Disabled() {
		return (EAttribute)complexTypeScriptModuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule_Name() {
		return (EAttribute)complexTypeScriptModuleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeScriptModule1() {
		return complexTypeScriptModule1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule1_Group() {
		return (EAttribute)complexTypeScriptModule1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule1_Tags() {
		return (EAttribute)complexTypeScriptModule1EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule1_Description() {
		return (EAttribute)complexTypeScriptModule1EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeScriptModule1_Parameter() {
		return (EReference)complexTypeScriptModule1EClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule1_GroupScriptActions() {
		return (EAttribute)complexTypeScriptModule1EClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeScriptModule1_Command() {
		return (EReference)complexTypeScriptModule1EClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeScriptModule1_Action() {
		return (EReference)complexTypeScriptModule1EClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeScriptModule1_Module() {
		return (EReference)complexTypeScriptModule1EClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule1_Codecomment() {
		return (EAttribute)complexTypeScriptModule1EClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule1_Id() {
		return (EAttribute)complexTypeScriptModule1EClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModule1_Version() {
		return (EAttribute)complexTypeScriptModule1EClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeScriptModuleCondition() {
		return complexTypeScriptModuleConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModuleCondition_Value() {
		return (EAttribute)complexTypeScriptModuleConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModuleCondition_Disabled() {
		return (EAttribute)complexTypeScriptModuleConditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeScriptModuleParameter() {
		return complexTypeScriptModuleParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModuleParameter_Name() {
		return (EAttribute)complexTypeScriptModuleParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeScriptModuleParameter_Value() {
		return (EAttribute)complexTypeScriptModuleParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeTestCase() {
		return complexTypeTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_Group() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_Tags() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_Description() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_GroupScriptActions() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeTestCase_Command() {
		return (EReference)complexTypeTestCaseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeTestCase_Action() {
		return (EReference)complexTypeTestCaseEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeTestCase_Module() {
		return (EReference)complexTypeTestCaseEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_Codecomment() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeTestCase_PostSteps() {
		return (EReference)complexTypeTestCaseEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_BaseURL() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_Disabled() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_Id() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_JunitTest() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCase_Version() {
		return (EAttribute)complexTypeTestCaseEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexTypeTestCasePostSteps() {
		return complexTypeTestCasePostStepsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCasePostSteps_GroupScriptActions() {
		return (EAttribute)complexTypeTestCasePostStepsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeTestCasePostSteps_Command() {
		return (EReference)complexTypeTestCasePostStepsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeTestCasePostSteps_Action() {
		return (EReference)complexTypeTestCasePostStepsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexTypeTestCasePostSteps_Module() {
		return (EReference)complexTypeTestCasePostStepsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexTypeTestCasePostSteps_Codecomment() {
		return (EAttribute)complexTypeTestCasePostStepsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Javamodule() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Scriptmodule() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Testcase() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSimpleTypeScriptCommandName() {
		return simpleTypeScriptCommandNameEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSimpleTypeNonEmptyString() {
		return simpleTypeNonEmptyStringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSimpleTypeScriptCommandNameObject() {
		return simpleTypeScriptCommandNameObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSimpleTypeString128() {
		return simpleTypeString128EDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSimpleTypeVersionIdent() {
		return simpleTypeVersionIdentEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScriptFactory getScriptFactory() {
		return (ScriptFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		complexTypeJavaModuleEClass = createEClass(COMPLEX_TYPE_JAVA_MODULE);
		createEAttribute(complexTypeJavaModuleEClass, COMPLEX_TYPE_JAVA_MODULE__GROUP);
		createEAttribute(complexTypeJavaModuleEClass, COMPLEX_TYPE_JAVA_MODULE__TAGS);
		createEAttribute(complexTypeJavaModuleEClass, COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION);
		createEReference(complexTypeJavaModuleEClass, COMPLEX_TYPE_JAVA_MODULE__PARAMETER);
		createEAttribute(complexTypeJavaModuleEClass, COMPLEX_TYPE_JAVA_MODULE__CLASS);
		createEAttribute(complexTypeJavaModuleEClass, COMPLEX_TYPE_JAVA_MODULE__ID);
		createEAttribute(complexTypeJavaModuleEClass, COMPLEX_TYPE_JAVA_MODULE__VERSION);

		complexTypeModuleParameterEClass = createEClass(COMPLEX_TYPE_MODULE_PARAMETER);
		createEAttribute(complexTypeModuleParameterEClass, COMPLEX_TYPE_MODULE_PARAMETER__DESC);
		createEAttribute(complexTypeModuleParameterEClass, COMPLEX_TYPE_MODULE_PARAMETER__NAME);

		complexTypeScriptActionEClass = createEClass(COMPLEX_TYPE_SCRIPT_ACTION);
		createEAttribute(complexTypeScriptActionEClass, COMPLEX_TYPE_SCRIPT_ACTION__GROUP);
		createEAttribute(complexTypeScriptActionEClass, COMPLEX_TYPE_SCRIPT_ACTION__COMMENT);
		createEAttribute(complexTypeScriptActionEClass, COMPLEX_TYPE_SCRIPT_ACTION__DISABLED);
		createEAttribute(complexTypeScriptActionEClass, COMPLEX_TYPE_SCRIPT_ACTION__NAME);

		complexTypeScriptCommandEClass = createEClass(COMPLEX_TYPE_SCRIPT_COMMAND);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__GROUP);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__TARGET);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__VALUE);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__NAME);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1);
		createEAttribute(complexTypeScriptCommandEClass, COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1);

		complexTypeScriptModuleEClass = createEClass(COMPLEX_TYPE_SCRIPT_MODULE);
		createEAttribute(complexTypeScriptModuleEClass, COMPLEX_TYPE_SCRIPT_MODULE__GROUP);
		createEAttribute(complexTypeScriptModuleEClass, COMPLEX_TYPE_SCRIPT_MODULE__COMMENT);
		createEReference(complexTypeScriptModuleEClass, COMPLEX_TYPE_SCRIPT_MODULE__CONDITION);
		createEReference(complexTypeScriptModuleEClass, COMPLEX_TYPE_SCRIPT_MODULE__PARAMETER);
		createEAttribute(complexTypeScriptModuleEClass, COMPLEX_TYPE_SCRIPT_MODULE__DISABLED);
		createEAttribute(complexTypeScriptModuleEClass, COMPLEX_TYPE_SCRIPT_MODULE__NAME);

		complexTypeScriptModule1EClass = createEClass(COMPLEX_TYPE_SCRIPT_MODULE1);
		createEAttribute(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__GROUP);
		createEAttribute(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__TAGS);
		createEAttribute(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION);
		createEReference(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER);
		createEAttribute(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS);
		createEReference(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND);
		createEReference(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__ACTION);
		createEReference(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__MODULE);
		createEAttribute(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT);
		createEAttribute(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__ID);
		createEAttribute(complexTypeScriptModule1EClass, COMPLEX_TYPE_SCRIPT_MODULE1__VERSION);

		complexTypeScriptModuleConditionEClass = createEClass(COMPLEX_TYPE_SCRIPT_MODULE_CONDITION);
		createEAttribute(complexTypeScriptModuleConditionEClass, COMPLEX_TYPE_SCRIPT_MODULE_CONDITION__VALUE);
		createEAttribute(complexTypeScriptModuleConditionEClass, COMPLEX_TYPE_SCRIPT_MODULE_CONDITION__DISABLED);

		complexTypeScriptModuleParameterEClass = createEClass(COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER);
		createEAttribute(complexTypeScriptModuleParameterEClass, COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER__NAME);
		createEAttribute(complexTypeScriptModuleParameterEClass, COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER__VALUE);

		complexTypeTestCaseEClass = createEClass(COMPLEX_TYPE_TEST_CASE);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__GROUP);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__TAGS);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__DESCRIPTION);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS);
		createEReference(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__COMMAND);
		createEReference(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__ACTION);
		createEReference(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__MODULE);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__CODECOMMENT);
		createEReference(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__POST_STEPS);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__BASE_URL);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__DISABLED);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__ID);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__JUNIT_TEST);
		createEAttribute(complexTypeTestCaseEClass, COMPLEX_TYPE_TEST_CASE__VERSION);

		complexTypeTestCasePostStepsEClass = createEClass(COMPLEX_TYPE_TEST_CASE_POST_STEPS);
		createEAttribute(complexTypeTestCasePostStepsEClass, COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS);
		createEReference(complexTypeTestCasePostStepsEClass, COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND);
		createEReference(complexTypeTestCasePostStepsEClass, COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION);
		createEReference(complexTypeTestCasePostStepsEClass, COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE);
		createEAttribute(complexTypeTestCasePostStepsEClass, COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__JAVAMODULE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__SCRIPTMODULE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__TESTCASE);

		// Create enums
		simpleTypeScriptCommandNameEEnum = createEEnum(SIMPLE_TYPE_SCRIPT_COMMAND_NAME);

		// Create data types
		simpleTypeNonEmptyStringEDataType = createEDataType(SIMPLE_TYPE_NON_EMPTY_STRING);
		simpleTypeScriptCommandNameObjectEDataType = createEDataType(SIMPLE_TYPE_SCRIPT_COMMAND_NAME_OBJECT);
		simpleTypeString128EDataType = createEDataType(SIMPLE_TYPE_STRING128);
		simpleTypeVersionIdentEDataType = createEDataType(SIMPLE_TYPE_VERSION_IDENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(complexTypeJavaModuleEClass, ComplexTypeJavaModule.class, "ComplexTypeJavaModule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeJavaModule_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ComplexTypeJavaModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeJavaModule_Tags(), theXMLTypePackage.getString(), "tags", null, 0, -1, ComplexTypeJavaModule.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeJavaModule_Description(), theXMLTypePackage.getString(), "description", null, 0, -1, ComplexTypeJavaModule.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeJavaModule_Parameter(), this.getComplexTypeModuleParameter(), null, "parameter", null, 0, -1, ComplexTypeJavaModule.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeJavaModule_Class(), this.getSimpleTypeNonEmptyString(), "class", null, 1, 1, ComplexTypeJavaModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeJavaModule_Id(), this.getSimpleTypeNonEmptyString(), "id", null, 0, 1, ComplexTypeJavaModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeJavaModule_Version(), this.getSimpleTypeVersionIdent(), "version", null, 1, 1, ComplexTypeJavaModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeModuleParameterEClass, ComplexTypeModuleParameter.class, "ComplexTypeModuleParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeModuleParameter_Desc(), this.getSimpleTypeNonEmptyString(), "desc", null, 0, 1, ComplexTypeModuleParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeModuleParameter_Name(), this.getSimpleTypeNonEmptyString(), "name", null, 1, 1, ComplexTypeModuleParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeScriptActionEClass, ComplexTypeScriptAction.class, "ComplexTypeScriptAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeScriptAction_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ComplexTypeScriptAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptAction_Comment(), theXMLTypePackage.getString(), "comment", null, 0, -1, ComplexTypeScriptAction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptAction_Disabled(), theXMLTypePackage.getBoolean(), "disabled", null, 0, 1, ComplexTypeScriptAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptAction_Name(), this.getSimpleTypeNonEmptyString(), "name", null, 1, 1, ComplexTypeScriptAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeScriptCommandEClass, ComplexTypeScriptCommand.class, "ComplexTypeScriptCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeScriptCommand_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ComplexTypeScriptCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptCommand_Target(), this.getSimpleTypeNonEmptyString(), "target", null, 0, -1, ComplexTypeScriptCommand.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptCommand_Value(), this.getSimpleTypeNonEmptyString(), "value", null, 0, -1, ComplexTypeScriptCommand.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptCommand_Comment(), theXMLTypePackage.getString(), "comment", null, 0, -1, ComplexTypeScriptCommand.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptCommand_Disabled(), theXMLTypePackage.getBoolean(), "disabled", null, 0, 1, ComplexTypeScriptCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptCommand_Name(), this.getSimpleTypeScriptCommandName(), "name", null, 1, 1, ComplexTypeScriptCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptCommand_Target1(), theXMLTypePackage.getString(), "target1", null, 0, 1, ComplexTypeScriptCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptCommand_Value1(), this.getSimpleTypeString128(), "value1", null, 0, 1, ComplexTypeScriptCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeScriptModuleEClass, ComplexTypeScriptModule.class, "ComplexTypeScriptModule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeScriptModule_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ComplexTypeScriptModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule_Comment(), theXMLTypePackage.getString(), "comment", null, 0, -1, ComplexTypeScriptModule.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeScriptModule_Condition(), this.getComplexTypeScriptModuleCondition(), null, "condition", null, 0, -1, ComplexTypeScriptModule.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeScriptModule_Parameter(), this.getComplexTypeScriptModuleParameter(), null, "parameter", null, 0, -1, ComplexTypeScriptModule.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule_Disabled(), theXMLTypePackage.getBoolean(), "disabled", null, 0, 1, ComplexTypeScriptModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule_Name(), this.getSimpleTypeNonEmptyString(), "name", null, 1, 1, ComplexTypeScriptModule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeScriptModule1EClass, ComplexTypeScriptModule1.class, "ComplexTypeScriptModule1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeScriptModule1_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ComplexTypeScriptModule1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule1_Tags(), theXMLTypePackage.getString(), "tags", null, 0, 2, ComplexTypeScriptModule1.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule1_Description(), theXMLTypePackage.getString(), "description", null, 0, 2, ComplexTypeScriptModule1.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeScriptModule1_Parameter(), this.getComplexTypeModuleParameter(), null, "parameter", null, 0, -1, ComplexTypeScriptModule1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule1_GroupScriptActions(), ecorePackage.getEFeatureMapEntry(), "groupScriptActions", null, 0, -1, ComplexTypeScriptModule1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeScriptModule1_Command(), this.getComplexTypeScriptCommand(), null, "command", null, 0, -1, ComplexTypeScriptModule1.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeScriptModule1_Action(), this.getComplexTypeScriptAction(), null, "action", null, 0, -1, ComplexTypeScriptModule1.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeScriptModule1_Module(), this.getComplexTypeScriptModule(), null, "module", null, 0, -1, ComplexTypeScriptModule1.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule1_Codecomment(), theXMLTypePackage.getString(), "codecomment", null, 0, -1, ComplexTypeScriptModule1.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule1_Id(), this.getSimpleTypeNonEmptyString(), "id", null, 0, 1, ComplexTypeScriptModule1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModule1_Version(), this.getSimpleTypeVersionIdent(), "version", null, 1, 1, ComplexTypeScriptModule1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeScriptModuleConditionEClass, ComplexTypeScriptModuleCondition.class, "ComplexTypeScriptModuleCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeScriptModuleCondition_Value(), this.getSimpleTypeNonEmptyString(), "value", null, 0, 1, ComplexTypeScriptModuleCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModuleCondition_Disabled(), theXMLTypePackage.getBoolean(), "disabled", null, 0, 1, ComplexTypeScriptModuleCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeScriptModuleParameterEClass, ComplexTypeScriptModuleParameter.class, "ComplexTypeScriptModuleParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeScriptModuleParameter_Name(), this.getSimpleTypeNonEmptyString(), "name", null, 1, 1, ComplexTypeScriptModuleParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeScriptModuleParameter_Value(), theXMLTypePackage.getString(), "value", null, 1, 1, ComplexTypeScriptModuleParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeTestCaseEClass, ComplexTypeTestCase.class, "ComplexTypeTestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeTestCase_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_Tags(), theXMLTypePackage.getString(), "tags", null, 0, 2, ComplexTypeTestCase.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_Description(), theXMLTypePackage.getString(), "description", null, 0, 2, ComplexTypeTestCase.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_GroupScriptActions(), ecorePackage.getEFeatureMapEntry(), "groupScriptActions", null, 0, -1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeTestCase_Command(), this.getComplexTypeScriptCommand(), null, "command", null, 0, -1, ComplexTypeTestCase.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeTestCase_Action(), this.getComplexTypeScriptAction(), null, "action", null, 0, -1, ComplexTypeTestCase.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeTestCase_Module(), this.getComplexTypeScriptModule(), null, "module", null, 0, -1, ComplexTypeTestCase.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_Codecomment(), theXMLTypePackage.getString(), "codecomment", null, 0, -1, ComplexTypeTestCase.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeTestCase_PostSteps(), this.getComplexTypeTestCasePostSteps(), null, "postSteps", null, 0, 1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_BaseURL(), this.getSimpleTypeNonEmptyString(), "baseURL", null, 0, 1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_Disabled(), theXMLTypePackage.getBoolean(), "disabled", null, 0, 1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_Id(), this.getSimpleTypeNonEmptyString(), "id", null, 0, 1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_JunitTest(), theXMLTypePackage.getBoolean(), "junitTest", null, 0, 1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCase_Version(), this.getSimpleTypeVersionIdent(), "version", null, 1, 1, ComplexTypeTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexTypeTestCasePostStepsEClass, ComplexTypeTestCasePostSteps.class, "ComplexTypeTestCasePostSteps", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexTypeTestCasePostSteps_GroupScriptActions(), ecorePackage.getEFeatureMapEntry(), "groupScriptActions", null, 0, -1, ComplexTypeTestCasePostSteps.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeTestCasePostSteps_Command(), this.getComplexTypeScriptCommand(), null, "command", null, 0, -1, ComplexTypeTestCasePostSteps.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeTestCasePostSteps_Action(), this.getComplexTypeScriptAction(), null, "action", null, 0, -1, ComplexTypeTestCasePostSteps.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComplexTypeTestCasePostSteps_Module(), this.getComplexTypeScriptModule(), null, "module", null, 0, -1, ComplexTypeTestCasePostSteps.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplexTypeTestCasePostSteps_Codecomment(), theXMLTypePackage.getString(), "codecomment", null, 0, -1, ComplexTypeTestCasePostSteps.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Javamodule(), this.getComplexTypeJavaModule(), null, "javamodule", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Scriptmodule(), this.getComplexTypeScriptModule1(), null, "scriptmodule", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Testcase(), this.getComplexTypeTestCase(), null, "testcase", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.class, "SimpleTypeScriptCommandName");
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ADD_SELECTION);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_ATTRIBUTE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_CHECKED);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_CLASS);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_ELEMENT_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_ELEMENT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_EVAL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_LOAD_TIME);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_PAGE_SIZE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_SELECTED_ID);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_SELECTED_INDEX);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_SELECTED_LABEL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_SELECTED_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_STYLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_TEXT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_TEXT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_TITLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_VISIBLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_XPATH_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_ATTRIBUTE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_CHECKED);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_CLASS);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_ELEMENT_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_ELEMENT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_EVAL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_SELECTED_ID);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_SELECTED_INDEX);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_SELECTED_LABEL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_SELECTED_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_STYLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_TEXT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_TEXT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_TITLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_VISIBLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ASSERT_NOT_XPATH_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CHECK);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CHECK_AND_WAIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CLICK);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CLICK_AND_WAIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CLOSE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CONTEXT_MENU);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CONTEXT_MENU_AT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.CREATE_COOKIE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.DELETE_ALL_VISIBLE_COOKIES);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.DELETE_COOKIE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.DOUBLE_CLICK);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.DOUBLE_CLICK_AND_WAIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.ECHO);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_DOWN);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_DOWN_AT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_MOVE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_MOVE_AT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_OUT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_OVER);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_UP);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.MOUSE_UP_AT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.OPEN);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.PAUSE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.REMOVE_SELECTION);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.SELECT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.SELECT_AND_WAIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE_ELEMENT_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.SELECT_FRAME);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.SELECT_WINDOW);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.SET_TIMEOUT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE_ATTRIBUTE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE_EVAL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE_TEXT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE_TITLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.STORE_XPATH_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.SUBMIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.SUBMIT_AND_WAIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.TYPE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.TYPE_AND_WAIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.UNCHECK);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.UNCHECK_AND_WAIT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_ATTRIBUTE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_CHECKED);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_CLASS);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_ELEMENT_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_PAGE_TO_LOAD);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_POP_UP);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_ELEMENT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_EVAL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_SELECTED_ID);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_SELECTED_INDEX);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_SELECTED_LABEL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_SELECTED_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_STYLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_TEXT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_TEXT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_TITLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_VISIBLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_XPATH_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_ATTRIBUTE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_CHECKED);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_CLASS);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_ELEMENT_COUNT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_ELEMENT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_EVAL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_SELECTED_ID);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_SELECTED_INDEX);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_SELECTED_LABEL);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_SELECTED_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_STYLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_TEXT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_TEXT_PRESENT);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_TITLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_VALUE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_VISIBLE);
		addEEnumLiteral(simpleTypeScriptCommandNameEEnum, SimpleTypeScriptCommandName.WAIT_FOR_NOT_XPATH_COUNT);

		// Initialize data types
		initEDataType(simpleTypeNonEmptyStringEDataType, String.class, "SimpleTypeNonEmptyString", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(simpleTypeScriptCommandNameObjectEDataType, SimpleTypeScriptCommandName.class, "SimpleTypeScriptCommandNameObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(simpleTypeString128EDataType, String.class, "SimpleTypeString128", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(simpleTypeVersionIdentEDataType, BigInteger.class, "SimpleTypeVersionIdent", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
		addAnnotation
		  (complexTypeJavaModuleEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.JavaModule",
			 "kind", "elementOnly"
		   });	
		addAnnotation
		  (getComplexTypeJavaModule_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });	
		addAnnotation
		  (getComplexTypeJavaModule_Tags(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "tags",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeJavaModule_Description(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "description",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeJavaModule_Parameter(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "parameter",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeJavaModule_Class(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "class"
		   });	
		addAnnotation
		  (getComplexTypeJavaModule_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id"
		   });	
		addAnnotation
		  (getComplexTypeJavaModule_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version"
		   });	
		addAnnotation
		  (complexTypeModuleParameterEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.Module.Parameter",
			 "kind", "empty"
		   });	
		addAnnotation
		  (getComplexTypeModuleParameter_Desc(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "desc"
		   });	
		addAnnotation
		  (getComplexTypeModuleParameter_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name"
		   });	
		addAnnotation
		  (complexTypeScriptActionEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.Script.Action",
			 "kind", "elementOnly"
		   });	
		addAnnotation
		  (getComplexTypeScriptAction_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptAction_Comment(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "comment",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptAction_Disabled(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "disabled"
		   });	
		addAnnotation
		  (getComplexTypeScriptAction_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name"
		   });	
		addAnnotation
		  (complexTypeScriptCommandEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.Script.Command",
			 "kind", "elementOnly"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Target(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "target",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Value(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "value",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Comment(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "comment",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Disabled(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "disabled"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Target1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "target"
		   });	
		addAnnotation
		  (getComplexTypeScriptCommand_Value1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "value"
		   });	
		addAnnotation
		  (complexTypeScriptModuleEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.Script.Module",
			 "kind", "elementOnly"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule_Comment(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "comment",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule_Condition(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "condition",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule_Parameter(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "parameter",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule_Disabled(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "disabled"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name"
		   });	
		addAnnotation
		  (complexTypeScriptModule1EClass, 
		   source, 
		   new String[] {
			 "name", "complexType.ScriptModule",
			 "kind", "elementOnly"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Tags(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "tags",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Description(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "description",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Parameter(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "parameter",
			 "namespace", "##targetNamespace"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_GroupScriptActions(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "GroupScriptActions:4"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Command(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "command",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:4"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Action(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "action",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:4"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Module(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "module",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:4"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Codecomment(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "codecomment",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:4"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id"
		   });	
		addAnnotation
		  (getComplexTypeScriptModule1_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version"
		   });	
		addAnnotation
		  (complexTypeScriptModuleConditionEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.Script.Module.Condition",
			 "kind", "simple"
		   });	
		addAnnotation
		  (getComplexTypeScriptModuleCondition_Value(), 
		   source, 
		   new String[] {
			 "name", ":0",
			 "kind", "simple"
		   });	
		addAnnotation
		  (getComplexTypeScriptModuleCondition_Disabled(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "disabled"
		   });	
		addAnnotation
		  (complexTypeScriptModuleParameterEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.Script.Module.Parameter",
			 "kind", "empty"
		   });	
		addAnnotation
		  (getComplexTypeScriptModuleParameter_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name"
		   });	
		addAnnotation
		  (getComplexTypeScriptModuleParameter_Value(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "value"
		   });	
		addAnnotation
		  (complexTypeTestCaseEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.TestCase",
			 "kind", "elementOnly"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Tags(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "tags",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Description(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "description",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_GroupScriptActions(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "GroupScriptActions:3"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Command(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "command",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:3"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Action(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "action",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:3"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Module(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "module",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:3"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Codecomment(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "codecomment",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:3"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_PostSteps(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "postSteps",
			 "namespace", "##targetNamespace"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_BaseURL(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "baseURL"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Disabled(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "disabled"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_JunitTest(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "junit-test"
		   });	
		addAnnotation
		  (getComplexTypeTestCase_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version"
		   });	
		addAnnotation
		  (complexTypeTestCasePostStepsEClass, 
		   source, 
		   new String[] {
			 "name", "complexType.TestCase.PostSteps",
			 "kind", "elementOnly"
		   });	
		addAnnotation
		  (getComplexTypeTestCasePostSteps_GroupScriptActions(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "GroupScriptActions:0"
		   });	
		addAnnotation
		  (getComplexTypeTestCasePostSteps_Command(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "command",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:0"
		   });	
		addAnnotation
		  (getComplexTypeTestCasePostSteps_Action(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "action",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:0"
		   });	
		addAnnotation
		  (getComplexTypeTestCasePostSteps_Module(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "module",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:0"
		   });	
		addAnnotation
		  (getComplexTypeTestCasePostSteps_Codecomment(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "codecomment",
			 "namespace", "##targetNamespace",
			 "group", "#GroupScriptActions:0"
		   });	
		addAnnotation
		  (documentRootEClass, 
		   source, 
		   new String[] {
			 "name", "",
			 "kind", "mixed"
		   });	
		addAnnotation
		  (getDocumentRoot_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });	
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xmlns:prefix"
		   });	
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xsi:schemaLocation"
		   });	
		addAnnotation
		  (getDocumentRoot_Javamodule(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "javamodule",
			 "namespace", "##targetNamespace"
		   });	
		addAnnotation
		  (getDocumentRoot_Scriptmodule(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "scriptmodule",
			 "namespace", "##targetNamespace"
		   });	
		addAnnotation
		  (getDocumentRoot_Testcase(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "testcase",
			 "namespace", "##targetNamespace"
		   });	
		addAnnotation
		  (simpleTypeNonEmptyStringEDataType, 
		   source, 
		   new String[] {
			 "name", "simpleType.NonEmptyString",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#string",
			 "minLength", "1"
		   });	
		addAnnotation
		  (simpleTypeScriptCommandNameEEnum, 
		   source, 
		   new String[] {
			 "name", "simpleType.Script.Command.Name"
		   });	
		addAnnotation
		  (simpleTypeScriptCommandNameObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "simpleType.Script.Command.Name:Object",
			 "baseType", "simpleType.Script.Command.Name"
		   });	
		addAnnotation
		  (simpleTypeString128EDataType, 
		   source, 
		   new String[] {
			 "name", "simpleType.String.128",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#string",
			 "maxLength", "128"
		   });	
		addAnnotation
		  (simpleTypeVersionIdentEDataType, 
		   source, 
		   new String[] {
			 "name", "simpleType.VersionIdent",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#positiveInteger",
			 "minInclusive", "2"
		   });
	}

} //ScriptPackageImpl
