/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.ComplexTypeJavaModule;
import com.xceptance.xlt.script.ComplexTypeScriptModule1;
import com.xceptance.xlt.script.ComplexTypeTestCase;
import com.xceptance.xlt.script.DocumentRoot;
import com.xceptance.xlt.script.ScriptPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.DocumentRootImpl#getJavamodule <em>Javamodule</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.DocumentRootImpl#getScriptmodule <em>Scriptmodule</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.DocumentRootImpl#getTestcase <em>Testcase</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentRootImpl extends MinimalEObjectImpl.Container implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScriptPackage.Literals.DOCUMENT_ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, ScriptPackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, ScriptPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, ScriptPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeJavaModule getJavamodule() {
		return (ComplexTypeJavaModule)getMixed().get(ScriptPackage.Literals.DOCUMENT_ROOT__JAVAMODULE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetJavamodule(ComplexTypeJavaModule newJavamodule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(ScriptPackage.Literals.DOCUMENT_ROOT__JAVAMODULE, newJavamodule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJavamodule(ComplexTypeJavaModule newJavamodule) {
		((FeatureMap.Internal)getMixed()).set(ScriptPackage.Literals.DOCUMENT_ROOT__JAVAMODULE, newJavamodule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeScriptModule1 getScriptmodule() {
		return (ComplexTypeScriptModule1)getMixed().get(ScriptPackage.Literals.DOCUMENT_ROOT__SCRIPTMODULE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScriptmodule(ComplexTypeScriptModule1 newScriptmodule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(ScriptPackage.Literals.DOCUMENT_ROOT__SCRIPTMODULE, newScriptmodule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScriptmodule(ComplexTypeScriptModule1 newScriptmodule) {
		((FeatureMap.Internal)getMixed()).set(ScriptPackage.Literals.DOCUMENT_ROOT__SCRIPTMODULE, newScriptmodule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeTestCase getTestcase() {
		return (ComplexTypeTestCase)getMixed().get(ScriptPackage.Literals.DOCUMENT_ROOT__TESTCASE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestcase(ComplexTypeTestCase newTestcase, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(ScriptPackage.Literals.DOCUMENT_ROOT__TESTCASE, newTestcase, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestcase(ComplexTypeTestCase newTestcase) {
		((FeatureMap.Internal)getMixed()).set(ScriptPackage.Literals.DOCUMENT_ROOT__TESTCASE, newTestcase);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScriptPackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case ScriptPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList<?>)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case ScriptPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList<?>)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case ScriptPackage.DOCUMENT_ROOT__JAVAMODULE:
				return basicSetJavamodule(null, msgs);
			case ScriptPackage.DOCUMENT_ROOT__SCRIPTMODULE:
				return basicSetScriptmodule(null, msgs);
			case ScriptPackage.DOCUMENT_ROOT__TESTCASE:
				return basicSetTestcase(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScriptPackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case ScriptPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case ScriptPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case ScriptPackage.DOCUMENT_ROOT__JAVAMODULE:
				return getJavamodule();
			case ScriptPackage.DOCUMENT_ROOT__SCRIPTMODULE:
				return getScriptmodule();
			case ScriptPackage.DOCUMENT_ROOT__TESTCASE:
				return getTestcase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScriptPackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case ScriptPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case ScriptPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case ScriptPackage.DOCUMENT_ROOT__JAVAMODULE:
				setJavamodule((ComplexTypeJavaModule)newValue);
				return;
			case ScriptPackage.DOCUMENT_ROOT__SCRIPTMODULE:
				setScriptmodule((ComplexTypeScriptModule1)newValue);
				return;
			case ScriptPackage.DOCUMENT_ROOT__TESTCASE:
				setTestcase((ComplexTypeTestCase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScriptPackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case ScriptPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case ScriptPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case ScriptPackage.DOCUMENT_ROOT__JAVAMODULE:
				setJavamodule((ComplexTypeJavaModule)null);
				return;
			case ScriptPackage.DOCUMENT_ROOT__SCRIPTMODULE:
				setScriptmodule((ComplexTypeScriptModule1)null);
				return;
			case ScriptPackage.DOCUMENT_ROOT__TESTCASE:
				setTestcase((ComplexTypeTestCase)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScriptPackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case ScriptPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case ScriptPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case ScriptPackage.DOCUMENT_ROOT__JAVAMODULE:
				return getJavamodule() != null;
			case ScriptPackage.DOCUMENT_ROOT__SCRIPTMODULE:
				return getScriptmodule() != null;
			case ScriptPackage.DOCUMENT_ROOT__TESTCASE:
				return getTestcase() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
