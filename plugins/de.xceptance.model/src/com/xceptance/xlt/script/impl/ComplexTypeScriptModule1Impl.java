/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.ComplexTypeModuleParameter;
import com.xceptance.xlt.script.ComplexTypeScriptAction;
import com.xceptance.xlt.script.ComplexTypeScriptCommand;
import com.xceptance.xlt.script.ComplexTypeScriptModule;
import com.xceptance.xlt.script.ComplexTypeScriptModule1;
import com.xceptance.xlt.script.ScriptPackage;

import java.math.BigInteger;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Type Script Module1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getTags <em>Tags</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getGroupScriptActions <em>Group Script Actions</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getCommand <em>Command</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getAction <em>Action</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getModule <em>Module</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getCodecomment <em>Codecomment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getId <em>Id</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptModule1Impl#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexTypeScriptModule1Impl extends MinimalEObjectImpl.Container implements ComplexTypeScriptModule1 {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<ComplexTypeModuleParameter> parameter;

	/**
	 * The cached value of the '{@link #getGroupScriptActions() <em>Group Script Actions</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupScriptActions()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap groupScriptActions;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected BigInteger version = VERSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexTypeScriptModule1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTags() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE1__TAGS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getDescription() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeModuleParameter> getParameter() {
		if (parameter == null) {
			parameter = new EObjectContainmentEList<ComplexTypeModuleParameter>(ComplexTypeModuleParameter.class, this, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER);
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroupScriptActions() {
		if (groupScriptActions == null) {
			groupScriptActions = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS);
		}
		return groupScriptActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptCommand> getCommand() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptAction> getAction() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE1__ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptModule> getModule() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE1__MODULE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getCodecomment() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(BigInteger newVersion) {
		BigInteger oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER:
				return ((InternalEList<?>)getParameter()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS:
				return ((InternalEList<?>)getGroupScriptActions()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND:
				return ((InternalEList<?>)getCommand()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ACTION:
				return ((InternalEList<?>)getAction()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__MODULE:
				return ((InternalEList<?>)getModule()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__TAGS:
				return getTags();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION:
				return getDescription();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER:
				return getParameter();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS:
				if (coreType) return getGroupScriptActions();
				return ((FeatureMap.Internal)getGroupScriptActions()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND:
				return getCommand();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ACTION:
				return getAction();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__MODULE:
				return getModule();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT:
				return getCodecomment();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ID:
				return getId();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__VERSION:
				return getVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER:
				getParameter().clear();
				getParameter().addAll((Collection<? extends ComplexTypeModuleParameter>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS:
				((FeatureMap.Internal)getGroupScriptActions()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND:
				getCommand().clear();
				getCommand().addAll((Collection<? extends ComplexTypeScriptCommand>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ACTION:
				getAction().clear();
				getAction().addAll((Collection<? extends ComplexTypeScriptAction>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__MODULE:
				getModule().clear();
				getModule().addAll((Collection<? extends ComplexTypeScriptModule>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT:
				getCodecomment().clear();
				getCodecomment().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ID:
				setId((String)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__VERSION:
				setVersion((BigInteger)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP:
				getGroup().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__TAGS:
				getTags().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION:
				getDescription().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER:
				getParameter().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS:
				getGroupScriptActions().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND:
				getCommand().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ACTION:
				getAction().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__MODULE:
				getModule().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT:
				getCodecomment().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ID:
				setId(ID_EDEFAULT);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP:
				return group != null && !group.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__TAGS:
				return !getTags().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__DESCRIPTION:
				return !getDescription().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__PARAMETER:
				return parameter != null && !parameter.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__GROUP_SCRIPT_ACTIONS:
				return groupScriptActions != null && !groupScriptActions.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__COMMAND:
				return !getCommand().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ACTION:
				return !getAction().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__MODULE:
				return !getModule().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__CODECOMMENT:
				return !getCodecomment().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", groupScriptActions: ");
		result.append(groupScriptActions);
		result.append(", id: ");
		result.append(id);
		result.append(", version: ");
		result.append(version);
		result.append(')');
		return result.toString();
	}

} //ComplexTypeScriptModule1Impl
