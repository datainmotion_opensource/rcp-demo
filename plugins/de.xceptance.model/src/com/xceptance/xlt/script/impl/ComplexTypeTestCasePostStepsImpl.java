/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.ComplexTypeScriptAction;
import com.xceptance.xlt.script.ComplexTypeScriptCommand;
import com.xceptance.xlt.script.ComplexTypeScriptModule;
import com.xceptance.xlt.script.ComplexTypeTestCasePostSteps;
import com.xceptance.xlt.script.ScriptPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Type Test Case Post Steps</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl#getGroupScriptActions <em>Group Script Actions</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl#getCommand <em>Command</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl#getAction <em>Action</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl#getModule <em>Module</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeTestCasePostStepsImpl#getCodecomment <em>Codecomment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexTypeTestCasePostStepsImpl extends MinimalEObjectImpl.Container implements ComplexTypeTestCasePostSteps {
	/**
	 * The cached value of the '{@link #getGroupScriptActions() <em>Group Script Actions</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupScriptActions()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap groupScriptActions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexTypeTestCasePostStepsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroupScriptActions() {
		if (groupScriptActions == null) {
			groupScriptActions = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS);
		}
		return groupScriptActions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptCommand> getCommand() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptAction> getAction() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeScriptModule> getModule() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getCodecomment() {
		return getGroupScriptActions().list(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS:
				return ((InternalEList<?>)getGroupScriptActions()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND:
				return ((InternalEList<?>)getCommand()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION:
				return ((InternalEList<?>)getAction()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE:
				return ((InternalEList<?>)getModule()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS:
				if (coreType) return getGroupScriptActions();
				return ((FeatureMap.Internal)getGroupScriptActions()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND:
				return getCommand();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION:
				return getAction();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE:
				return getModule();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT:
				return getCodecomment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS:
				((FeatureMap.Internal)getGroupScriptActions()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND:
				getCommand().clear();
				getCommand().addAll((Collection<? extends ComplexTypeScriptCommand>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION:
				getAction().clear();
				getAction().addAll((Collection<? extends ComplexTypeScriptAction>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE:
				getModule().clear();
				getModule().addAll((Collection<? extends ComplexTypeScriptModule>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT:
				getCodecomment().clear();
				getCodecomment().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS:
				getGroupScriptActions().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND:
				getCommand().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION:
				getAction().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE:
				getModule().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT:
				getCodecomment().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS:
				return groupScriptActions != null && !groupScriptActions.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND:
				return !getCommand().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION:
				return !getAction().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE:
				return !getModule().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT:
				return !getCodecomment().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (groupScriptActions: ");
		result.append(groupScriptActions);
		result.append(')');
		return result.toString();
	}

} //ComplexTypeTestCasePostStepsImpl
