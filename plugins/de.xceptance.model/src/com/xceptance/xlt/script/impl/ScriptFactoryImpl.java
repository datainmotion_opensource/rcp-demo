/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.*;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScriptFactoryImpl extends EFactoryImpl implements ScriptFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ScriptFactory init() {
		try {
			ScriptFactory theScriptFactory = (ScriptFactory)EPackage.Registry.INSTANCE.getEFactory(ScriptPackage.eNS_URI);
			if (theScriptFactory != null) {
				return theScriptFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ScriptFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScriptFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE: return createComplexTypeJavaModule();
			case ScriptPackage.COMPLEX_TYPE_MODULE_PARAMETER: return createComplexTypeModuleParameter();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_ACTION: return createComplexTypeScriptAction();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND: return createComplexTypeScriptCommand();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE: return createComplexTypeScriptModule();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE1: return createComplexTypeScriptModule1();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE_CONDITION: return createComplexTypeScriptModuleCondition();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_MODULE_PARAMETER: return createComplexTypeScriptModuleParameter();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE: return createComplexTypeTestCase();
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS: return createComplexTypeTestCasePostSteps();
			case ScriptPackage.DOCUMENT_ROOT: return createDocumentRoot();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ScriptPackage.SIMPLE_TYPE_SCRIPT_COMMAND_NAME:
				return createSimpleTypeScriptCommandNameFromString(eDataType, initialValue);
			case ScriptPackage.SIMPLE_TYPE_NON_EMPTY_STRING:
				return createSimpleTypeNonEmptyStringFromString(eDataType, initialValue);
			case ScriptPackage.SIMPLE_TYPE_SCRIPT_COMMAND_NAME_OBJECT:
				return createSimpleTypeScriptCommandNameObjectFromString(eDataType, initialValue);
			case ScriptPackage.SIMPLE_TYPE_STRING128:
				return createSimpleTypeString128FromString(eDataType, initialValue);
			case ScriptPackage.SIMPLE_TYPE_VERSION_IDENT:
				return createSimpleTypeVersionIdentFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ScriptPackage.SIMPLE_TYPE_SCRIPT_COMMAND_NAME:
				return convertSimpleTypeScriptCommandNameToString(eDataType, instanceValue);
			case ScriptPackage.SIMPLE_TYPE_NON_EMPTY_STRING:
				return convertSimpleTypeNonEmptyStringToString(eDataType, instanceValue);
			case ScriptPackage.SIMPLE_TYPE_SCRIPT_COMMAND_NAME_OBJECT:
				return convertSimpleTypeScriptCommandNameObjectToString(eDataType, instanceValue);
			case ScriptPackage.SIMPLE_TYPE_STRING128:
				return convertSimpleTypeString128ToString(eDataType, instanceValue);
			case ScriptPackage.SIMPLE_TYPE_VERSION_IDENT:
				return convertSimpleTypeVersionIdentToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeJavaModule createComplexTypeJavaModule() {
		ComplexTypeJavaModuleImpl complexTypeJavaModule = new ComplexTypeJavaModuleImpl();
		return complexTypeJavaModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeModuleParameter createComplexTypeModuleParameter() {
		ComplexTypeModuleParameterImpl complexTypeModuleParameter = new ComplexTypeModuleParameterImpl();
		return complexTypeModuleParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeScriptAction createComplexTypeScriptAction() {
		ComplexTypeScriptActionImpl complexTypeScriptAction = new ComplexTypeScriptActionImpl();
		return complexTypeScriptAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeScriptCommand createComplexTypeScriptCommand() {
		ComplexTypeScriptCommandImpl complexTypeScriptCommand = new ComplexTypeScriptCommandImpl();
		return complexTypeScriptCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeScriptModule createComplexTypeScriptModule() {
		ComplexTypeScriptModuleImpl complexTypeScriptModule = new ComplexTypeScriptModuleImpl();
		return complexTypeScriptModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeScriptModule1 createComplexTypeScriptModule1() {
		ComplexTypeScriptModule1Impl complexTypeScriptModule1 = new ComplexTypeScriptModule1Impl();
		return complexTypeScriptModule1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeScriptModuleCondition createComplexTypeScriptModuleCondition() {
		ComplexTypeScriptModuleConditionImpl complexTypeScriptModuleCondition = new ComplexTypeScriptModuleConditionImpl();
		return complexTypeScriptModuleCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeScriptModuleParameter createComplexTypeScriptModuleParameter() {
		ComplexTypeScriptModuleParameterImpl complexTypeScriptModuleParameter = new ComplexTypeScriptModuleParameterImpl();
		return complexTypeScriptModuleParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeTestCase createComplexTypeTestCase() {
		ComplexTypeTestCaseImpl complexTypeTestCase = new ComplexTypeTestCaseImpl();
		return complexTypeTestCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeTestCasePostSteps createComplexTypeTestCasePostSteps() {
		ComplexTypeTestCasePostStepsImpl complexTypeTestCasePostSteps = new ComplexTypeTestCasePostStepsImpl();
		return complexTypeTestCasePostSteps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleTypeScriptCommandName createSimpleTypeScriptCommandNameFromString(EDataType eDataType, String initialValue) {
		SimpleTypeScriptCommandName result = SimpleTypeScriptCommandName.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypeScriptCommandNameToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createSimpleTypeNonEmptyStringFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.STRING, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypeNonEmptyStringToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.STRING, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleTypeScriptCommandName createSimpleTypeScriptCommandNameObjectFromString(EDataType eDataType, String initialValue) {
		return createSimpleTypeScriptCommandNameFromString(ScriptPackage.Literals.SIMPLE_TYPE_SCRIPT_COMMAND_NAME, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypeScriptCommandNameObjectToString(EDataType eDataType, Object instanceValue) {
		return convertSimpleTypeScriptCommandNameToString(ScriptPackage.Literals.SIMPLE_TYPE_SCRIPT_COMMAND_NAME, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createSimpleTypeString128FromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.STRING, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypeString128ToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.STRING, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger createSimpleTypeVersionIdentFromString(EDataType eDataType, String initialValue) {
		return (BigInteger)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.POSITIVE_INTEGER, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypeVersionIdentToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.POSITIVE_INTEGER, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScriptPackage getScriptPackage() {
		return (ScriptPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ScriptPackage getPackage() {
		return ScriptPackage.eINSTANCE;
	}

} //ScriptFactoryImpl
