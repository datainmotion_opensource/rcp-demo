/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.ComplexTypeJavaModule;
import com.xceptance.xlt.script.ComplexTypeModuleParameter;
import com.xceptance.xlt.script.ScriptPackage;

import java.math.BigInteger;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Type Java Module</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeJavaModuleImpl#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexTypeJavaModuleImpl extends MinimalEObjectImpl.Container implements ComplexTypeJavaModule {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getClass_() <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected String class_ = CLASS_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected BigInteger version = VERSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexTypeJavaModuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScriptPackage.Literals.COMPLEX_TYPE_JAVA_MODULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTags() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_JAVA_MODULE__TAGS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getDescription() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComplexTypeModuleParameter> getParameter() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_JAVA_MODULE__PARAMETER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClass_() {
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(String newClass) {
		String oldClass = class_;
		class_ = newClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__CLASS, oldClass, class_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(BigInteger newVersion) {
		BigInteger oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__PARAMETER:
				return ((InternalEList<?>)getParameter()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__TAGS:
				return getTags();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION:
				return getDescription();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__PARAMETER:
				return getParameter();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__CLASS:
				return getClass_();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__ID:
				return getId();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__VERSION:
				return getVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__PARAMETER:
				getParameter().clear();
				getParameter().addAll((Collection<? extends ComplexTypeModuleParameter>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__CLASS:
				setClass((String)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__ID:
				setId((String)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__VERSION:
				setVersion((BigInteger)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__GROUP:
				getGroup().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__TAGS:
				getTags().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION:
				getDescription().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__PARAMETER:
				getParameter().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__CLASS:
				setClass(CLASS_EDEFAULT);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__ID:
				setId(ID_EDEFAULT);
				return;
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__GROUP:
				return group != null && !group.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__TAGS:
				return !getTags().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__DESCRIPTION:
				return !getDescription().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__PARAMETER:
				return !getParameter().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__CLASS:
				return CLASS_EDEFAULT == null ? class_ != null : !CLASS_EDEFAULT.equals(class_);
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ScriptPackage.COMPLEX_TYPE_JAVA_MODULE__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", class: ");
		result.append(class_);
		result.append(", id: ");
		result.append(id);
		result.append(", version: ");
		result.append(version);
		result.append(')');
		return result.toString();
	}

} //ComplexTypeJavaModuleImpl
