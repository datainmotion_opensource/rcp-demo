/**
 */
package com.xceptance.xlt.script.impl;

import com.xceptance.xlt.script.ComplexTypeScriptCommand;
import com.xceptance.xlt.script.ScriptPackage;
import com.xceptance.xlt.script.SimpleTypeScriptCommandName;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Type Script Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#isDisabled <em>Disabled</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#getTarget1 <em>Target1</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.impl.ComplexTypeScriptCommandImpl#getValue1 <em>Value1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexTypeScriptCommandImpl extends MinimalEObjectImpl.Container implements ComplexTypeScriptCommand {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDisabled() <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisabled()
	 * @generated
	 * @ordered
	 */
	protected boolean disabled = DISABLED_EDEFAULT;

	/**
	 * This is true if the Disabled attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean disabledESet;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleTypeScriptCommandName NAME_EDEFAULT = SimpleTypeScriptCommandName.ADD_SELECTION;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected SimpleTypeScriptCommandName name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The default value of the '{@link #getTarget1() <em>Target1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget1()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTarget1() <em>Target1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget1()
	 * @generated
	 * @ordered
	 */
	protected String target1 = TARGET1_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue1() <em>Value1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue1()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue1() <em>Value1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue1()
	 * @generated
	 * @ordered
	 */
	protected String value1 = VALUE1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexTypeScriptCommandImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_COMMAND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTarget() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getValue() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getComment() {
		return getGroup().list(ScriptPackage.Literals.COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisabled(boolean newDisabled) {
		boolean oldDisabled = disabled;
		disabled = newDisabled;
		boolean oldDisabledESet = disabledESet;
		disabledESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED, oldDisabled, disabled, !oldDisabledESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDisabled() {
		boolean oldDisabled = disabled;
		boolean oldDisabledESet = disabledESet;
		disabled = DISABLED_EDEFAULT;
		disabledESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED, oldDisabled, DISABLED_EDEFAULT, oldDisabledESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDisabled() {
		return disabledESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleTypeScriptCommandName getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(SimpleTypeScriptCommandName newName) {
		SimpleTypeScriptCommandName oldName = name;
		name = newName == null ? NAME_EDEFAULT : newName;
		boolean oldNameESet = nameESet;
		nameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__NAME, oldName, name, !oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetName() {
		SimpleTypeScriptCommandName oldName = name;
		boolean oldNameESet = nameESet;
		name = NAME_EDEFAULT;
		nameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__NAME, oldName, NAME_EDEFAULT, oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTarget1() {
		return target1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget1(String newTarget1) {
		String oldTarget1 = target1;
		target1 = newTarget1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1, oldTarget1, target1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue1() {
		return value1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue1(String newValue1) {
		String oldValue1 = value1;
		value1 = newValue1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1, oldValue1, value1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET:
				return getTarget();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE:
				return getValue();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT:
				return getComment();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED:
				return isDisabled();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__NAME:
				return getName();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1:
				return getTarget1();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1:
				return getValue1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET:
				getTarget().clear();
				getTarget().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends String>)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED:
				setDisabled((Boolean)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__NAME:
				setName((SimpleTypeScriptCommandName)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1:
				setTarget1((String)newValue);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1:
				setValue1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__GROUP:
				getGroup().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET:
				getTarget().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE:
				getValue().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT:
				getComment().clear();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED:
				unsetDisabled();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__NAME:
				unsetName();
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1:
				setTarget1(TARGET1_EDEFAULT);
				return;
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1:
				setValue1(VALUE1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__GROUP:
				return group != null && !group.isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET:
				return !getTarget().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE:
				return !getValue().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__COMMENT:
				return !getComment().isEmpty();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__DISABLED:
				return isSetDisabled();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__NAME:
				return isSetName();
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__TARGET1:
				return TARGET1_EDEFAULT == null ? target1 != null : !TARGET1_EDEFAULT.equals(target1);
			case ScriptPackage.COMPLEX_TYPE_SCRIPT_COMMAND__VALUE1:
				return VALUE1_EDEFAULT == null ? value1 != null : !VALUE1_EDEFAULT.equals(value1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", disabled: ");
		if (disabledESet) result.append(disabled); else result.append("<unset>");
		result.append(", name: ");
		if (nameESet) result.append(name); else result.append("<unset>");
		result.append(", target1: ");
		result.append(target1);
		result.append(", value1: ");
		result.append(value1);
		result.append(')');
		return result.toString();
	}

} //ComplexTypeScriptCommandImpl
