/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.DocumentRoot#getJavamodule <em>Javamodule</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.DocumentRoot#getScriptmodule <em>Scriptmodule</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.DocumentRoot#getTestcase <em>Testcase</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XMLNS Prefix Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see com.xceptance.xlt.script.ScriptPackage#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString>" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap<String, String> getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XSI Schema Location</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see com.xceptance.xlt.script.ScriptPackage#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString>" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap<String, String> getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>Javamodule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Javamodule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Javamodule</em>' containment reference.
	 * @see #setJavamodule(ComplexTypeJavaModule)
	 * @see com.xceptance.xlt.script.ScriptPackage#getDocumentRoot_Javamodule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='javamodule' namespace='##targetNamespace'"
	 * @generated
	 */
	ComplexTypeJavaModule getJavamodule();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.DocumentRoot#getJavamodule <em>Javamodule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Javamodule</em>' containment reference.
	 * @see #getJavamodule()
	 * @generated
	 */
	void setJavamodule(ComplexTypeJavaModule value);

	/**
	 * Returns the value of the '<em><b>Scriptmodule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scriptmodule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scriptmodule</em>' containment reference.
	 * @see #setScriptmodule(ComplexTypeScriptModule1)
	 * @see com.xceptance.xlt.script.ScriptPackage#getDocumentRoot_Scriptmodule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='scriptmodule' namespace='##targetNamespace'"
	 * @generated
	 */
	ComplexTypeScriptModule1 getScriptmodule();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.DocumentRoot#getScriptmodule <em>Scriptmodule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scriptmodule</em>' containment reference.
	 * @see #getScriptmodule()
	 * @generated
	 */
	void setScriptmodule(ComplexTypeScriptModule1 value);

	/**
	 * Returns the value of the '<em><b>Testcase</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Testcase</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Testcase</em>' containment reference.
	 * @see #setTestcase(ComplexTypeTestCase)
	 * @see com.xceptance.xlt.script.ScriptPackage#getDocumentRoot_Testcase()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='testcase' namespace='##targetNamespace'"
	 * @generated
	 */
	ComplexTypeTestCase getTestcase();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.DocumentRoot#getTestcase <em>Testcase</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Testcase</em>' containment reference.
	 * @see #getTestcase()
	 * @generated
	 */
	void setTestcase(ComplexTypeTestCase value);

} // DocumentRoot
