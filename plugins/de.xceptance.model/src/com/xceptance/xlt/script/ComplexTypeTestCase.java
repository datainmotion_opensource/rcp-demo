/**
 */
package com.xceptance.xlt.script;

import java.math.BigInteger;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getTags <em>Tags</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getDescription <em>Description</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getGroupScriptActions <em>Group Script Actions</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getCommand <em>Command</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getAction <em>Action</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getModule <em>Module</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getCodecomment <em>Codecomment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getPostSteps <em>Post Steps</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getBaseURL <em>Base URL</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#isDisabled <em>Disabled</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getId <em>Id</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#isJunitTest <em>Junit Test</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeTestCase#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase()
 * @model extendedMetaData="name='complexType.TestCase' kind='elementOnly'"
 * @generated
 */
public interface ComplexTypeTestCase extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Tags</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tags</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tags</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Tags()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tags' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getTags();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Description()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getDescription();

	/**
	 * Returns the value of the '<em><b>Group Script Actions</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Script Actions</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Script Actions</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_GroupScriptActions()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='GroupScriptActions:3'"
	 * @generated
	 */
	FeatureMap getGroupScriptActions();

	/**
	 * Returns the value of the '<em><b>Command</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptCommand}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Command()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='command' namespace='##targetNamespace' group='#GroupScriptActions:3'"
	 * @generated
	 */
	EList<ComplexTypeScriptCommand> getCommand();

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Action()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='action' namespace='##targetNamespace' group='#GroupScriptActions:3'"
	 * @generated
	 */
	EList<ComplexTypeScriptAction> getAction();

	/**
	 * Returns the value of the '<em><b>Module</b></em>' containment reference list.
	 * The list contents are of type {@link com.xceptance.xlt.script.ComplexTypeScriptModule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module</em>' containment reference list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Module()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='module' namespace='##targetNamespace' group='#GroupScriptActions:3'"
	 * @generated
	 */
	EList<ComplexTypeScriptModule> getModule();

	/**
	 * Returns the value of the '<em><b>Codecomment</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Codecomment</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Codecomment</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Codecomment()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='codecomment' namespace='##targetNamespace' group='#GroupScriptActions:3'"
	 * @generated
	 */
	EList<String> getCodecomment();

	/**
	 * Returns the value of the '<em><b>Post Steps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Post Steps</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Post Steps</em>' containment reference.
	 * @see #setPostSteps(ComplexTypeTestCasePostSteps)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_PostSteps()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='postSteps' namespace='##targetNamespace'"
	 * @generated
	 */
	ComplexTypeTestCasePostSteps getPostSteps();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getPostSteps <em>Post Steps</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Post Steps</em>' containment reference.
	 * @see #getPostSteps()
	 * @generated
	 */
	void setPostSteps(ComplexTypeTestCasePostSteps value);

	/**
	 * Returns the value of the '<em><b>Base URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base URL</em>' attribute.
	 * @see #setBaseURL(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_BaseURL()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString"
	 *        extendedMetaData="kind='attribute' name='baseURL'"
	 * @generated
	 */
	String getBaseURL();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getBaseURL <em>Base URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base URL</em>' attribute.
	 * @see #getBaseURL()
	 * @generated
	 */
	void setBaseURL(String value);

	/**
	 * Returns the value of the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #setDisabled(boolean)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Disabled()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='disabled'"
	 * @generated
	 */
	boolean isDisabled();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @generated
	 */
	void setDisabled(boolean value);

	/**
	 * Unsets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	void unsetDisabled();

	/**
	 * Returns whether the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isDisabled <em>Disabled</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Disabled</em>' attribute is set.
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	boolean isSetDisabled();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Id()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Junit Test</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Junit Test</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Junit Test</em>' attribute.
	 * @see #isSetJunitTest()
	 * @see #unsetJunitTest()
	 * @see #setJunitTest(boolean)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_JunitTest()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='junit-test'"
	 * @generated
	 */
	boolean isJunitTest();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isJunitTest <em>Junit Test</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Junit Test</em>' attribute.
	 * @see #isSetJunitTest()
	 * @see #unsetJunitTest()
	 * @see #isJunitTest()
	 * @generated
	 */
	void setJunitTest(boolean value);

	/**
	 * Unsets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isJunitTest <em>Junit Test</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetJunitTest()
	 * @see #isJunitTest()
	 * @see #setJunitTest(boolean)
	 * @generated
	 */
	void unsetJunitTest();

	/**
	 * Returns whether the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#isJunitTest <em>Junit Test</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Junit Test</em>' attribute is set.
	 * @see #unsetJunitTest()
	 * @see #isJunitTest()
	 * @see #setJunitTest(boolean)
	 * @generated
	 */
	boolean isSetJunitTest();

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(BigInteger)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeTestCase_Version()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeVersionIdent" required="true"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	BigInteger getVersion();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeTestCase#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(BigInteger value);

} // ComplexTypeTestCase
