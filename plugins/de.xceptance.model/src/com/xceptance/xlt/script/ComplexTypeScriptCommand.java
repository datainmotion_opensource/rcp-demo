/**
 */
package com.xceptance.xlt.script;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Type Script Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getGroup <em>Group</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getTarget <em>Target</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getValue <em>Value</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getComment <em>Comment</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#isDisabled <em>Disabled</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getName <em>Name</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getTarget1 <em>Target1</em>}</li>
 *   <li>{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getValue1 <em>Value1</em>}</li>
 * </ul>
 *
 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand()
 * @model extendedMetaData="name='complexType.Script.Command' kind='elementOnly'"
 * @generated
 */
public interface ComplexTypeScriptCommand extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Target</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Target()
	 * @model unique="false" dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='target' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getTarget();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Value()
	 * @model unique="false" dataType="com.xceptance.xlt.script.SimpleTypeNonEmptyString" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getValue();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' attribute list.
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Comment()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getComment();

	/**
	 * Returns the value of the '<em><b>Disabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #setDisabled(boolean)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Disabled()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='disabled'"
	 * @generated
	 */
	boolean isDisabled();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disabled</em>' attribute.
	 * @see #isSetDisabled()
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @generated
	 */
	void setDisabled(boolean value);

	/**
	 * Unsets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#isDisabled <em>Disabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	void unsetDisabled();

	/**
	 * Returns whether the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#isDisabled <em>Disabled</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Disabled</em>' attribute is set.
	 * @see #unsetDisabled()
	 * @see #isDisabled()
	 * @see #setDisabled(boolean)
	 * @generated
	 */
	boolean isSetDisabled();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The literals are from the enumeration {@link com.xceptance.xlt.script.SimpleTypeScriptCommandName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #setName(SimpleTypeScriptCommandName)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Name()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='name'"
	 * @generated
	 */
	SimpleTypeScriptCommandName getName();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see com.xceptance.xlt.script.SimpleTypeScriptCommandName
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #getName()
	 * @generated
	 */
	void setName(SimpleTypeScriptCommandName value);

	/**
	 * Unsets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetName()
	 * @see #getName()
	 * @see #setName(SimpleTypeScriptCommandName)
	 * @generated
	 */
	void unsetName();

	/**
	 * Returns whether the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #unsetName()
	 * @see #getName()
	 * @see #setName(SimpleTypeScriptCommandName)
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Target1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target1</em>' attribute.
	 * @see #setTarget1(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Target1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='target'"
	 * @generated
	 */
	String getTarget1();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getTarget1 <em>Target1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target1</em>' attribute.
	 * @see #getTarget1()
	 * @generated
	 */
	void setTarget1(String value);

	/**
	 * Returns the value of the '<em><b>Value1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value1</em>' attribute.
	 * @see #setValue1(String)
	 * @see com.xceptance.xlt.script.ScriptPackage#getComplexTypeScriptCommand_Value1()
	 * @model dataType="com.xceptance.xlt.script.SimpleTypeString128"
	 *        extendedMetaData="kind='attribute' name='value'"
	 * @generated
	 */
	String getValue1();

	/**
	 * Sets the value of the '{@link com.xceptance.xlt.script.ComplexTypeScriptCommand#getValue1 <em>Value1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value1</em>' attribute.
	 * @see #getValue1()
	 * @generated
	 */
	void setValue1(String value);

} // ComplexTypeScriptCommand
