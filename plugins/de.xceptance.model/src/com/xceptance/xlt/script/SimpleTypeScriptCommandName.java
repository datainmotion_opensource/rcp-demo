/**
 */
package com.xceptance.xlt.script;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Simple Type Script Command Name</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.xceptance.xlt.script.ScriptPackage#getSimpleTypeScriptCommandName()
 * @model extendedMetaData="name='simpleType.Script.Command.Name'"
 * @generated
 */
public enum SimpleTypeScriptCommandName implements Enumerator {
	/**
	 * The '<em><b>Add Selection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADD_SELECTION_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_SELECTION(0, "addSelection", "addSelection"),

	/**
	 * The '<em><b>Assert Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_ATTRIBUTE(1, "assertAttribute", "assertAttribute"),

	/**
	 * The '<em><b>Assert Checked</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_CHECKED_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_CHECKED(2, "assertChecked", "assertChecked"),

	/**
	 * The '<em><b>Assert Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_CLASS(3, "assertClass", "assertClass"),

	/**
	 * The '<em><b>Assert Element Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_ELEMENT_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_ELEMENT_COUNT(4, "assertElementCount", "assertElementCount"),

	/**
	 * The '<em><b>Assert Element Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_ELEMENT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_ELEMENT_PRESENT(5, "assertElementPresent", "assertElementPresent"),

	/**
	 * The '<em><b>Assert Eval</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_EVAL_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_EVAL(6, "assertEval", "assertEval"),

	/**
	 * The '<em><b>Assert Load Time</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_LOAD_TIME_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_LOAD_TIME(7, "assertLoadTime", "assertLoadTime"),

	/**
	 * The '<em><b>Assert Page Size</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_PAGE_SIZE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_PAGE_SIZE(8, "assertPageSize", "assertPageSize"),

	/**
	 * The '<em><b>Assert Selected Id</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_ID_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_SELECTED_ID(9, "assertSelectedId", "assertSelectedId"),

	/**
	 * The '<em><b>Assert Selected Index</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_INDEX_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_SELECTED_INDEX(10, "assertSelectedIndex", "assertSelectedIndex"),

	/**
	 * The '<em><b>Assert Selected Label</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_LABEL_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_SELECTED_LABEL(11, "assertSelectedLabel", "assertSelectedLabel"),

	/**
	 * The '<em><b>Assert Selected Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_SELECTED_VALUE(12, "assertSelectedValue", "assertSelectedValue"),

	/**
	 * The '<em><b>Assert Style</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_STYLE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_STYLE(13, "assertStyle", "assertStyle"),

	/**
	 * The '<em><b>Assert Text</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_TEXT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_TEXT(14, "assertText", "assertText"),

	/**
	 * The '<em><b>Assert Text Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_TEXT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_TEXT_PRESENT(15, "assertTextPresent", "assertTextPresent"),

	/**
	 * The '<em><b>Assert Title</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_TITLE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_TITLE(16, "assertTitle", "assertTitle"),

	/**
	 * The '<em><b>Assert Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_VALUE(17, "assertValue", "assertValue"),

	/**
	 * The '<em><b>Assert Visible</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_VISIBLE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_VISIBLE(18, "assertVisible", "assertVisible"),

	/**
	 * The '<em><b>Assert Xpath Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_XPATH_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_XPATH_COUNT(19, "assertXpathCount", "assertXpathCount"),

	/**
	 * The '<em><b>Assert Not Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_ATTRIBUTE(20, "assertNotAttribute", "assertNotAttribute"),

	/**
	 * The '<em><b>Assert Not Checked</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_CHECKED_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_CHECKED(21, "assertNotChecked", "assertNotChecked"),

	/**
	 * The '<em><b>Assert Not Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_CLASS(22, "assertNotClass", "assertNotClass"),

	/**
	 * The '<em><b>Assert Not Element Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_ELEMENT_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_ELEMENT_COUNT(23, "assertNotElementCount", "assertNotElementCount"),

	/**
	 * The '<em><b>Assert Not Element Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_ELEMENT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_ELEMENT_PRESENT(24, "assertNotElementPresent", "assertNotElementPresent"),

	/**
	 * The '<em><b>Assert Not Eval</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_EVAL_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_EVAL(25, "assertNotEval", "assertNotEval"),

	/**
	 * The '<em><b>Assert Not Selected Id</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_ID_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_SELECTED_ID(26, "assertNotSelectedId", "assertNotSelectedId"),

	/**
	 * The '<em><b>Assert Not Selected Index</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_INDEX_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_SELECTED_INDEX(27, "assertNotSelectedIndex", "assertNotSelectedIndex"),

	/**
	 * The '<em><b>Assert Not Selected Label</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_LABEL_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_SELECTED_LABEL(28, "assertNotSelectedLabel", "assertNotSelectedLabel"),

	/**
	 * The '<em><b>Assert Not Selected Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_SELECTED_VALUE(29, "assertNotSelectedValue", "assertNotSelectedValue"),

	/**
	 * The '<em><b>Assert Not Style</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_STYLE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_STYLE(30, "assertNotStyle", "assertNotStyle"),

	/**
	 * The '<em><b>Assert Not Text</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_TEXT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_TEXT(31, "assertNotText", "assertNotText"),

	/**
	 * The '<em><b>Assert Not Text Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_TEXT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_TEXT_PRESENT(32, "assertNotTextPresent", "assertNotTextPresent"),

	/**
	 * The '<em><b>Assert Not Title</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_TITLE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_TITLE(33, "assertNotTitle", "assertNotTitle"),

	/**
	 * The '<em><b>Assert Not Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_VALUE(34, "assertNotValue", "assertNotValue"),

	/**
	 * The '<em><b>Assert Not Visible</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_VISIBLE_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_VISIBLE(35, "assertNotVisible", "assertNotVisible"),

	/**
	 * The '<em><b>Assert Not Xpath Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_XPATH_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	ASSERT_NOT_XPATH_COUNT(36, "assertNotXpathCount", "assertNotXpathCount"),

	/**
	 * The '<em><b>Check</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CHECK_VALUE
	 * @generated
	 * @ordered
	 */
	CHECK(37, "check", "check"),

	/**
	 * The '<em><b>Check And Wait</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CHECK_AND_WAIT_VALUE
	 * @generated
	 * @ordered
	 */
	CHECK_AND_WAIT(38, "checkAndWait", "checkAndWait"),

	/**
	 * The '<em><b>Click</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLICK_VALUE
	 * @generated
	 * @ordered
	 */
	CLICK(39, "click", "click"),

	/**
	 * The '<em><b>Click And Wait</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLICK_AND_WAIT_VALUE
	 * @generated
	 * @ordered
	 */
	CLICK_AND_WAIT(40, "clickAndWait", "clickAndWait"),

	/**
	 * The '<em><b>Close</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLOSE_VALUE
	 * @generated
	 * @ordered
	 */
	CLOSE(41, "close", "close"),

	/**
	 * The '<em><b>Context Menu</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTEXT_MENU_VALUE
	 * @generated
	 * @ordered
	 */
	CONTEXT_MENU(42, "contextMenu", "contextMenu"),

	/**
	 * The '<em><b>Context Menu At</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTEXT_MENU_AT_VALUE
	 * @generated
	 * @ordered
	 */
	CONTEXT_MENU_AT(43, "contextMenuAt", "contextMenuAt"),

	/**
	 * The '<em><b>Create Cookie</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CREATE_COOKIE_VALUE
	 * @generated
	 * @ordered
	 */
	CREATE_COOKIE(44, "createCookie", "createCookie"),

	/**
	 * The '<em><b>Delete All Visible Cookies</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DELETE_ALL_VISIBLE_COOKIES_VALUE
	 * @generated
	 * @ordered
	 */
	DELETE_ALL_VISIBLE_COOKIES(45, "deleteAllVisibleCookies", "deleteAllVisibleCookies"),

	/**
	 * The '<em><b>Delete Cookie</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DELETE_COOKIE_VALUE
	 * @generated
	 * @ordered
	 */
	DELETE_COOKIE(46, "deleteCookie", "deleteCookie"),

	/**
	 * The '<em><b>Double Click</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_CLICK_VALUE
	 * @generated
	 * @ordered
	 */
	DOUBLE_CLICK(47, "doubleClick", "doubleClick"),

	/**
	 * The '<em><b>Double Click And Wait</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_CLICK_AND_WAIT_VALUE
	 * @generated
	 * @ordered
	 */
	DOUBLE_CLICK_AND_WAIT(48, "doubleClickAndWait", "doubleClickAndWait"),

	/**
	 * The '<em><b>Echo</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ECHO_VALUE
	 * @generated
	 * @ordered
	 */
	ECHO(49, "echo", "echo"),

	/**
	 * The '<em><b>Mouse Down</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_DOWN_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_DOWN(50, "mouseDown", "mouseDown"),

	/**
	 * The '<em><b>Mouse Down At</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_DOWN_AT_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_DOWN_AT(51, "mouseDownAt", "mouseDownAt"),

	/**
	 * The '<em><b>Mouse Move</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_MOVE_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_MOVE(52, "mouseMove", "mouseMove"),

	/**
	 * The '<em><b>Mouse Move At</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_MOVE_AT_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_MOVE_AT(53, "mouseMoveAt", "mouseMoveAt"),

	/**
	 * The '<em><b>Mouse Out</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_OUT_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_OUT(54, "mouseOut", "mouseOut"),

	/**
	 * The '<em><b>Mouse Over</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_OVER_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_OVER(55, "mouseOver", "mouseOver"),

	/**
	 * The '<em><b>Mouse Up</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_UP_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_UP(56, "mouseUp", "mouseUp"),

	/**
	 * The '<em><b>Mouse Up At</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOUSE_UP_AT_VALUE
	 * @generated
	 * @ordered
	 */
	MOUSE_UP_AT(57, "mouseUpAt", "mouseUpAt"),

	/**
	 * The '<em><b>Open</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPEN_VALUE
	 * @generated
	 * @ordered
	 */
	OPEN(58, "open", "open"),

	/**
	 * The '<em><b>Pause</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAUSE_VALUE
	 * @generated
	 * @ordered
	 */
	PAUSE(59, "pause", "pause"),

	/**
	 * The '<em><b>Remove Selection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REMOVE_SELECTION_VALUE
	 * @generated
	 * @ordered
	 */
	REMOVE_SELECTION(60, "removeSelection", "removeSelection"),

	/**
	 * The '<em><b>Select</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECT_VALUE
	 * @generated
	 * @ordered
	 */
	SELECT(61, "select", "select"),

	/**
	 * The '<em><b>Select And Wait</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECT_AND_WAIT_VALUE
	 * @generated
	 * @ordered
	 */
	SELECT_AND_WAIT(62, "selectAndWait", "selectAndWait"),

	/**
	 * The '<em><b>Store Element Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_ELEMENT_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	STORE_ELEMENT_COUNT(63, "storeElementCount", "storeElementCount"),

	/**
	 * The '<em><b>Select Frame</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECT_FRAME_VALUE
	 * @generated
	 * @ordered
	 */
	SELECT_FRAME(64, "selectFrame", "selectFrame"),

	/**
	 * The '<em><b>Select Window</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECT_WINDOW_VALUE
	 * @generated
	 * @ordered
	 */
	SELECT_WINDOW(65, "selectWindow", "selectWindow"),

	/**
	 * The '<em><b>Set Timeout</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_TIMEOUT_VALUE
	 * @generated
	 * @ordered
	 */
	SET_TIMEOUT(66, "setTimeout", "setTimeout"),

	/**
	 * The '<em><b>Store</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_VALUE_
	 * @generated
	 * @ordered
	 */
	STORE(67, "store", "store"),

	/**
	 * The '<em><b>Store Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	STORE_ATTRIBUTE(68, "storeAttribute", "storeAttribute"),

	/**
	 * The '<em><b>Store Eval</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_EVAL_VALUE
	 * @generated
	 * @ordered
	 */
	STORE_EVAL(69, "storeEval", "storeEval"),

	/**
	 * The '<em><b>Store Text</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_TEXT_VALUE
	 * @generated
	 * @ordered
	 */
	STORE_TEXT(70, "storeText", "storeText"),

	/**
	 * The '<em><b>Store Title</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_TITLE_VALUE
	 * @generated
	 * @ordered
	 */
	STORE_TITLE(71, "storeTitle", "storeTitle"),

	/**
	 * The '<em><b>Store Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	STORE_VALUE(72, "storeValue", "storeValue"),

	/**
	 * The '<em><b>Store Xpath Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORE_XPATH_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	STORE_XPATH_COUNT(73, "storeXpathCount", "storeXpathCount"),

	/**
	 * The '<em><b>Submit</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMIT_VALUE
	 * @generated
	 * @ordered
	 */
	SUBMIT(74, "submit", "submit"),

	/**
	 * The '<em><b>Submit And Wait</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMIT_AND_WAIT_VALUE
	 * @generated
	 * @ordered
	 */
	SUBMIT_AND_WAIT(75, "submitAndWait", "submitAndWait"),

	/**
	 * The '<em><b>Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	TYPE(76, "type", "type"),

	/**
	 * The '<em><b>Type And Wait</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TYPE_AND_WAIT_VALUE
	 * @generated
	 * @ordered
	 */
	TYPE_AND_WAIT(77, "typeAndWait", "typeAndWait"),

	/**
	 * The '<em><b>Uncheck</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNCHECK_VALUE
	 * @generated
	 * @ordered
	 */
	UNCHECK(78, "uncheck", "uncheck"),

	/**
	 * The '<em><b>Uncheck And Wait</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNCHECK_AND_WAIT_VALUE
	 * @generated
	 * @ordered
	 */
	UNCHECK_AND_WAIT(79, "uncheckAndWait", "uncheckAndWait"),

	/**
	 * The '<em><b>Wait For Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_ATTRIBUTE(80, "waitForAttribute", "waitForAttribute"),

	/**
	 * The '<em><b>Wait For Checked</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_CHECKED_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_CHECKED(81, "waitForChecked", "waitForChecked"),

	/**
	 * The '<em><b>Wait For Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_CLASS(82, "waitForClass", "waitForClass"),

	/**
	 * The '<em><b>Wait For Element Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_ELEMENT_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_ELEMENT_COUNT(83, "waitForElementCount", "waitForElementCount"),

	/**
	 * The '<em><b>Wait For Page To Load</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_PAGE_TO_LOAD_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_PAGE_TO_LOAD(84, "waitForPageToLoad", "waitForPageToLoad"),

	/**
	 * The '<em><b>Wait For Pop Up</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_POP_UP_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_POP_UP(85, "waitForPopUp", "waitForPopUp"),

	/**
	 * The '<em><b>Wait For Element Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_ELEMENT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_ELEMENT_PRESENT(86, "waitForElementPresent", "waitForElementPresent"),

	/**
	 * The '<em><b>Wait For Eval</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_EVAL_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_EVAL(87, "waitForEval", "waitForEval"),

	/**
	 * The '<em><b>Wait For Selected Id</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_ID_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_SELECTED_ID(88, "waitForSelectedId", "waitForSelectedId"),

	/**
	 * The '<em><b>Wait For Selected Index</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_INDEX_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_SELECTED_INDEX(89, "waitForSelectedIndex", "waitForSelectedIndex"),

	/**
	 * The '<em><b>Wait For Selected Label</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_LABEL_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_SELECTED_LABEL(90, "waitForSelectedLabel", "waitForSelectedLabel"),

	/**
	 * The '<em><b>Wait For Selected Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_SELECTED_VALUE(91, "waitForSelectedValue", "waitForSelectedValue"),

	/**
	 * The '<em><b>Wait For Style</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_STYLE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_STYLE(92, "waitForStyle", "waitForStyle"),

	/**
	 * The '<em><b>Wait For Text</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_TEXT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_TEXT(93, "waitForText", "waitForText"),

	/**
	 * The '<em><b>Wait For Text Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_TEXT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_TEXT_PRESENT(94, "waitForTextPresent", "waitForTextPresent"),

	/**
	 * The '<em><b>Wait For Title</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_TITLE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_TITLE(95, "waitForTitle", "waitForTitle"),

	/**
	 * The '<em><b>Wait For Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_VALUE(96, "waitForValue", "waitForValue"),

	/**
	 * The '<em><b>Wait For Visible</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_VISIBLE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_VISIBLE(97, "waitForVisible", "waitForVisible"),

	/**
	 * The '<em><b>Wait For Xpath Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_XPATH_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_XPATH_COUNT(98, "waitForXpathCount", "waitForXpathCount"),

	/**
	 * The '<em><b>Wait For Not Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_ATTRIBUTE(99, "waitForNotAttribute", "waitForNotAttribute"),

	/**
	 * The '<em><b>Wait For Not Checked</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_CHECKED_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_CHECKED(100, "waitForNotChecked", "waitForNotChecked"),

	/**
	 * The '<em><b>Wait For Not Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_CLASS(101, "waitForNotClass", "waitForNotClass"),

	/**
	 * The '<em><b>Wait For Not Element Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_ELEMENT_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_ELEMENT_COUNT(102, "waitForNotElementCount", "waitForNotElementCount"),

	/**
	 * The '<em><b>Wait For Not Element Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_ELEMENT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_ELEMENT_PRESENT(103, "waitForNotElementPresent", "waitForNotElementPresent"),

	/**
	 * The '<em><b>Wait For Not Eval</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_EVAL_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_EVAL(104, "waitForNotEval", "waitForNotEval"),

	/**
	 * The '<em><b>Wait For Not Selected Id</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_ID_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_SELECTED_ID(105, "waitForNotSelectedId", "waitForNotSelectedId"),

	/**
	 * The '<em><b>Wait For Not Selected Index</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_INDEX_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_SELECTED_INDEX(106, "waitForNotSelectedIndex", "waitForNotSelectedIndex"),

	/**
	 * The '<em><b>Wait For Not Selected Label</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_LABEL_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_SELECTED_LABEL(107, "waitForNotSelectedLabel", "waitForNotSelectedLabel"),

	/**
	 * The '<em><b>Wait For Not Selected Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_SELECTED_VALUE(108, "waitForNotSelectedValue", "waitForNotSelectedValue"),

	/**
	 * The '<em><b>Wait For Not Style</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_STYLE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_STYLE(109, "waitForNotStyle", "waitForNotStyle"),

	/**
	 * The '<em><b>Wait For Not Text</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_TEXT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_TEXT(110, "waitForNotText", "waitForNotText"),

	/**
	 * The '<em><b>Wait For Not Text Present</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_TEXT_PRESENT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_TEXT_PRESENT(111, "waitForNotTextPresent", "waitForNotTextPresent"),

	/**
	 * The '<em><b>Wait For Not Title</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_TITLE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_TITLE(112, "waitForNotTitle", "waitForNotTitle"),

	/**
	 * The '<em><b>Wait For Not Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_VALUE(113, "waitForNotValue", "waitForNotValue"),

	/**
	 * The '<em><b>Wait For Not Visible</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_VISIBLE_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_VISIBLE(114, "waitForNotVisible", "waitForNotVisible"),

	/**
	 * The '<em><b>Wait For Not Xpath Count</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_XPATH_COUNT_VALUE
	 * @generated
	 * @ordered
	 */
	WAIT_FOR_NOT_XPATH_COUNT(115, "waitForNotXpathCount", "waitForNotXpathCount");

	/**
	 * The '<em><b>Add Selection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Selection</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_SELECTION
	 * @model name="addSelection"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_SELECTION_VALUE = 0;

	/**
	 * The '<em><b>Assert Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_ATTRIBUTE
	 * @model name="assertAttribute"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_ATTRIBUTE_VALUE = 1;

	/**
	 * The '<em><b>Assert Checked</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Checked</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_CHECKED
	 * @model name="assertChecked"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_CHECKED_VALUE = 2;

	/**
	 * The '<em><b>Assert Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_CLASS
	 * @model name="assertClass"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_CLASS_VALUE = 3;

	/**
	 * The '<em><b>Assert Element Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Element Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_ELEMENT_COUNT
	 * @model name="assertElementCount"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_ELEMENT_COUNT_VALUE = 4;

	/**
	 * The '<em><b>Assert Element Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Element Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_ELEMENT_PRESENT
	 * @model name="assertElementPresent"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_ELEMENT_PRESENT_VALUE = 5;

	/**
	 * The '<em><b>Assert Eval</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Eval</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_EVAL
	 * @model name="assertEval"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_EVAL_VALUE = 6;

	/**
	 * The '<em><b>Assert Load Time</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Load Time</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_LOAD_TIME
	 * @model name="assertLoadTime"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_LOAD_TIME_VALUE = 7;

	/**
	 * The '<em><b>Assert Page Size</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Page Size</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_PAGE_SIZE
	 * @model name="assertPageSize"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_PAGE_SIZE_VALUE = 8;

	/**
	 * The '<em><b>Assert Selected Id</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Selected Id</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_ID
	 * @model name="assertSelectedId"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_SELECTED_ID_VALUE = 9;

	/**
	 * The '<em><b>Assert Selected Index</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Selected Index</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_INDEX
	 * @model name="assertSelectedIndex"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_SELECTED_INDEX_VALUE = 10;

	/**
	 * The '<em><b>Assert Selected Label</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Selected Label</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_LABEL
	 * @model name="assertSelectedLabel"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_SELECTED_LABEL_VALUE = 11;

	/**
	 * The '<em><b>Assert Selected Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Selected Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_SELECTED_VALUE
	 * @model name="assertSelectedValue"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_SELECTED_VALUE_VALUE = 12;

	/**
	 * The '<em><b>Assert Style</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Style</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_STYLE
	 * @model name="assertStyle"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_STYLE_VALUE = 13;

	/**
	 * The '<em><b>Assert Text</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Text</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_TEXT
	 * @model name="assertText"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_TEXT_VALUE = 14;

	/**
	 * The '<em><b>Assert Text Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Text Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_TEXT_PRESENT
	 * @model name="assertTextPresent"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_TEXT_PRESENT_VALUE = 15;

	/**
	 * The '<em><b>Assert Title</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Title</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_TITLE
	 * @model name="assertTitle"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_TITLE_VALUE = 16;

	/**
	 * The '<em><b>Assert Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_VALUE
	 * @model name="assertValue"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_VALUE_VALUE = 17;

	/**
	 * The '<em><b>Assert Visible</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Visible</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_VISIBLE
	 * @model name="assertVisible"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_VISIBLE_VALUE = 18;

	/**
	 * The '<em><b>Assert Xpath Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Xpath Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_XPATH_COUNT
	 * @model name="assertXpathCount"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_XPATH_COUNT_VALUE = 19;

	/**
	 * The '<em><b>Assert Not Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_ATTRIBUTE
	 * @model name="assertNotAttribute"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_ATTRIBUTE_VALUE = 20;

	/**
	 * The '<em><b>Assert Not Checked</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Checked</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_CHECKED
	 * @model name="assertNotChecked"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_CHECKED_VALUE = 21;

	/**
	 * The '<em><b>Assert Not Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_CLASS
	 * @model name="assertNotClass"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_CLASS_VALUE = 22;

	/**
	 * The '<em><b>Assert Not Element Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Element Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_ELEMENT_COUNT
	 * @model name="assertNotElementCount"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_ELEMENT_COUNT_VALUE = 23;

	/**
	 * The '<em><b>Assert Not Element Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Element Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_ELEMENT_PRESENT
	 * @model name="assertNotElementPresent"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_ELEMENT_PRESENT_VALUE = 24;

	/**
	 * The '<em><b>Assert Not Eval</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Eval</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_EVAL
	 * @model name="assertNotEval"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_EVAL_VALUE = 25;

	/**
	 * The '<em><b>Assert Not Selected Id</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Selected Id</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_ID
	 * @model name="assertNotSelectedId"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_SELECTED_ID_VALUE = 26;

	/**
	 * The '<em><b>Assert Not Selected Index</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Selected Index</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_INDEX
	 * @model name="assertNotSelectedIndex"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_SELECTED_INDEX_VALUE = 27;

	/**
	 * The '<em><b>Assert Not Selected Label</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Selected Label</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_LABEL
	 * @model name="assertNotSelectedLabel"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_SELECTED_LABEL_VALUE = 28;

	/**
	 * The '<em><b>Assert Not Selected Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Selected Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_SELECTED_VALUE
	 * @model name="assertNotSelectedValue"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_SELECTED_VALUE_VALUE = 29;

	/**
	 * The '<em><b>Assert Not Style</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Style</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_STYLE
	 * @model name="assertNotStyle"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_STYLE_VALUE = 30;

	/**
	 * The '<em><b>Assert Not Text</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Text</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_TEXT
	 * @model name="assertNotText"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_TEXT_VALUE = 31;

	/**
	 * The '<em><b>Assert Not Text Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Text Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_TEXT_PRESENT
	 * @model name="assertNotTextPresent"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_TEXT_PRESENT_VALUE = 32;

	/**
	 * The '<em><b>Assert Not Title</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Title</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_TITLE
	 * @model name="assertNotTitle"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_TITLE_VALUE = 33;

	/**
	 * The '<em><b>Assert Not Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_VALUE
	 * @model name="assertNotValue"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_VALUE_VALUE = 34;

	/**
	 * The '<em><b>Assert Not Visible</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Visible</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_VISIBLE
	 * @model name="assertNotVisible"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_VISIBLE_VALUE = 35;

	/**
	 * The '<em><b>Assert Not Xpath Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Assert Not Xpath Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSERT_NOT_XPATH_COUNT
	 * @model name="assertNotXpathCount"
	 * @generated
	 * @ordered
	 */
	public static final int ASSERT_NOT_XPATH_COUNT_VALUE = 36;

	/**
	 * The '<em><b>Check</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Check</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CHECK
	 * @model name="check"
	 * @generated
	 * @ordered
	 */
	public static final int CHECK_VALUE = 37;

	/**
	 * The '<em><b>Check And Wait</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Check And Wait</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CHECK_AND_WAIT
	 * @model name="checkAndWait"
	 * @generated
	 * @ordered
	 */
	public static final int CHECK_AND_WAIT_VALUE = 38;

	/**
	 * The '<em><b>Click</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Click</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLICK
	 * @model name="click"
	 * @generated
	 * @ordered
	 */
	public static final int CLICK_VALUE = 39;

	/**
	 * The '<em><b>Click And Wait</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Click And Wait</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLICK_AND_WAIT
	 * @model name="clickAndWait"
	 * @generated
	 * @ordered
	 */
	public static final int CLICK_AND_WAIT_VALUE = 40;

	/**
	 * The '<em><b>Close</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Close</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLOSE
	 * @model name="close"
	 * @generated
	 * @ordered
	 */
	public static final int CLOSE_VALUE = 41;

	/**
	 * The '<em><b>Context Menu</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Context Menu</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONTEXT_MENU
	 * @model name="contextMenu"
	 * @generated
	 * @ordered
	 */
	public static final int CONTEXT_MENU_VALUE = 42;

	/**
	 * The '<em><b>Context Menu At</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Context Menu At</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONTEXT_MENU_AT
	 * @model name="contextMenuAt"
	 * @generated
	 * @ordered
	 */
	public static final int CONTEXT_MENU_AT_VALUE = 43;

	/**
	 * The '<em><b>Create Cookie</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Create Cookie</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CREATE_COOKIE
	 * @model name="createCookie"
	 * @generated
	 * @ordered
	 */
	public static final int CREATE_COOKIE_VALUE = 44;

	/**
	 * The '<em><b>Delete All Visible Cookies</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Delete All Visible Cookies</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DELETE_ALL_VISIBLE_COOKIES
	 * @model name="deleteAllVisibleCookies"
	 * @generated
	 * @ordered
	 */
	public static final int DELETE_ALL_VISIBLE_COOKIES_VALUE = 45;

	/**
	 * The '<em><b>Delete Cookie</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Delete Cookie</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DELETE_COOKIE
	 * @model name="deleteCookie"
	 * @generated
	 * @ordered
	 */
	public static final int DELETE_COOKIE_VALUE = 46;

	/**
	 * The '<em><b>Double Click</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Double Click</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_CLICK
	 * @model name="doubleClick"
	 * @generated
	 * @ordered
	 */
	public static final int DOUBLE_CLICK_VALUE = 47;

	/**
	 * The '<em><b>Double Click And Wait</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Double Click And Wait</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_CLICK_AND_WAIT
	 * @model name="doubleClickAndWait"
	 * @generated
	 * @ordered
	 */
	public static final int DOUBLE_CLICK_AND_WAIT_VALUE = 48;

	/**
	 * The '<em><b>Echo</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Echo</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ECHO
	 * @model name="echo"
	 * @generated
	 * @ordered
	 */
	public static final int ECHO_VALUE = 49;

	/**
	 * The '<em><b>Mouse Down</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Down</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_DOWN
	 * @model name="mouseDown"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_DOWN_VALUE = 50;

	/**
	 * The '<em><b>Mouse Down At</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Down At</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_DOWN_AT
	 * @model name="mouseDownAt"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_DOWN_AT_VALUE = 51;

	/**
	 * The '<em><b>Mouse Move</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Move</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_MOVE
	 * @model name="mouseMove"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_MOVE_VALUE = 52;

	/**
	 * The '<em><b>Mouse Move At</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Move At</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_MOVE_AT
	 * @model name="mouseMoveAt"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_MOVE_AT_VALUE = 53;

	/**
	 * The '<em><b>Mouse Out</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Out</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_OUT
	 * @model name="mouseOut"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_OUT_VALUE = 54;

	/**
	 * The '<em><b>Mouse Over</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Over</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_OVER
	 * @model name="mouseOver"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_OVER_VALUE = 55;

	/**
	 * The '<em><b>Mouse Up</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Up</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_UP
	 * @model name="mouseUp"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_UP_VALUE = 56;

	/**
	 * The '<em><b>Mouse Up At</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mouse Up At</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOUSE_UP_AT
	 * @model name="mouseUpAt"
	 * @generated
	 * @ordered
	 */
	public static final int MOUSE_UP_AT_VALUE = 57;

	/**
	 * The '<em><b>Open</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Open</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPEN
	 * @model name="open"
	 * @generated
	 * @ordered
	 */
	public static final int OPEN_VALUE = 58;

	/**
	 * The '<em><b>Pause</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pause</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PAUSE
	 * @model name="pause"
	 * @generated
	 * @ordered
	 */
	public static final int PAUSE_VALUE = 59;

	/**
	 * The '<em><b>Remove Selection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Remove Selection</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REMOVE_SELECTION
	 * @model name="removeSelection"
	 * @generated
	 * @ordered
	 */
	public static final int REMOVE_SELECTION_VALUE = 60;

	/**
	 * The '<em><b>Select</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Select</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECT
	 * @model name="select"
	 * @generated
	 * @ordered
	 */
	public static final int SELECT_VALUE = 61;

	/**
	 * The '<em><b>Select And Wait</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Select And Wait</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECT_AND_WAIT
	 * @model name="selectAndWait"
	 * @generated
	 * @ordered
	 */
	public static final int SELECT_AND_WAIT_VALUE = 62;

	/**
	 * The '<em><b>Store Element Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store Element Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE_ELEMENT_COUNT
	 * @model name="storeElementCount"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_ELEMENT_COUNT_VALUE = 63;

	/**
	 * The '<em><b>Select Frame</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Select Frame</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECT_FRAME
	 * @model name="selectFrame"
	 * @generated
	 * @ordered
	 */
	public static final int SELECT_FRAME_VALUE = 64;

	/**
	 * The '<em><b>Select Window</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Select Window</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECT_WINDOW
	 * @model name="selectWindow"
	 * @generated
	 * @ordered
	 */
	public static final int SELECT_WINDOW_VALUE = 65;

	/**
	 * The '<em><b>Set Timeout</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Timeout</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_TIMEOUT
	 * @model name="setTimeout"
	 * @generated
	 * @ordered
	 */
	public static final int SET_TIMEOUT_VALUE = 66;

	/**
	 * The '<em><b>Store</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE
	 * @model name="store"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_VALUE_ = 67;

	/**
	 * The '<em><b>Store Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE_ATTRIBUTE
	 * @model name="storeAttribute"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_ATTRIBUTE_VALUE = 68;

	/**
	 * The '<em><b>Store Eval</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store Eval</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE_EVAL
	 * @model name="storeEval"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_EVAL_VALUE = 69;

	/**
	 * The '<em><b>Store Text</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store Text</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE_TEXT
	 * @model name="storeText"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_TEXT_VALUE = 70;

	/**
	 * The '<em><b>Store Title</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store Title</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE_TITLE
	 * @model name="storeTitle"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_TITLE_VALUE = 71;

	/**
	 * The '<em><b>Store Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE_VALUE
	 * @model name="storeValue"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_VALUE_VALUE = 72;

	/**
	 * The '<em><b>Store Xpath Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Store Xpath Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORE_XPATH_COUNT
	 * @model name="storeXpathCount"
	 * @generated
	 * @ordered
	 */
	public static final int STORE_XPATH_COUNT_VALUE = 73;

	/**
	 * The '<em><b>Submit</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Submit</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUBMIT
	 * @model name="submit"
	 * @generated
	 * @ordered
	 */
	public static final int SUBMIT_VALUE = 74;

	/**
	 * The '<em><b>Submit And Wait</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Submit And Wait</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUBMIT_AND_WAIT
	 * @model name="submitAndWait"
	 * @generated
	 * @ordered
	 */
	public static final int SUBMIT_AND_WAIT_VALUE = 75;

	/**
	 * The '<em><b>Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TYPE
	 * @model name="type"
	 * @generated
	 * @ordered
	 */
	public static final int TYPE_VALUE = 76;

	/**
	 * The '<em><b>Type And Wait</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Type And Wait</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TYPE_AND_WAIT
	 * @model name="typeAndWait"
	 * @generated
	 * @ordered
	 */
	public static final int TYPE_AND_WAIT_VALUE = 77;

	/**
	 * The '<em><b>Uncheck</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Uncheck</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNCHECK
	 * @model name="uncheck"
	 * @generated
	 * @ordered
	 */
	public static final int UNCHECK_VALUE = 78;

	/**
	 * The '<em><b>Uncheck And Wait</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Uncheck And Wait</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNCHECK_AND_WAIT
	 * @model name="uncheckAndWait"
	 * @generated
	 * @ordered
	 */
	public static final int UNCHECK_AND_WAIT_VALUE = 79;

	/**
	 * The '<em><b>Wait For Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_ATTRIBUTE
	 * @model name="waitForAttribute"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_ATTRIBUTE_VALUE = 80;

	/**
	 * The '<em><b>Wait For Checked</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Checked</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_CHECKED
	 * @model name="waitForChecked"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_CHECKED_VALUE = 81;

	/**
	 * The '<em><b>Wait For Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_CLASS
	 * @model name="waitForClass"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_CLASS_VALUE = 82;

	/**
	 * The '<em><b>Wait For Element Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Element Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_ELEMENT_COUNT
	 * @model name="waitForElementCount"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_ELEMENT_COUNT_VALUE = 83;

	/**
	 * The '<em><b>Wait For Page To Load</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Page To Load</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_PAGE_TO_LOAD
	 * @model name="waitForPageToLoad"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_PAGE_TO_LOAD_VALUE = 84;

	/**
	 * The '<em><b>Wait For Pop Up</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Pop Up</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_POP_UP
	 * @model name="waitForPopUp"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_POP_UP_VALUE = 85;

	/**
	 * The '<em><b>Wait For Element Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Element Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_ELEMENT_PRESENT
	 * @model name="waitForElementPresent"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_ELEMENT_PRESENT_VALUE = 86;

	/**
	 * The '<em><b>Wait For Eval</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Eval</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_EVAL
	 * @model name="waitForEval"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_EVAL_VALUE = 87;

	/**
	 * The '<em><b>Wait For Selected Id</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Selected Id</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_ID
	 * @model name="waitForSelectedId"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_SELECTED_ID_VALUE = 88;

	/**
	 * The '<em><b>Wait For Selected Index</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Selected Index</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_INDEX
	 * @model name="waitForSelectedIndex"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_SELECTED_INDEX_VALUE = 89;

	/**
	 * The '<em><b>Wait For Selected Label</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Selected Label</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_LABEL
	 * @model name="waitForSelectedLabel"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_SELECTED_LABEL_VALUE = 90;

	/**
	 * The '<em><b>Wait For Selected Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Selected Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_SELECTED_VALUE
	 * @model name="waitForSelectedValue"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_SELECTED_VALUE_VALUE = 91;

	/**
	 * The '<em><b>Wait For Style</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Style</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_STYLE
	 * @model name="waitForStyle"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_STYLE_VALUE = 92;

	/**
	 * The '<em><b>Wait For Text</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Text</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_TEXT
	 * @model name="waitForText"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_TEXT_VALUE = 93;

	/**
	 * The '<em><b>Wait For Text Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Text Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_TEXT_PRESENT
	 * @model name="waitForTextPresent"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_TEXT_PRESENT_VALUE = 94;

	/**
	 * The '<em><b>Wait For Title</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Title</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_TITLE
	 * @model name="waitForTitle"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_TITLE_VALUE = 95;

	/**
	 * The '<em><b>Wait For Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_VALUE
	 * @model name="waitForValue"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_VALUE_VALUE = 96;

	/**
	 * The '<em><b>Wait For Visible</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Visible</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_VISIBLE
	 * @model name="waitForVisible"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_VISIBLE_VALUE = 97;

	/**
	 * The '<em><b>Wait For Xpath Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Xpath Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_XPATH_COUNT
	 * @model name="waitForXpathCount"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_XPATH_COUNT_VALUE = 98;

	/**
	 * The '<em><b>Wait For Not Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_ATTRIBUTE
	 * @model name="waitForNotAttribute"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_ATTRIBUTE_VALUE = 99;

	/**
	 * The '<em><b>Wait For Not Checked</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Checked</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_CHECKED
	 * @model name="waitForNotChecked"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_CHECKED_VALUE = 100;

	/**
	 * The '<em><b>Wait For Not Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_CLASS
	 * @model name="waitForNotClass"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_CLASS_VALUE = 101;

	/**
	 * The '<em><b>Wait For Not Element Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Element Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_ELEMENT_COUNT
	 * @model name="waitForNotElementCount"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_ELEMENT_COUNT_VALUE = 102;

	/**
	 * The '<em><b>Wait For Not Element Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Element Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_ELEMENT_PRESENT
	 * @model name="waitForNotElementPresent"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_ELEMENT_PRESENT_VALUE = 103;

	/**
	 * The '<em><b>Wait For Not Eval</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Eval</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_EVAL
	 * @model name="waitForNotEval"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_EVAL_VALUE = 104;

	/**
	 * The '<em><b>Wait For Not Selected Id</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Selected Id</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_ID
	 * @model name="waitForNotSelectedId"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_SELECTED_ID_VALUE = 105;

	/**
	 * The '<em><b>Wait For Not Selected Index</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Selected Index</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_INDEX
	 * @model name="waitForNotSelectedIndex"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_SELECTED_INDEX_VALUE = 106;

	/**
	 * The '<em><b>Wait For Not Selected Label</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Selected Label</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_LABEL
	 * @model name="waitForNotSelectedLabel"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_SELECTED_LABEL_VALUE = 107;

	/**
	 * The '<em><b>Wait For Not Selected Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Selected Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_SELECTED_VALUE
	 * @model name="waitForNotSelectedValue"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_SELECTED_VALUE_VALUE = 108;

	/**
	 * The '<em><b>Wait For Not Style</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Style</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_STYLE
	 * @model name="waitForNotStyle"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_STYLE_VALUE = 109;

	/**
	 * The '<em><b>Wait For Not Text</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Text</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_TEXT
	 * @model name="waitForNotText"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_TEXT_VALUE = 110;

	/**
	 * The '<em><b>Wait For Not Text Present</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Text Present</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_TEXT_PRESENT
	 * @model name="waitForNotTextPresent"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_TEXT_PRESENT_VALUE = 111;

	/**
	 * The '<em><b>Wait For Not Title</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Title</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_TITLE
	 * @model name="waitForNotTitle"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_TITLE_VALUE = 112;

	/**
	 * The '<em><b>Wait For Not Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_VALUE
	 * @model name="waitForNotValue"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_VALUE_VALUE = 113;

	/**
	 * The '<em><b>Wait For Not Visible</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Visible</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_VISIBLE
	 * @model name="waitForNotVisible"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_VISIBLE_VALUE = 114;

	/**
	 * The '<em><b>Wait For Not Xpath Count</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Wait For Not Xpath Count</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WAIT_FOR_NOT_XPATH_COUNT
	 * @model name="waitForNotXpathCount"
	 * @generated
	 * @ordered
	 */
	public static final int WAIT_FOR_NOT_XPATH_COUNT_VALUE = 115;

	/**
	 * An array of all the '<em><b>Simple Type Script Command Name</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SimpleTypeScriptCommandName[] VALUES_ARRAY =
		new SimpleTypeScriptCommandName[] {
			ADD_SELECTION,
			ASSERT_ATTRIBUTE,
			ASSERT_CHECKED,
			ASSERT_CLASS,
			ASSERT_ELEMENT_COUNT,
			ASSERT_ELEMENT_PRESENT,
			ASSERT_EVAL,
			ASSERT_LOAD_TIME,
			ASSERT_PAGE_SIZE,
			ASSERT_SELECTED_ID,
			ASSERT_SELECTED_INDEX,
			ASSERT_SELECTED_LABEL,
			ASSERT_SELECTED_VALUE,
			ASSERT_STYLE,
			ASSERT_TEXT,
			ASSERT_TEXT_PRESENT,
			ASSERT_TITLE,
			ASSERT_VALUE,
			ASSERT_VISIBLE,
			ASSERT_XPATH_COUNT,
			ASSERT_NOT_ATTRIBUTE,
			ASSERT_NOT_CHECKED,
			ASSERT_NOT_CLASS,
			ASSERT_NOT_ELEMENT_COUNT,
			ASSERT_NOT_ELEMENT_PRESENT,
			ASSERT_NOT_EVAL,
			ASSERT_NOT_SELECTED_ID,
			ASSERT_NOT_SELECTED_INDEX,
			ASSERT_NOT_SELECTED_LABEL,
			ASSERT_NOT_SELECTED_VALUE,
			ASSERT_NOT_STYLE,
			ASSERT_NOT_TEXT,
			ASSERT_NOT_TEXT_PRESENT,
			ASSERT_NOT_TITLE,
			ASSERT_NOT_VALUE,
			ASSERT_NOT_VISIBLE,
			ASSERT_NOT_XPATH_COUNT,
			CHECK,
			CHECK_AND_WAIT,
			CLICK,
			CLICK_AND_WAIT,
			CLOSE,
			CONTEXT_MENU,
			CONTEXT_MENU_AT,
			CREATE_COOKIE,
			DELETE_ALL_VISIBLE_COOKIES,
			DELETE_COOKIE,
			DOUBLE_CLICK,
			DOUBLE_CLICK_AND_WAIT,
			ECHO,
			MOUSE_DOWN,
			MOUSE_DOWN_AT,
			MOUSE_MOVE,
			MOUSE_MOVE_AT,
			MOUSE_OUT,
			MOUSE_OVER,
			MOUSE_UP,
			MOUSE_UP_AT,
			OPEN,
			PAUSE,
			REMOVE_SELECTION,
			SELECT,
			SELECT_AND_WAIT,
			STORE_ELEMENT_COUNT,
			SELECT_FRAME,
			SELECT_WINDOW,
			SET_TIMEOUT,
			STORE,
			STORE_ATTRIBUTE,
			STORE_EVAL,
			STORE_TEXT,
			STORE_TITLE,
			STORE_VALUE,
			STORE_XPATH_COUNT,
			SUBMIT,
			SUBMIT_AND_WAIT,
			TYPE,
			TYPE_AND_WAIT,
			UNCHECK,
			UNCHECK_AND_WAIT,
			WAIT_FOR_ATTRIBUTE,
			WAIT_FOR_CHECKED,
			WAIT_FOR_CLASS,
			WAIT_FOR_ELEMENT_COUNT,
			WAIT_FOR_PAGE_TO_LOAD,
			WAIT_FOR_POP_UP,
			WAIT_FOR_ELEMENT_PRESENT,
			WAIT_FOR_EVAL,
			WAIT_FOR_SELECTED_ID,
			WAIT_FOR_SELECTED_INDEX,
			WAIT_FOR_SELECTED_LABEL,
			WAIT_FOR_SELECTED_VALUE,
			WAIT_FOR_STYLE,
			WAIT_FOR_TEXT,
			WAIT_FOR_TEXT_PRESENT,
			WAIT_FOR_TITLE,
			WAIT_FOR_VALUE,
			WAIT_FOR_VISIBLE,
			WAIT_FOR_XPATH_COUNT,
			WAIT_FOR_NOT_ATTRIBUTE,
			WAIT_FOR_NOT_CHECKED,
			WAIT_FOR_NOT_CLASS,
			WAIT_FOR_NOT_ELEMENT_COUNT,
			WAIT_FOR_NOT_ELEMENT_PRESENT,
			WAIT_FOR_NOT_EVAL,
			WAIT_FOR_NOT_SELECTED_ID,
			WAIT_FOR_NOT_SELECTED_INDEX,
			WAIT_FOR_NOT_SELECTED_LABEL,
			WAIT_FOR_NOT_SELECTED_VALUE,
			WAIT_FOR_NOT_STYLE,
			WAIT_FOR_NOT_TEXT,
			WAIT_FOR_NOT_TEXT_PRESENT,
			WAIT_FOR_NOT_TITLE,
			WAIT_FOR_NOT_VALUE,
			WAIT_FOR_NOT_VISIBLE,
			WAIT_FOR_NOT_XPATH_COUNT,
		};

	/**
	 * A public read-only list of all the '<em><b>Simple Type Script Command Name</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SimpleTypeScriptCommandName> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Simple Type Script Command Name</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SimpleTypeScriptCommandName get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SimpleTypeScriptCommandName result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Simple Type Script Command Name</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SimpleTypeScriptCommandName getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SimpleTypeScriptCommandName result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Simple Type Script Command Name</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SimpleTypeScriptCommandName get(int value) {
		switch (value) {
			case ADD_SELECTION_VALUE: return ADD_SELECTION;
			case ASSERT_ATTRIBUTE_VALUE: return ASSERT_ATTRIBUTE;
			case ASSERT_CHECKED_VALUE: return ASSERT_CHECKED;
			case ASSERT_CLASS_VALUE: return ASSERT_CLASS;
			case ASSERT_ELEMENT_COUNT_VALUE: return ASSERT_ELEMENT_COUNT;
			case ASSERT_ELEMENT_PRESENT_VALUE: return ASSERT_ELEMENT_PRESENT;
			case ASSERT_EVAL_VALUE: return ASSERT_EVAL;
			case ASSERT_LOAD_TIME_VALUE: return ASSERT_LOAD_TIME;
			case ASSERT_PAGE_SIZE_VALUE: return ASSERT_PAGE_SIZE;
			case ASSERT_SELECTED_ID_VALUE: return ASSERT_SELECTED_ID;
			case ASSERT_SELECTED_INDEX_VALUE: return ASSERT_SELECTED_INDEX;
			case ASSERT_SELECTED_LABEL_VALUE: return ASSERT_SELECTED_LABEL;
			case ASSERT_SELECTED_VALUE_VALUE: return ASSERT_SELECTED_VALUE;
			case ASSERT_STYLE_VALUE: return ASSERT_STYLE;
			case ASSERT_TEXT_VALUE: return ASSERT_TEXT;
			case ASSERT_TEXT_PRESENT_VALUE: return ASSERT_TEXT_PRESENT;
			case ASSERT_TITLE_VALUE: return ASSERT_TITLE;
			case ASSERT_VALUE_VALUE: return ASSERT_VALUE;
			case ASSERT_VISIBLE_VALUE: return ASSERT_VISIBLE;
			case ASSERT_XPATH_COUNT_VALUE: return ASSERT_XPATH_COUNT;
			case ASSERT_NOT_ATTRIBUTE_VALUE: return ASSERT_NOT_ATTRIBUTE;
			case ASSERT_NOT_CHECKED_VALUE: return ASSERT_NOT_CHECKED;
			case ASSERT_NOT_CLASS_VALUE: return ASSERT_NOT_CLASS;
			case ASSERT_NOT_ELEMENT_COUNT_VALUE: return ASSERT_NOT_ELEMENT_COUNT;
			case ASSERT_NOT_ELEMENT_PRESENT_VALUE: return ASSERT_NOT_ELEMENT_PRESENT;
			case ASSERT_NOT_EVAL_VALUE: return ASSERT_NOT_EVAL;
			case ASSERT_NOT_SELECTED_ID_VALUE: return ASSERT_NOT_SELECTED_ID;
			case ASSERT_NOT_SELECTED_INDEX_VALUE: return ASSERT_NOT_SELECTED_INDEX;
			case ASSERT_NOT_SELECTED_LABEL_VALUE: return ASSERT_NOT_SELECTED_LABEL;
			case ASSERT_NOT_SELECTED_VALUE_VALUE: return ASSERT_NOT_SELECTED_VALUE;
			case ASSERT_NOT_STYLE_VALUE: return ASSERT_NOT_STYLE;
			case ASSERT_NOT_TEXT_VALUE: return ASSERT_NOT_TEXT;
			case ASSERT_NOT_TEXT_PRESENT_VALUE: return ASSERT_NOT_TEXT_PRESENT;
			case ASSERT_NOT_TITLE_VALUE: return ASSERT_NOT_TITLE;
			case ASSERT_NOT_VALUE_VALUE: return ASSERT_NOT_VALUE;
			case ASSERT_NOT_VISIBLE_VALUE: return ASSERT_NOT_VISIBLE;
			case ASSERT_NOT_XPATH_COUNT_VALUE: return ASSERT_NOT_XPATH_COUNT;
			case CHECK_VALUE: return CHECK;
			case CHECK_AND_WAIT_VALUE: return CHECK_AND_WAIT;
			case CLICK_VALUE: return CLICK;
			case CLICK_AND_WAIT_VALUE: return CLICK_AND_WAIT;
			case CLOSE_VALUE: return CLOSE;
			case CONTEXT_MENU_VALUE: return CONTEXT_MENU;
			case CONTEXT_MENU_AT_VALUE: return CONTEXT_MENU_AT;
			case CREATE_COOKIE_VALUE: return CREATE_COOKIE;
			case DELETE_ALL_VISIBLE_COOKIES_VALUE: return DELETE_ALL_VISIBLE_COOKIES;
			case DELETE_COOKIE_VALUE: return DELETE_COOKIE;
			case DOUBLE_CLICK_VALUE: return DOUBLE_CLICK;
			case DOUBLE_CLICK_AND_WAIT_VALUE: return DOUBLE_CLICK_AND_WAIT;
			case ECHO_VALUE: return ECHO;
			case MOUSE_DOWN_VALUE: return MOUSE_DOWN;
			case MOUSE_DOWN_AT_VALUE: return MOUSE_DOWN_AT;
			case MOUSE_MOVE_VALUE: return MOUSE_MOVE;
			case MOUSE_MOVE_AT_VALUE: return MOUSE_MOVE_AT;
			case MOUSE_OUT_VALUE: return MOUSE_OUT;
			case MOUSE_OVER_VALUE: return MOUSE_OVER;
			case MOUSE_UP_VALUE: return MOUSE_UP;
			case MOUSE_UP_AT_VALUE: return MOUSE_UP_AT;
			case OPEN_VALUE: return OPEN;
			case PAUSE_VALUE: return PAUSE;
			case REMOVE_SELECTION_VALUE: return REMOVE_SELECTION;
			case SELECT_VALUE: return SELECT;
			case SELECT_AND_WAIT_VALUE: return SELECT_AND_WAIT;
			case STORE_ELEMENT_COUNT_VALUE: return STORE_ELEMENT_COUNT;
			case SELECT_FRAME_VALUE: return SELECT_FRAME;
			case SELECT_WINDOW_VALUE: return SELECT_WINDOW;
			case SET_TIMEOUT_VALUE: return SET_TIMEOUT;
			case STORE_VALUE_: return STORE;
			case STORE_ATTRIBUTE_VALUE: return STORE_ATTRIBUTE;
			case STORE_EVAL_VALUE: return STORE_EVAL;
			case STORE_TEXT_VALUE: return STORE_TEXT;
			case STORE_TITLE_VALUE: return STORE_TITLE;
			case STORE_VALUE_VALUE: return STORE_VALUE;
			case STORE_XPATH_COUNT_VALUE: return STORE_XPATH_COUNT;
			case SUBMIT_VALUE: return SUBMIT;
			case SUBMIT_AND_WAIT_VALUE: return SUBMIT_AND_WAIT;
			case TYPE_VALUE: return TYPE;
			case TYPE_AND_WAIT_VALUE: return TYPE_AND_WAIT;
			case UNCHECK_VALUE: return UNCHECK;
			case UNCHECK_AND_WAIT_VALUE: return UNCHECK_AND_WAIT;
			case WAIT_FOR_ATTRIBUTE_VALUE: return WAIT_FOR_ATTRIBUTE;
			case WAIT_FOR_CHECKED_VALUE: return WAIT_FOR_CHECKED;
			case WAIT_FOR_CLASS_VALUE: return WAIT_FOR_CLASS;
			case WAIT_FOR_ELEMENT_COUNT_VALUE: return WAIT_FOR_ELEMENT_COUNT;
			case WAIT_FOR_PAGE_TO_LOAD_VALUE: return WAIT_FOR_PAGE_TO_LOAD;
			case WAIT_FOR_POP_UP_VALUE: return WAIT_FOR_POP_UP;
			case WAIT_FOR_ELEMENT_PRESENT_VALUE: return WAIT_FOR_ELEMENT_PRESENT;
			case WAIT_FOR_EVAL_VALUE: return WAIT_FOR_EVAL;
			case WAIT_FOR_SELECTED_ID_VALUE: return WAIT_FOR_SELECTED_ID;
			case WAIT_FOR_SELECTED_INDEX_VALUE: return WAIT_FOR_SELECTED_INDEX;
			case WAIT_FOR_SELECTED_LABEL_VALUE: return WAIT_FOR_SELECTED_LABEL;
			case WAIT_FOR_SELECTED_VALUE_VALUE: return WAIT_FOR_SELECTED_VALUE;
			case WAIT_FOR_STYLE_VALUE: return WAIT_FOR_STYLE;
			case WAIT_FOR_TEXT_VALUE: return WAIT_FOR_TEXT;
			case WAIT_FOR_TEXT_PRESENT_VALUE: return WAIT_FOR_TEXT_PRESENT;
			case WAIT_FOR_TITLE_VALUE: return WAIT_FOR_TITLE;
			case WAIT_FOR_VALUE_VALUE: return WAIT_FOR_VALUE;
			case WAIT_FOR_VISIBLE_VALUE: return WAIT_FOR_VISIBLE;
			case WAIT_FOR_XPATH_COUNT_VALUE: return WAIT_FOR_XPATH_COUNT;
			case WAIT_FOR_NOT_ATTRIBUTE_VALUE: return WAIT_FOR_NOT_ATTRIBUTE;
			case WAIT_FOR_NOT_CHECKED_VALUE: return WAIT_FOR_NOT_CHECKED;
			case WAIT_FOR_NOT_CLASS_VALUE: return WAIT_FOR_NOT_CLASS;
			case WAIT_FOR_NOT_ELEMENT_COUNT_VALUE: return WAIT_FOR_NOT_ELEMENT_COUNT;
			case WAIT_FOR_NOT_ELEMENT_PRESENT_VALUE: return WAIT_FOR_NOT_ELEMENT_PRESENT;
			case WAIT_FOR_NOT_EVAL_VALUE: return WAIT_FOR_NOT_EVAL;
			case WAIT_FOR_NOT_SELECTED_ID_VALUE: return WAIT_FOR_NOT_SELECTED_ID;
			case WAIT_FOR_NOT_SELECTED_INDEX_VALUE: return WAIT_FOR_NOT_SELECTED_INDEX;
			case WAIT_FOR_NOT_SELECTED_LABEL_VALUE: return WAIT_FOR_NOT_SELECTED_LABEL;
			case WAIT_FOR_NOT_SELECTED_VALUE_VALUE: return WAIT_FOR_NOT_SELECTED_VALUE;
			case WAIT_FOR_NOT_STYLE_VALUE: return WAIT_FOR_NOT_STYLE;
			case WAIT_FOR_NOT_TEXT_VALUE: return WAIT_FOR_NOT_TEXT;
			case WAIT_FOR_NOT_TEXT_PRESENT_VALUE: return WAIT_FOR_NOT_TEXT_PRESENT;
			case WAIT_FOR_NOT_TITLE_VALUE: return WAIT_FOR_NOT_TITLE;
			case WAIT_FOR_NOT_VALUE_VALUE: return WAIT_FOR_NOT_VALUE;
			case WAIT_FOR_NOT_VISIBLE_VALUE: return WAIT_FOR_NOT_VISIBLE;
			case WAIT_FOR_NOT_XPATH_COUNT_VALUE: return WAIT_FOR_NOT_XPATH_COUNT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SimpleTypeScriptCommandName(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SimpleTypeScriptCommandName
