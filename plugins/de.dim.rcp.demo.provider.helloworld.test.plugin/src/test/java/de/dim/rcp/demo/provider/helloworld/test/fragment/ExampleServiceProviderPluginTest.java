/**
 * Copyright (c) 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package de.dim.rcp.demo.provider.helloworld.test.fragment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import de.dim.rcp.demo.api.helloworld.IExampleService;

/**
 * This is more like an integration test. Here you will have a full osig environment together with the ability to access the service registry. 
 * Here we can look if our services are available and can call them as needed.
 * @author Juergen Albert
 */
public class ExampleServiceProviderPluginTest {

	/**
	 * A ServiceTrackerCustomizer can be handed over to a ServiceTracker, which is another way of getting informed about the lifecycle of services,
	 * if dependency injection is not the way you need at the moment.
	 * 
	 * @author Juergen Albert
	 *
	 */
	public static class ExampleTracker implements ServiceTrackerCustomizer<IExampleService, IExampleService> {

		private BundleContext context;
		private CountDownLatch addCounter;
		List<ServiceReference<IExampleService>> references = new LinkedList<>();
		
		
		public ExampleTracker(BundleContext context, CountDownLatch addCoutner){
			this.context = context;
			this.addCounter = addCoutner;
			
		}
		
		/* (non-Javadoc)
		 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
		 */
		@Override
		public IExampleService addingService(ServiceReference<IExampleService> reference) {
			addCounter.countDown();
			references.add(reference);
			return context.getService(reference);
		}

		/* (non-Javadoc)
		 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference, java.lang.Object)
		 */
		@Override
		public void modifiedService(ServiceReference<IExampleService> reference, IExampleService service) {
			// Nothing todo here
		}

		/* (non-Javadoc)
		 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
		 */
		@Override
		public void removedService(ServiceReference<IExampleService> reference, IExampleService service) {
			references.remove(reference);
		}
		
		/**
		 * Returns the reference with given name property
		 * @param name the configured name of the service
		 * @return
		 */
		public ServiceReference<IExampleService> getServiceReferenceByName(String name){
			for(ServiceReference<IExampleService> ref : references){
				if(ref.getProperty("name") != null && ref.getProperty("name").equals(name)){
					return ref;
				}
			}
			return null;
		}
		
	}
	
	
	/**
	 * Here we will test if our services will be registered as expected and with the properties we expect.
	 * @throws InterruptedException 
	 */
	@Test
	public void test() throws InterruptedException {

		
		
		Bundle bundle = FrameworkUtil.getBundle(getClass());
		
		//We Expect at least to services to be added
		CountDownLatch latch = new CountDownLatch(2);
		
		ExampleTracker exampleTracker = new ExampleTracker(bundle.getBundleContext(), latch);
		
		ServiceTracker<IExampleService, IExampleService> tracker = new ServiceTracker<>(bundle.getBundleContext(), IExampleService.class, exampleTracker);
		tracker.open();
		
		// We can safly asume that the 2 services will be available after a second.
		assertTrue(latch.await(1, TimeUnit.SECONDS));
		
		//Now we check if this are the right services
		ServiceReference<IExampleService> serviceReferenceByName = exampleTracker.getServiceReferenceByName("annotation");
		assertNotNull(serviceReferenceByName);
		IExampleService annotationService = bundle.getBundleContext().getService(serviceReferenceByName);
		assertNotNull(annotationService);
		annotationService.doSomething("I'm a test!");

		bundle.getBundleContext().ungetService(serviceReferenceByName);
		
		serviceReferenceByName = exampleTracker.getServiceReferenceByName("declarativeService");
		assertNotNull(serviceReferenceByName);
		IExampleService dsService = bundle.getBundleContext().getService(serviceReferenceByName);
		assertNotNull(dsService);
		dsService.doSomething("I'm a test!");
		
		bundle.getBundleContext().ungetService(serviceReferenceByName);
	}

}
