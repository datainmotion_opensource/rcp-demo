/**
 * Copyright (c) 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package de.dim.rcp.demo.helloworld.provider;

import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import de.dim.rcp.demo.api.helloworld.IExampleService;

/**
 * A Demo Hello world Service that will activate as the Bundle is started
 * 
 * @author Juergen Albert
 * @since 24.03.2017
 *
 */
@Component(property = "name=manual")
public class ExampleServiceProviderToStartManually implements IExampleService {

	private String name = "default";
	
	/* (non-Javadoc)
	 * @see de.dim.rcp.demo.api.helloworld.IExampleService#doSomething(java.lang.String)
	 */
	@Override
	public void doSomething(String withThis) {
		System.out.println(String.format("[%s] I'm more special and I do something with: %s", name, withThis));
	}
	
	/**
	 * The default activate method
	 * @param properties the properties the service is configured with
	 */
	@Activate
	public void activate(Map<String, Object> properties){
		if(properties.containsKey("name")){
			name = (String) properties.get("name");
		}
	}
}
