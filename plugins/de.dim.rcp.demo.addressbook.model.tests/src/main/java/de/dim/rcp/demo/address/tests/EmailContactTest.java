/**
 */
package de.dim.rcp.demo.address.tests;

import de.dim.rcp.demo.address.AddressFactory;
import de.dim.rcp.demo.address.EmailContact;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Email Contact</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmailContactTest extends ContactTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EmailContactTest.class);
	}

	/**
	 * Constructs a new Email Contact test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmailContactTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Email Contact test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EmailContact getFixture() {
		return (EmailContact)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(AddressFactory.eINSTANCE.createEmailContact());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EmailContactTest
