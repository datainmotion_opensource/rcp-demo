/**
 */
package de.dim.rcp.demo.address.tests;

import de.dim.rcp.demo.address.AddressFactory;
import de.dim.rcp.demo.address.BusinessAddress;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Business Address</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class BusinessAddressTest extends AddressTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BusinessAddressTest.class);
	}

	/**
	 * Constructs a new Business Address test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BusinessAddressTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Business Address test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BusinessAddress getFixture() {
		return (BusinessAddress)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(AddressFactory.eINSTANCE.createBusinessAddress());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //BusinessAddressTest
