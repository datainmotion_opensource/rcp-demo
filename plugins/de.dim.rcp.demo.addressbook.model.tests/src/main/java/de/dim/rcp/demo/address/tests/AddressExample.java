/**
 */
package de.dim.rcp.demo.address.tests;

import de.dim.rcp.demo.address.Address;
import de.dim.rcp.demo.address.AddressFactory;
import de.dim.rcp.demo.address.AddressPackage;
import de.dim.rcp.demo.address.EmailContact;
import de.dim.rcp.demo.address.Person;
import de.dim.rcp.demo.address.PhoneContact;
import de.dim.rcp.demo.address.Type;

import java.io.File;
import java.io.IOException;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import org.eclipse.emf.ecore.util.Diagnostician;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * A sample utility for the '<em><b>address</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class AddressExample {
	/**
	 * <!-- begin-user-doc -->
	 * Load all the argument file paths or URIs as instances of the model.
	 * <!-- end-user-doc -->
	 * @param args the file paths or URIs.
	 * @generated NOT
	 */
	public static void main(String[] args) {
		// Create a resource set to hold the resources.
		//
		ResourceSet resourceSet = new ResourceSetImpl();
		
		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
			 new XMIResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
		(Resource.Factory.Registry.DEFAULT_EXTENSION, 
				new XMIResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
			(AddressPackage.eNS_URI, 
			 AddressPackage.eINSTANCE);
        
		// If there are no arguments, emit an appropriate usage message.
		//
		if (args.length == 0) {
			System.out.println("Enter a list of file paths or URIs that have content like this:");
			try {
				Resource fuchsResource = resourceSet.createResource(URI.createURI("http:///fuchs.address"));
				Resource albertResource = resourceSet.createResource(URI.createURI("http:///albert.address"));
				Resource addressResource = resourceSet.createResource(URI.createURI("http:///kahlaische_4.address"));
				

				Person fuchs = AddressFactory.eINSTANCE.createPerson();
				fuchs.setFirstName("Sebastian");
				fuchs.setLastName("Fuchs");
				
				PhoneContact phone = AddressFactory.eINSTANCE.createPhoneContact();
				fuchs.getContacts().add(phone);
				phone.setType(Type.PRIVATE);
				phone.setValue("+4");
				
				
				fuchsResource.getContents().add(fuchs);

				Person albert = AddressFactory.eINSTANCE.createPerson();
				albert.setFirstName("J�rgen");
				albert.setLastName("Albert");
				
				PhoneContact albertPhone = AddressFactory.eINSTANCE.createPhoneContact();
				albert.getContacts().add(albertPhone);
				albertPhone.setType(Type.PRIVATE);
				albertPhone.setValue("+4912345667891011121314");

				EmailContact email = AddressFactory.eINSTANCE.createEmailContact();
				albert.getContacts().add(email);
				email.setType(Type.BUSINESS);
				email.setValue("spammichzu@someaddress.de");
				
				
				albertResource.getContents().add(albert);

				Address address = AddressFactory.eINSTANCE.createAddress();
				address.setCity("Jena");
				address.setNumber("4");
				address.setStreet("Kahlaische Str.");
				address.setZipCode("07743");
				
				address.getPersons().add(fuchs);
				address.getPersons().add(albert);

				addressResource.getContents().add(address);

				
				validateResource(fuchsResource);
				fuchsResource.save(System.out, null);
				
				validateResource(albertResource);
				albertResource.save(System.out, null);

				validateResource(addressResource);
				addressResource.save(System.out, null);
			
			
//				BusinessAddress businessAddress = AddressFactory.eINSTANCE.createBusinessAddress();
//				
//				copyInto(address, businessAddress);
//				businessAddress.setBuildingName("TCF");
//				addressResource.getContents().add(businessAddress);
//				addressResource.save(System.out, null);
//				
			}
			catch (IOException exception) {
				exception.printStackTrace();
			}
		}
		else {
			// Iterate over all the arguments.
			//
			for (int i = 0; i < args.length; ++i) {
				// Construct the URI for the instance file.
				// The argument is treated as a file path only if it denotes an existing file.
				// Otherwise, it's directly treated as a URL.
				//
				File file = new File(args[i]);
				URI uri = file.isFile() ? URI.createFileURI(file.getAbsolutePath()): URI.createURI(args[i]);

				try {
					// Demand load resource for this file.
					//
					Resource resource = resourceSet.getResource(uri, true);
					System.out.println("Loaded " + uri);

					// Validate the contents of the loaded resource.
					//
					
				}
				catch (RuntimeException exception) {
					System.out.println("Problem loading " + uri);
					exception.printStackTrace();
				}
			}
		}
	}
	
	private static void copyInto(EObject source, EObject target) {
		source.eClass().getEStructuralFeatures().stream().forEach(feature -> {
			target.eSet(feature, source.eGet(feature));
		});
	}


	private static void validateResource(Resource resource) {
		for (EObject eObject : resource.getContents()) {
			Diagnostic diagnostic = Diagnostician.INSTANCE.validate(eObject);
			if (diagnostic.getSeverity() != Diagnostic.OK) {
				printDiagnostic(diagnostic, "");
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Prints diagnostics with indentation.
	 * <!-- end-user-doc -->
	 * @param diagnostic the diagnostic to print.
	 * @param indent the indentation for printing.
	 * @generated
	 */
	protected static void printDiagnostic(Diagnostic diagnostic, String indent) {
		System.out.print(indent);
		System.out.println(diagnostic.getMessage());
		for (Diagnostic child : diagnostic.getChildren()) {
			printDiagnostic(child, indent + "  ");
		}
	}

} //AddressExample
