/**
 */
package de.dim.rcp.demo.address.tests;

import de.dim.rcp.demo.address.AddressFactory;
import de.dim.rcp.demo.address.PhoneContact;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Phone Contact</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PhoneContactTest extends ContactTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PhoneContactTest.class);
	}

	/**
	 * Constructs a new Phone Contact test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhoneContactTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Phone Contact test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PhoneContact getFixture() {
		return (PhoneContact)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(AddressFactory.eINSTANCE.createPhoneContact());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PhoneContactTest
