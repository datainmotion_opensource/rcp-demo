/**
 */
package de.dim.rcp.demo.address.impl;

import de.dim.rcp.demo.address.AddressPackage;
import de.dim.rcp.demo.address.BusinessAddress;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Business Address</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.dim.rcp.demo.address.impl.BusinessAddressImpl#getBuildingName <em>Building Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BusinessAddressImpl extends AddressImpl implements BusinessAddress {
	/**
	 * The default value of the '{@link #getBuildingName() <em>Building Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuildingName()
	 * @generated
	 * @ordered
	 */
	protected static final String BUILDING_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBuildingName() <em>Building Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuildingName()
	 * @generated
	 * @ordered
	 */
	protected String buildingName = BUILDING_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BusinessAddressImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AddressPackage.Literals.BUSINESS_ADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBuildingName() {
		return buildingName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBuildingName(String newBuildingName) {
		String oldBuildingName = buildingName;
		buildingName = newBuildingName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AddressPackage.BUSINESS_ADDRESS__BUILDING_NAME, oldBuildingName, buildingName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AddressPackage.BUSINESS_ADDRESS__BUILDING_NAME:
				return getBuildingName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AddressPackage.BUSINESS_ADDRESS__BUILDING_NAME:
				setBuildingName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AddressPackage.BUSINESS_ADDRESS__BUILDING_NAME:
				setBuildingName(BUILDING_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AddressPackage.BUSINESS_ADDRESS__BUILDING_NAME:
				return BUILDING_NAME_EDEFAULT == null ? buildingName != null : !BUILDING_NAME_EDEFAULT.equals(buildingName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (buildingName: ");
		result.append(buildingName);
		result.append(')');
		return result.toString();
	}

} //BusinessAddressImpl
