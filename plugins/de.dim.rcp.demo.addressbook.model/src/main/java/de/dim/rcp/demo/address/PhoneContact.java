/**
 */
package de.dim.rcp.demo.address;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Phone Contact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.dim.rcp.demo.address.PhoneContact#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.dim.rcp.demo.address.AddressPackage#getPhoneContact()
 * @model
 * @generated
 */
public interface PhoneContact extends Contact {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see de.dim.rcp.demo.address.AddressPackage#getPhoneContact_Value()
	 * @model dataType="de.dim.rcp.demo.address.MSISDN"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link de.dim.rcp.demo.address.PhoneContact#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // PhoneContact
