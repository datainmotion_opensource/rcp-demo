/**
 */
package de.dim.rcp.demo.address;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Business Address</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.dim.rcp.demo.address.BusinessAddress#getBuildingName <em>Building Name</em>}</li>
 * </ul>
 *
 * @see de.dim.rcp.demo.address.AddressPackage#getBusinessAddress()
 * @model
 * @generated
 */
public interface BusinessAddress extends Address {
	/**
	 * Returns the value of the '<em><b>Building Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Building Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Building Name</em>' attribute.
	 * @see #setBuildingName(String)
	 * @see de.dim.rcp.demo.address.AddressPackage#getBusinessAddress_BuildingName()
	 * @model
	 * @generated
	 */
	String getBuildingName();

	/**
	 * Sets the value of the '{@link de.dim.rcp.demo.address.BusinessAddress#getBuildingName <em>Building Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Building Name</em>' attribute.
	 * @see #getBuildingName()
	 * @generated
	 */
	void setBuildingName(String value);

} // BusinessAddress
