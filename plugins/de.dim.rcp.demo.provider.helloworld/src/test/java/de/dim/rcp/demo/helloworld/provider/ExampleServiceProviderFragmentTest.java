/**
 * Copyright (c) 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package de.dim.rcp.demo.helloworld.provider;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Test;

import de.dim.rcp.demo.helloworld.provider.ExampleServiceProvider;

/**
 * A simple test example. Tasts can be written and placed here, but they will be added to the final jar, 
 * thus it is not recommended to write tests like this.
 * @author Juergen Albert
 */
public class ExampleServiceProviderFragmentTest {

	
	@Test
	public void test() {
		ExampleServiceProvider provider = new ExampleServiceProvider();
		provider.activate(Collections.singletonMap("name", "test"));
		provider.doSomething("I'm a test!");
	}

}
