package de.dim.rcp.demo.module1.editors;

import org.eclipse.ui.editors.text.TextEditor;

public class DemoXMLEditor extends TextEditor {

	private ColorManager colorManager;

	public DemoXMLEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new XMLConfiguration(colorManager));
		setDocumentProvider(new XMLDocumentProvider());
	}
	@Override
	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}

}
