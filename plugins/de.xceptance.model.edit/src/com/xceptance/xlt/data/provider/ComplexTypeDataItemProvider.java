/**
 */
package com.xceptance.xlt.data.provider;


import com.xceptance.project.ProjectFactory;
import com.xceptance.project.ProjectPackage;

import com.xceptance.project.provider.XceptanceEditPlugin;

import com.xceptance.xlt.data.ComplexTypeData;
import com.xceptance.xlt.data.DataFactory;
import com.xceptance.xlt.data.DataPackage;

import com.xceptance.xlt.script.ScriptFactory;
import com.xceptance.xlt.script.ScriptPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.xceptance.xlt.data.ComplexTypeData} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ComplexTypeDataItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeDataItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DataPackage.Literals.COMPLEX_TYPE_DATA__ANY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ComplexTypeData.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ComplexTypeData"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_ComplexTypeData_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ComplexTypeData.class)) {
			case DataPackage.COMPLEX_TYPE_DATA__ANY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(DataPackage.Literals.COMPLEX_TYPE_DATA__ANY,
				 FeatureMapUtil.createEntry
					(ProjectPackage.Literals.DOCUMENT_ROOT__PROJECT,
					 ProjectFactory.eINSTANCE.createComplexTypeProject())));

		newChildDescriptors.add
			(createChildParameter
				(DataPackage.Literals.COMPLEX_TYPE_DATA__ANY,
				 FeatureMapUtil.createEntry
					(DataPackage.Literals.DOCUMENT_ROOT__DATA,
					 DataFactory.eINSTANCE.createComplexTypeData())));

		newChildDescriptors.add
			(createChildParameter
				(DataPackage.Literals.COMPLEX_TYPE_DATA__ANY,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.DOCUMENT_ROOT__JAVAMODULE,
					 ScriptFactory.eINSTANCE.createComplexTypeJavaModule())));

		newChildDescriptors.add
			(createChildParameter
				(DataPackage.Literals.COMPLEX_TYPE_DATA__ANY,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.DOCUMENT_ROOT__SCRIPTMODULE,
					 ScriptFactory.eINSTANCE.createComplexTypeScriptModule1())));

		newChildDescriptors.add
			(createChildParameter
				(DataPackage.Literals.COMPLEX_TYPE_DATA__ANY,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.DOCUMENT_ROOT__TESTCASE,
					 ScriptFactory.eINSTANCE.createComplexTypeTestCase())));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return XceptanceEditPlugin.INSTANCE;
	}

}
