/**
 */
package com.xceptance.xlt.script.provider;


import com.xceptance.project.provider.XceptanceEditPlugin;

import com.xceptance.xlt.script.ComplexTypeTestCase;
import com.xceptance.xlt.script.ScriptFactory;
import com.xceptance.xlt.script.ScriptPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.xceptance.xlt.script.ComplexTypeTestCase} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ComplexTypeTestCaseItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeTestCaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
			addCodecommentPropertyDescriptor(object);
			addBaseURLPropertyDescriptor(object);
			addDisabledPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addJunitTestPropertyDescriptor(object);
			addVersionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_tags_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_description_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_description_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__DESCRIPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Codecomment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCodecommentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_codecomment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_codecomment_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__CODECOMMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Base URL feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseURLPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_baseURL_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_baseURL_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__BASE_URL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Disabled feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_disabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_disabled_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__DISABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_id_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Junit Test feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addJunitTestPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_junitTest_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_junitTest_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCase_version_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCase_version_feature", "_UI_ComplexTypeTestCase_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP);
			childrenFeatures.add(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS);
			childrenFeatures.add(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__POST_STEPS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ComplexTypeTestCase.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ComplexTypeTestCase"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ComplexTypeTestCase)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_ComplexTypeTestCase_type") :
			getString("_UI_ComplexTypeTestCase_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ComplexTypeTestCase.class)) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__TAGS:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DESCRIPTION:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__CODECOMMENT:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__BASE_URL:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__DISABLED:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__ID:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__JUNIT_TEST:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__VERSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS:
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE__POST_STEPS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__TAGS,
					 "")));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__DESCRIPTION,
					 "")));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__COMMAND,
					 ScriptFactory.eINSTANCE.createComplexTypeScriptCommand())));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__ACTION,
					 ScriptFactory.eINSTANCE.createComplexTypeScriptAction())));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__MODULE,
					 ScriptFactory.eINSTANCE.createComplexTypeScriptModule())));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__CODECOMMENT,
					 "")));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE__POST_STEPS,
				 ScriptFactory.eINSTANCE.createComplexTypeTestCasePostSteps()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return XceptanceEditPlugin.INSTANCE;
	}

}
