/**
 */
package com.xceptance.xlt.script.provider;


import com.xceptance.project.provider.XceptanceEditPlugin;

import com.xceptance.xlt.script.ComplexTypeTestCasePostSteps;
import com.xceptance.xlt.script.ScriptFactory;
import com.xceptance.xlt.script.ScriptPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.xceptance.xlt.script.ComplexTypeTestCasePostSteps} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ComplexTypeTestCasePostStepsItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeTestCasePostStepsItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCodecommentPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Codecomment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCodecommentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeTestCasePostSteps_codecomment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeTestCasePostSteps_codecomment_feature", "_UI_ComplexTypeTestCasePostSteps_type"),
				 ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ComplexTypeTestCasePostSteps.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ComplexTypeTestCasePostSteps"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_ComplexTypeTestCasePostSteps_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ComplexTypeTestCasePostSteps.class)) {
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ScriptPackage.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__COMMAND,
					 ScriptFactory.eINSTANCE.createComplexTypeScriptCommand())));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__ACTION,
					 ScriptFactory.eINSTANCE.createComplexTypeScriptAction())));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__MODULE,
					 ScriptFactory.eINSTANCE.createComplexTypeScriptModule())));

		newChildDescriptors.add
			(createChildParameter
				(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__GROUP_SCRIPT_ACTIONS,
				 FeatureMapUtil.createEntry
					(ScriptPackage.Literals.COMPLEX_TYPE_TEST_CASE_POST_STEPS__CODECOMMENT,
					 "")));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return XceptanceEditPlugin.INSTANCE;
	}

}
