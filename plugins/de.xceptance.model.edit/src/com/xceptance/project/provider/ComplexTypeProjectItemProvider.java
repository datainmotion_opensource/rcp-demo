/**
 */
package com.xceptance.project.provider;


import com.xceptance.project.ComplexTypeProject;
import com.xceptance.project.ProjectFactory;
import com.xceptance.project.ProjectPackage;

import com.xceptance.xlt.data.DataFactory;

import com.xceptance.xlt.script.ScriptFactory;

import java.math.BigInteger;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link com.xceptance.project.ComplexTypeProject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ComplexTypeProjectItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexTypeProjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVersionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ComplexTypeProject_version_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ComplexTypeProject_version_feature", "_UI_ComplexTypeProject_type"),
				 ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME);
			childrenFeatures.add(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ComplexTypeProject.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ComplexTypeProject"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		BigInteger labelValue = ((ComplexTypeProject)object).getVersion();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0 ?
			getString("_UI_ComplexTypeProject_type") :
			getString("_UI_ComplexTypeProject_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ComplexTypeProject.class)) {
			case ProjectPackage.COMPLEX_TYPE_PROJECT__VERSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ProjectPackage.COMPLEX_TYPE_PROJECT__NAME:
			case ProjectPackage.COMPLEX_TYPE_PROJECT__DESCRIPTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ProjectFactory.eINSTANCE.createComplexTypeProject()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 DataFactory.eINSTANCE.createComplexTypeData()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeJavaModule()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeModuleParameter()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptAction()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptCommand()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModule()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModule1()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModuleCondition()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModuleParameter()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeTestCase()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 ScriptFactory.eINSTANCE.createComplexTypeTestCasePostSteps()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME,
				 XMLTypeFactory.eINSTANCE.createAnyType()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ProjectFactory.eINSTANCE.createComplexTypeProject()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 DataFactory.eINSTANCE.createComplexTypeData()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeJavaModule()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeModuleParameter()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptAction()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptCommand()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModule()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModule1()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModuleCondition()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeScriptModuleParameter()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeTestCase()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 ScriptFactory.eINSTANCE.createComplexTypeTestCasePostSteps()));

		newChildDescriptors.add
			(createChildParameter
				(ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION,
				 XMLTypeFactory.eINSTANCE.createAnyType()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__NAME ||
			childFeature == ProjectPackage.Literals.COMPLEX_TYPE_PROJECT__DESCRIPTION;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return XceptanceEditPlugin.INSTANCE;
	}

}
